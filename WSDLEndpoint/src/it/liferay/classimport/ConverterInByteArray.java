package it.liferay.classimport;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

/**
 * 
 * @author Luca
 *
 */
public class ConverterInByteArray {

	/**
	 * Decode a base64 string and puts it in a byte array.
	 * @param base64EncodedString
	 * @return
	 */
	public static byte[] base64StringConvert(String base64EncodedString) {

		return (new Base64()).decode(base64EncodedString);

	}

	/**
	 * Read a file from the filesystem at the specified path and puts it in a byte array.
	 * @param path where the file is located in the filesystem
	 * @return
	 */
	public static byte[] readFileAndConvert(String path) {

		byte array[] = null;

		try {
			array = IOUtils.toByteArray(new FileInputStream(new File(path)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return array;

	}

}
