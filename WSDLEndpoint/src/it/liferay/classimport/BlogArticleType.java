package it.liferay.classimport;

public enum BlogArticleType {
	General("general"),
	News("news"),
	Blogs("blogs"),
	Test("test"),
	PressRelease("press release");
	
    /**
     * @param text
     */
    private BlogArticleType(final String nameEnum) {
        this.text = nameEnum;
    }

    /**
	 * @uml.property  name="text"
	 */
    private final String text;

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return this.text;
    }	
}
