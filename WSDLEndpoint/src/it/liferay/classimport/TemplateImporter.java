package it.liferay.classimport;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.journal.model.JournalTemplateSoap;
import com.liferay.portlet.journal.service.http.JournalArticleServiceSoap;
import com.liferay.portlet.journal.service.http.JournalArticleServiceSoapServiceLocator;
import com.liferay.portlet.journal.service.http.JournalTemplateServiceSoap;
import com.liferay.portlet.journal.service.http.JournalTemplateServiceSoapServiceLocator;
import com.liferay.portlet.journal.service.http.Portlet_Journal_JournalTemplateServiceSoapBindingStub;

import it.liferay.config.Credentials;
import it.liferay.config.DefaultConfig;
import it.liferay.util.DescriptionHelper;

public class TemplateImporter {
	
	public static TemplateImporter singletonTemplate = null;
	
	private static final String USERNAME = Credentials.USERNAME;
	private static final String PASSWORD = Credentials.PASSWORD;
	private static final Long GROUPID = DefaultConfig.GROUPID;

	private JournalTemplateServiceSoap soapTemplate;
	private JournalTemplateServiceSoapServiceLocator locatorTemplate;

	private String templateId;
	
	public TemplateImporter () throws ServiceException{
		this.locatorTemplate = new JournalTemplateServiceSoapServiceLocator();
		this.soapTemplate = this.locatorTemplate.getPortlet_Journal_JournalTemplateService();
		((Portlet_Journal_JournalTemplateServiceSoapBindingStub)this.soapTemplate).setUsername(USERNAME);
		((Portlet_Journal_JournalTemplateServiceSoapBindingStub)this.soapTemplate).setPassword(PASSWORD);
		if (DefaultConfig.OVERRIDE_SOAP_TIMEOUT == true) {
			((Portlet_Journal_JournalTemplateServiceSoapBindingStub) this.soapTemplate)
			.setTimeout(DefaultConfig.SOAP_TIMEOUT * 1000);			
		}     
	}
	
	public TemplateImporter (String templateId) throws ServiceException{
		this();
		this.templateId = templateId;
	}
	
	public static TemplateImporter getInstance() {
		return getInstance(GROUPID);
	}
	
	public static TemplateImporter getInstance(Long templateId) {
		if ( singletonTemplate == null ) {
			try {
				singletonTemplate = new TemplateImporter (String.valueOf(templateId));
			} catch (ServiceException e) {
				e.printStackTrace();
				singletonTemplate = null;
			}
		}
		return singletonTemplate;
	}
	

	
	/*Da sistemare il tipo di ritorno, createdTemplate.getTemplateId() ritorna una stringa*/
	public long addTemplate(String title, String structureId) {
		long retValue = DefaultConfig.RETERROR;
		String[] templateProperties = DescriptionHelper.setAndGetTitlesFromLanguageIds(title);
		long vocabularyId = 10538;
		JournalTemplateSoap createdTemplate= null;
		ServiceContext srvCntx = null;
		
		String[] titleMapValues = DescriptionHelper.setAndGetTitlesFromLanguageIds(title);
		/*Description � sempre null perci� non lo considero
		 * String[] descriptionMapValues = DescriptionHelper.setAndGetDescriptionsFromLanguageIds(description); 
		 */
				
		try {
			srvCntx = new ServiceContext();
			srvCntx.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);
			/*Il terzo parametro indica se l'id del template � autogenerato o meno, quindi controllare se bisogna darlo da imput o lo facciamo generare automaticamente*/
			/*Sistemare xls mettendolo empty*/
			createdTemplate = this.soapTemplate.addTemplate(GROUPID, templateId, false, structureId, DefaultConfig.titleMapLanguageIds,titleMapValues, null,null, "xsl", true, "langType", true, srvCntx);
			retValue = createdTemplate.getId();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	
		return retValue;
	}
}
