package it.liferay.classimport;

import it.liferay.config.Credentials;
import it.liferay.config.DefaultConfig;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.liferay.portal.kernel.repository.model.FileEntrySoap;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.documentlibrary.model.DLFolderSoap;
import com.liferay.portlet.documentlibrary.service.http.DLAppServiceSoap;
import com.liferay.portlet.documentlibrary.service.http.DLAppServiceSoapServiceLocator;
import com.liferay.portlet.documentlibrary.service.http.DLFolderServiceSoap;
import com.liferay.portlet.documentlibrary.service.http.DLFolderServiceSoapServiceLocator;
import com.liferay.portlet.documentlibrary.service.http.Portlet_DL_DLAppServiceSoapBindingStub;
import com.liferay.portlet.documentlibrary.service.http.Portlet_DL_DLFolderServiceSoapBindingStub;

public class DirectoryImporter {
	private static DirectoryImporter singletonDirectory = null;
	
	public static DirectoryImporter getInstance() {
		return getInstance(GROUPID);
	}
	
	public static DirectoryImporter getInstance(long repositoryId) {
		if ( singletonDirectory == null ) {
			try {
				singletonDirectory = new DirectoryImporter(repositoryId);
			} catch (ServiceException e) {
				e.printStackTrace();
				singletonDirectory = null;
			}
		}
		
		return singletonDirectory;
	}
	
	public static DirectoryImporter replaceInstance() {
		return replaceInstance(GROUPID);
	}
	
	public static DirectoryImporter replaceInstance(long repositoryId) {
		singletonDirectory.repoId = repositoryId;
		return singletonDirectory;
	}		
	
	//
	private static final String USERNAME = Credentials.USERNAME;
	private static final String PASSWORD = Credentials.PASSWORD;	
	private static final Long GROUPID = DefaultConfig.GROUPID;
	private static final Long ROOTFOLDER = DefaultConfig.ROOTFOLDER;

	// 
	/**
	 * @uml.property  name="locatorFolder"
	 * @uml.associationEnd  
	 */
	private DLFolderServiceSoapServiceLocator locatorFolder;
	/**
	 * @uml.property  name="soapFolder"
	 * @uml.associationEnd  
	 */
	private DLFolderServiceSoap soapFolder;
	/**
	 * @uml.property  name="locatorAppService"
	 * @uml.associationEnd  
	 */
	private DLAppServiceSoapServiceLocator locatorAppService;
	/**
	 * @uml.property  name="soapAppService"
	 * @uml.associationEnd  
	 */
	private DLAppServiceSoap soapAppService;
	/**
	 * @uml.property  name="repoId"
	 */
	private long repoId;
	
	@SuppressWarnings("unused")
	private DirectoryImporter() throws ServiceException {
    	// Directory
		this.locatorFolder = new DLFolderServiceSoapServiceLocator();
    	this.soapFolder = this.locatorFolder.getPortlet_DL_DLFolderService();
        ((Portlet_DL_DLFolderServiceSoapBindingStub)this.soapFolder).setUsername(USERNAME);
        ((Portlet_DL_DLFolderServiceSoapBindingStub)this.soapFolder).setPassword(PASSWORD);
		if (DefaultConfig.OVERRIDE_SOAP_TIMEOUT == true) {
			((Portlet_DL_DLFolderServiceSoapBindingStub) this.soapFolder)
			.setTimeout(DefaultConfig.SOAP_TIMEOUT * 1000);			
		}           
        // File
    	this.locatorAppService = new DLAppServiceSoapServiceLocator();
    	this.soapAppService = this.locatorAppService.getPortlet_DL_DLAppService();
        ((Portlet_DL_DLAppServiceSoapBindingStub)this.soapAppService).setUsername(USERNAME);
        ((Portlet_DL_DLAppServiceSoapBindingStub)this.soapAppService).setPassword(PASSWORD);
		if (DefaultConfig.OVERRIDE_SOAP_TIMEOUT == true) {
			((Portlet_DL_DLAppServiceSoapBindingStub) this.soapAppService)
			.setTimeout(DefaultConfig.SOAP_TIMEOUT * 1000);			
		}        
	}
	
	private DirectoryImporter(long repositoryId) throws ServiceException {
		this();
		this.repoId = repositoryId;
	}	
	
	public long addFolderInRoot(String name, String description) {
		return addFolder(name, description, ROOTFOLDER);
	}
	
	public long addFolder(String name, String description, long parentFolder) {
		long retValue = DefaultConfig.RETERROR;
		ServiceContext srvCntx = null ;
		try {
			srvCntx = new ServiceContext();
			srvCntx.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);
			DLFolderSoap createdFolder = this.soapFolder.addFolder(GROUPID, this.repoId, false, parentFolder, name, description, srvCntx);
			retValue = createdFolder.getFolderId();
		} catch (RemoteException e) {
			
			/*System.out.println("================= updated ==================");
			try {
				this.soapFolder.updateFolder(retValue,name,description,(new Long(name)),null,true,srvCntx);
			} catch (RemoteException e1) {
				System.out.println("================= no ==================");
				e1.printStackTrace();
			}*/
			e.printStackTrace();
		}
		
		return retValue;
	}
	
	public long[] getSubfoldersInRoot() {
		return getSubfolders(ROOTFOLDER);
	}
	
	public long[] getSubfolders(long parentFolder) {
		long[] subFolders = null;
		
		try {
			subFolders = this.soapFolder.getSubfolderIds(this.repoId, parentFolder, false);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return subFolders;
	}
	
	public boolean deleteFolder(long folderId) {
		boolean retValue = false;
		
		if (folderId == ROOTFOLDER) {
			return false;
		}
		
		try {
			this.soapFolder.deleteFolder(folderId);
			retValue = true;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retValue;
	}
	
	public long getFolderByNameInRoot(String folderName) {
		return getFolderByNameInDirectory(folderName, ROOTFOLDER);
	}	
	
	public long getFolderByNameInDirectory(String folderName, long parentFolder) {
		long retValue = DefaultConfig.RETERROR;
		
		try {
			DLFolderSoap retFolder = this.soapFolder.getFolder(this.repoId, parentFolder, folderName);
			retValue = retFolder.getFolderId();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retValue;
	}
	
	public long getFileCountInRoot() {
		return getFileCount(ROOTFOLDER);
	}
	
	public long getFileCount(long folder) {
		long retValue = DefaultConfig.RETERROR;
	
		try {
			retValue = this.soapAppService.getFileEntriesCount(this.repoId, folder);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retValue;
	}
	
	public long[] getFileListInRoot() {
		return getFileList(ROOTFOLDER);
	}
	
	public long[] getFileList(long folder) {
		long[] retValue = null;
	
		try {
			FileEntrySoap[] fileList = this.soapAppService.getFileEntries(this.repoId, folder);
			if ((fileList != null) && (fileList.length > 0)) {
				retValue = new long[fileList.length];
				for(int i = 0; i < fileList.length; i++) {
					retValue[i] = fileList[i].getFileEntryId();
				}
			} else {
				retValue = new long[0];
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retValue;
	}
	
	public long[] getFileListLimit(long folder, int from, int to) {
		long[] retValue = null;
	
		try {
			FileEntrySoap[] fileList = this.soapAppService.getFileEntries(this.repoId, folder, from, to);
			if ((fileList != null) && (fileList.length > 0)) {
				retValue = new long[fileList.length];
				for(int i = 0; i < fileList.length; i++) {
					retValue[i] = fileList[i].getFileEntryId();
				}
			} else {
				retValue = new long[0];
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retValue;
	}
}
