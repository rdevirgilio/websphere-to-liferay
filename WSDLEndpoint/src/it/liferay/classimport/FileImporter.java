package it.liferay.classimport;

import it.liferay.config.Credentials;
import it.liferay.config.DefaultConfig;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.liferay.portal.kernel.repository.model.FileEntrySoap;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.documentlibrary.model.DLFileShortcutSoap;
import com.liferay.portlet.documentlibrary.service.http.DLAppServiceSoap;
import com.liferay.portlet.documentlibrary.service.http.DLAppServiceSoapServiceLocator;
import com.liferay.portlet.documentlibrary.service.http.Portlet_DL_DLAppServiceSoapBindingStub;

public class FileImporter {

	private static final String USERNAME = Credentials.USERNAME;
	private static final String PASSWORD = Credentials.PASSWORD;

	private static final Long GROUPID = DefaultConfig.GROUPID;
	private static final Long ROOTFOLDER = DefaultConfig.ROOTFOLDER;

	/**
	 * @uml.property  name="locatorAppService"
	 * @uml.associationEnd  
	 */
	private DLAppServiceSoapServiceLocator locatorAppService;
	/**
	 * @uml.property  name="soapAppService"
	 * @uml.associationEnd  
	 */
	private DLAppServiceSoap soapAppService;

	private static FileImporter instance = null;

	/**
	 * @uml.property  name="repoId"
	 */
	private long repoId;

	/**
	 * 
	 * @return
	 */
	public static FileImporter getInstance() {
		return getInstance(GROUPID);
	}

	/**
	 * 
	 * @return
	 */
	public static FileImporter getInstance(long repositoryId) {

		if (instance == null) {
			try {
				instance = new FileImporter(repositoryId);
			} catch (ServiceException e) {
				instance = null;
				e.printStackTrace();
			}
		}

		return instance;

	}

	/**
	 * 
	 * @return
	 */
	public static FileImporter replaceInstance(long repositoryId) {
		instance.repoId = repositoryId;
		return instance;
	}

	/**
	 * 
	 * @return
	 */
	public static FileImporter replaceInstance() {
		return replaceInstance(GROUPID);
	}

	
	/**
	 * Create a FileEntry from a codified string in base64
	 * @param folderId
	 * @param base64String
	 * @param sourceFileName
	 * @param mimeType
	 * @param title
	 * @param description
	 * @param changeLog
	 * @return the id of the created file, -1 if error
	 */
	public long addFileEntryFromBase64String(long folderId,
			String base64String, String sourceFileName, String mimeType,
			String title, String description, String changeLog) {

		return addFileEntry(folderId, ConverterInByteArray.base64StringConvert(base64String), 
				sourceFileName, mimeType, title, description, changeLog);
	}
	
	/**
	 * 
	 * @param base64String
	 * @param sourceFileName
	 * @param mimeType
	 * @param title
	 * @param description
	 * @param changeLog
	 * @return
	 */
	public long addFileEntryFromBase64StringInRoot(String base64String, String sourceFileName,
			String mimeType, String title, String description, String changeLog) {

		return addFileEntryFromBase64String(ROOTFOLDER, base64String, 
				sourceFileName, mimeType, title, description, changeLog);
	}

	
	/**
	 * Create a FileEntry from a file located in the specified path
	 * @param folderId
	 * @param path where is located the file to load
	 * @param sourceFileName
	 * @param mimeType
	 * @param title
	 * @param description
	 * @param changeLog
	 * @return
	 */
	public long addFileEntryFromPath(long folderId, String path, String sourceFileName,
			String mimeType, String title, String description, String changeLog) {
		
		return addFileEntry(folderId, ConverterInByteArray.readFileAndConvert(path), 
				sourceFileName, mimeType, title, description, changeLog);
	}
	
	
	/**
	 * 
	 * @param path
	 * @param sourceFileName
	 * @param mimeType
	 * @param title
	 * @param description
	 * @param changeLog
	 * @return
	 */
	public long addFileEntryFromPathInRoot(String path, String sourceFileName,
			String mimeType, String title, String description, String changeLog) {
		
		return addFileEntryFromPath(ROOTFOLDER, path, sourceFileName, mimeType, 
				title, description, changeLog);
	}

	
	/**
	 * Create a FileEntry from a byte[] array.
	 * @param folderId
	 * @param data
	 * @param sourceFileName
	 * @param mimeType
	 * @param title
	 * @param description
	 * @param changeLog
	 * @return
	 */
	public long addFileEntry(long folderId, byte[] data, String sourceFileName,
			String mimeType, String title, String description, String changeLog) {

		FileEntrySoap fileEntrySoap = null;

		try {
			ServiceContext srvCntx = new ServiceContext();
			srvCntx.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);
			
			fileEntrySoap = this.soapAppService.addFileEntry(this.repoId,
					folderId, sourceFileName, mimeType, title, description,
					changeLog, data, srvCntx);

		} catch (RemoteException e) {
			e.printStackTrace();
			return DefaultConfig.RETERROR;
		}

		return fileEntrySoap.getFileEntryId();

	}
	
	/**
	 * Add a FileEntry from a byte[] array in the root folder.
	 * @param data
	 * @param sourceFileName
	 * @param mimeType
	 * @param title
	 * @param description
	 * @param changeLog
	 * @return
	 */
	public long addFileEntryInRoot(byte[] data, String sourceFileName,
			String mimeType, String title, String description, String changeLog) {
		return addFileEntry(ROOTFOLDER, data, 
				sourceFileName, mimeType, title, description, changeLog);
	}
	
	/**
	 * 
	 * @param folderId
	 * @param toFileEntryId
	 * @return
	 */
	public long addFileShortcut(long folderId, long toFileEntryId) {

		DLFileShortcutSoap fileShortcutSoap = null;

		try {
			ServiceContext srvCntx = new ServiceContext();
			srvCntx.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);
			
			fileShortcutSoap = this.soapAppService.addFileShortcut(this.repoId, folderId, toFileEntryId, srvCntx);

		} catch (RemoteException e) {
			e.printStackTrace();
			return DefaultConfig.RETERROR;
		}

		return fileShortcutSoap.getFileShortcutId();

	}

	/**
	 * 
	 * @return
	 */
	public boolean deleteFileEntry(long entryID) {
		try {
			this.soapAppService.deleteFileEntry(entryID);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * 
	 * @param entryID
	 * @return
	 */
	public FileEntrySoap getFileEntry(long entryID) {

		FileEntrySoap fileEntrySoap = null;

		try {
			this.soapAppService.getFileEntry(entryID);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		return fileEntrySoap;
	}

	/**
	 * With DefaultConfig.GROUPID as groupID
	 * @param folderId
	 * @param title
	 * @return
	 */
	public FileEntrySoap getFileEntry(long folderId, String title) {
		return getFileEntry(GROUPID, folderId, title);
	}
	
	/**
	 * 
	 * @param groupID
	 * @param folderId
	 * @param title
	 * @return
	 */
	public FileEntrySoap getFileEntry(long groupID, long folderId, String title) {

		FileEntrySoap fileEntrySoap = null;

		try {
			this.soapAppService.getFileEntry(groupID, folderId, title);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		return fileEntrySoap;
	}

	/**
	 * @throws ServiceException
	 * 
	 */
	@SuppressWarnings("unused")
	private FileImporter(long repositoryId) throws ServiceException {

		this.locatorAppService = new DLAppServiceSoapServiceLocator();
		this.soapAppService = locatorAppService.getPortlet_DL_DLAppService();

		((Portlet_DL_DLAppServiceSoapBindingStub) this.soapAppService)
				.setUsername(USERNAME);
		((Portlet_DL_DLAppServiceSoapBindingStub) this.soapAppService)
				.setPassword(PASSWORD);
		
		if (DefaultConfig.OVERRIDE_SOAP_TIMEOUT == true) {
			((Portlet_DL_DLAppServiceSoapBindingStub) this.soapAppService)
			.setTimeout(DefaultConfig.SOAP_TIMEOUT * 1000);			
		}

		this.repoId = repositoryId;

	}

}
