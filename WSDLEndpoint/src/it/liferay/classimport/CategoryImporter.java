package it.liferay.classimport;

import java.rmi.RemoteException;

import it.liferay.config.Credentials;
import it.liferay.config.DefaultConfig;
import it.liferay.util.DescriptionHelper;

import javax.xml.rpc.ServiceException;

import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.asset.model.AssetCategorySoap;
import com.liferay.portlet.asset.service.http.AssetCategoryServiceSoap;
import com.liferay.portlet.asset.service.http.AssetCategoryServiceSoapServiceLocator;
import com.liferay.portlet.asset.service.http.Portlet_Asset_AssetCategoryServiceSoapBindingStub;

//addCategory(long parentCategoryId, java.lang.String name, java.lang.String description, java.lang.String displayStyle, java.lang.String emailAddress, java.lang.String inProtocol, java.lang.String inServerName, int inServerPort, boolean inUseSSL, java.lang.String inUserName, java.lang.String inPassword, int inReadInterval, java.lang.String outEmailAddress, boolean outCustom, java.lang.String outServerName, int outServerPort, boolean outUseSSL, java.lang.String outUserName, java.lang.String outPassword, boolean mailingListActive, boolean allowAnonymousEmail, com.liferay.portal.service.ServiceContext serviceContext

public class CategoryImporter {
	private static CategoryImporter singletonCategory = null;

	private static final String USERNAME = Credentials.USERNAME;
	private static final String PASSWORD = Credentials.PASSWORD;
	private static final Long GROUPID = DefaultConfig.GROUPID;

	private AssetCategoryServiceSoap soapCategory;
	private AssetCategoryServiceSoapServiceLocator locatorCategory;
	/**
	 * @uml.property name="soapAppService"
	 * @uml.associationEnd
	 */
	private AssetCategoryServiceSoap soapAppService;
	/**
	 * @uml.property name="categoryId"
	 */
	private long categoryId;

	public CategoryImporter() throws ServiceException {
		// Category
		this.locatorCategory = new AssetCategoryServiceSoapServiceLocator();
		this.soapCategory = this.locatorCategory.getPortlet_Asset_AssetCategoryService();
		((Portlet_Asset_AssetCategoryServiceSoapBindingStub) this.soapCategory).setUsername(USERNAME);
		((Portlet_Asset_AssetCategoryServiceSoapBindingStub) this.soapCategory).setPassword(PASSWORD);
		if (DefaultConfig.OVERRIDE_SOAP_TIMEOUT == true) {
			((Portlet_Asset_AssetCategoryServiceSoapBindingStub) this.soapCategory)
					.setTimeout(DefaultConfig.SOAP_TIMEOUT * 1000);
		}
	}

	public CategoryImporter(long categoryId) throws ServiceException {
		this();
		this.categoryId = categoryId;
	}
	
	//Controllare uso getInstance
	public static CategoryImporter getInstance() {
		return getInstance(GROUPID);
	}

	public static CategoryImporter getInstance(long repositoryId) {
		if (singletonCategory == null) {
			try {
				singletonCategory = new CategoryImporter(repositoryId);
			} catch (ServiceException e) {
				e.printStackTrace();
				singletonCategory = null;
			}
		}
		return singletonCategory;
	}

	public long addCategory(long parentCategoryId, String title,
			String description) {
		long retValue = DefaultConfig.RETERROR;
		String[] categoryProperties = DescriptionHelper.setAndGetTitlesFromLanguageIds(title);
		long vocabularyId = 10538;
		AssetCategorySoap createdCategory = null;
		ServiceContext srvCntx = null;
		
		String[] titleMapValues = DescriptionHelper.setAndGetTitlesFromLanguageIds(title);
		String[] descriptionMapValues = DescriptionHelper.setAndGetDescriptionsFromLanguageIds(description); 
				

		try {
			srvCntx = new ServiceContext();
			srvCntx.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);
			createdCategory = this.soapCategory.addCategory(parentCategoryId,
					DefaultConfig.titleMapLanguageIds, titleMapValues,
					DefaultConfig.descriptionMapLanguageIds,
					descriptionMapValues, vocabularyId, categoryProperties,
					srvCntx);
			retValue = createdCategory.getCategoryId();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public void deleteCategory(long categoryId) {
		ServiceContext srvCntx = null;
		try {
			srvCntx = new ServiceContext();
			srvCntx.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);
			this.soapCategory.deleteCategory(categoryId);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}
}
