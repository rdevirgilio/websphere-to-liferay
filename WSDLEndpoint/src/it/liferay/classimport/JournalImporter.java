package it.liferay.classimport;

import it.liferay.config.Credentials;
import it.liferay.config.DefaultConfig;
import it.liferay.util.DescriptionHelper;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.xml.rpc.ServiceException;

import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.journal.model.JournalArticleSoap;
import com.liferay.portlet.journal.service.http.JournalArticleServiceSoap;
import com.liferay.portlet.journal.service.http.JournalArticleServiceSoapServiceLocator;
import com.liferay.portlet.journal.service.http.Portlet_Journal_JournalArticleServiceSoapBindingStub;

public class JournalImporter {
	private static JournalImporter singletonBlog = null;
	
	public static JournalImporter getInstance() {
		return getInstance(BLOGID);
	}
	
	public static JournalImporter getInstance(long blogId) {
		if ( singletonBlog == null ) {
			try {
				singletonBlog = new JournalImporter(blogId);
			} catch (ServiceException e) {
				e.printStackTrace();
				singletonBlog = null;
			}
		}
		
		return singletonBlog;
	}
	
	public static JournalImporter replaceInstance() {
		return replaceInstance(BLOGID);
	}
	
	public static JournalImporter replaceInstance(long blogId) {
		singletonBlog.blogId = blogId;
		return singletonBlog;
	}		
	
	//
	private static final String USERNAME = Credentials.USERNAME;
	private static final String PASSWORD = Credentials.PASSWORD;	
	private static final Long GROUPID = DefaultConfig.GROUPID;
	private static final Long BLOGID = DefaultConfig.BLOGID;
	@SuppressWarnings("unused")
	private static final Long COMPANYID = DefaultConfig.COMPANYID;
	
	// 
	/**
	 * @uml.property  name="locatorArticle"
	 * @uml.associationEnd  
	 */
	private JournalArticleServiceSoapServiceLocator locatorArticle;
	/**
	 * @uml.property  name="soapArticle"
	 * @uml.associationEnd  
	 */
	private JournalArticleServiceSoap soapArticle;
	/**
	 * @uml.property  name="blogId"
	 */
	@SuppressWarnings("unused")
	private long blogId;
	
	@SuppressWarnings("unused")
	private JournalImporter() throws ServiceException {
		this.locatorArticle = new JournalArticleServiceSoapServiceLocator();
    	this.soapArticle = this.locatorArticle.getPortlet_Journal_JournalArticleService();
        ((Portlet_Journal_JournalArticleServiceSoapBindingStub)this.soapArticle).setUsername(USERNAME);
        ((Portlet_Journal_JournalArticleServiceSoapBindingStub)this.soapArticle).setPassword(PASSWORD);
		if (DefaultConfig.OVERRIDE_SOAP_TIMEOUT == true) {
			((Portlet_Journal_JournalArticleServiceSoapBindingStub) this.soapArticle)
			.setTimeout(DefaultConfig.SOAP_TIMEOUT * 1000);			
		}        
	}
	
	private JournalImporter(long blogId) throws ServiceException {
		this();
		this.blogId = blogId;
	}
	
	/**
	 * 
	 * @param title
	 * @param body must be an html string
	 * @return
	 */
	public long addArticle(String title, String body) {
		Calendar today = Calendar.getInstance();
		
		//Controllare come passare structureId e templateId
		return this.addArticle(title, body, BlogArticleType.General, 
				"", "", "", 
				today.get(Calendar.MONTH), 
				today.get(Calendar.DAY_OF_MONTH), 
				today.get(Calendar.YEAR), 
				today.get(Calendar.HOUR_OF_DAY), 
				today.get(Calendar.MINUTE), 
				1, 1, 1970, 0, 0, 
				false, 
				today.get(Calendar.MONTH), 
				today.get(Calendar.DAY_OF_MONTH),
				today.get(Calendar.YEAR), 
				today.get(Calendar.HOUR_OF_DAY), 
				today.get(Calendar.MINUTE), 
				true, true, "");
	}
	
	public long addArticle(String title, String body,
			BlogArticleType typeBlog, String structureId, String templateId, String layoutUUID,
			int displayDateMonth, int displayDateDay, int displayDateYear,
			int displayDateHour, int displayDateMinute,
			int expirationDateMonth, int expirationDateDay, int expirationDateYear,
			int expirationDateHour, int expirationDateMinute, boolean doesExpire,
			int reviewDateMonth, int reviewDateDay, int reviewDateYear, 
			int reviewDateHour, int reviewDateMinute,
			boolean neverReview, boolean indexable, String articleURL ) {
		
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setScopeGroupId(GROUPID);
		serviceContext.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);
		int i;
		
	//   String[] titleMapLanguageIds = {"it_IT"}, titleMapValues = {"Titolo In Italiano"};
    //     String[] descriptionMapLanguageIds = {"it_IT"}, descriptionMapValues = {"Description in US"};

		String[] titleMapValues= DescriptionHelper.setAndGetTitlesFromLanguageIds(title);
		
		long classNameId = 0; // this.blogId;
		long classPk = 0L;
		String articleId = null;
		long retValue = DefaultConfig.RETERROR;
		
		 try {
			JournalArticleSoap jas = this.soapArticle.addArticle(
					 GROUPID,
					 classNameId,
					 classPk, // long
					 "", // string
					 true, // autoArticleId
					 DefaultConfig.titleMapLanguageIds, //new String[0], 
					 titleMapValues, // new String[], 
					 new String[0],// DefaultConfig.descriptionMapLanguageIds, // new String[], descriptionMapLanguagesId
					 new String[0], // new String[], descriptionMapValues
					 // title, // title
					 // description, //description
					 body, //content
					 typeBlog.toString(),
					 structureId, // ""
					 templateId, // ""
					 layoutUUID, // ""
					 displayDateMonth, displayDateDay, displayDateYear,
					 displayDateHour, displayDateMinute,
					 expirationDateMonth, expirationDateDay, expirationDateYear,
					 expirationDateHour, expirationDateMinute, // 0
					 doesExpire, // true: never expire
					 reviewDateMonth, reviewDateDay, reviewDateYear, reviewDateHour, reviewDateMinute, // 0
					 neverReview, // true
					 indexable, // true
					 articleURL, // ""
					 serviceContext);
			retValue = jas.getId();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return retValue;
	}
	
	/*
	 * company id: 10154
	 // http://pamir.dia.uniroma3.it:8080/liferay/group/control_panel/
	  * manage?p_p_id=161&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&
	  * doAsGroupId=10180&refererPlid=10183&_161_struts_action=%2Fblogs_admin%2Fview_entry&
	  * _161_redirect=http%3A%2F%2Fpamir.dia.uniroma3.it%3A8080%2Fliferay%2Fgroup%2Fcontrol_panel%2Fmanage%3Fp_p_id%3D161%26p_p_lifecycle%3D0%26p_p_state%3Dmaximized%26p_p_mode%3Dview%26doAsGroupId%3D10180%26refererPlid%3D10183%26_161_struts_action%3D%252Fblogs_admin%252Fview%26_161_cur%3D1%26_161_delta%3D20%26_161_keywords%3D%26_161_advancedSearch%3Dfalse%26_161_andOperator%3Dtrue%26_161_author%3D%26_161_status%3D%26_161_title%3D&_161_entryId=11043
	 * 
 JournalArticleServiceSoapServiceLocator locator = new JournalArticleServiceSoapServiceLocator();
 3        ServiceContext serviceContext = new ServiceContext();
 4        serviceContext.setScopeGroupId(GROUP_ID);
 5        serviceContext.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH); //so that the article is added as published and not draft
 6        JournalArticleServiceSoap soap = locator.getPortlet_Journal_JournalArticleService(LifeRayUtils.getURL(remoteUser, remotePassword, ARTICLE_ENTRY_SERVICE_NAME, server, port));
 7            
 8
 9                JournalArticleSoap jas = soap.addArticle(GROUP_ID,
10                                b.getBlogId().toString(),
11                                true, //autoArticleId
12                                b.getTitle(), //title
13                                null, //description
14                                b.getBody(), //content
15                                "news", //type: General, news, blogs, test, press release
16                                "", // structure id
17                                "", //template id
18                                displayDateMonth,
19                                displayDateDay,
20                                displayDateYear,
21                                displayDateHour,
22                                displayDateMinute,
23                                0, //expirationDateMonth
24                                0, //expirationDateDay
25                                0, //expirationDateYear
26                                0, //expirationDateHour
27                                0, //expirationDateMinute
28                                true, //never expire
29                                0, //reviewDateMonth
30                                0, //reviewDateDay
31                                0, //reviewDateYear
32                                0, //reviewDateHour
33                                0, //reviewDateMinute
34                                true, //neverReview
35                                true, //indexable
36                                "", //articleURL
37                                serviceContext);
	 */
}