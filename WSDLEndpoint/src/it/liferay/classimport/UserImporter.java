package it.liferay.classimport;

import it.liferay.config.Credentials;
import it.liferay.config.DefaultConfig;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.UserSoap;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.http.Portal_UserServiceSoapBindingStub;
import com.liferay.portal.service.http.UserServiceSoap;
import com.liferay.portal.service.http.UserServiceSoapServiceLocator;

public class UserImporter {

	private static UserImporter instance = null;

	public static UserImporter getInstance() {
		return getInstance(GROUPID);
	}

	public static UserImporter getInstance(long repositoryId) {
		if (instance == null) {
			try {
				instance = new UserImporter(repositoryId);
			} catch (ServiceException e) {
				e.printStackTrace();
				instance = null;
			}
		}

		return instance;
	}

	public static UserImporter replaceInstance() {
		return replaceInstance(GROUPID);
	}

	public static UserImporter replaceInstance(long repositoryId) {
		instance.repoId = repositoryId;
		return instance;
	}

	private static final String USERNAME = Credentials.USERNAME;
	private static final String PASSWORD = Credentials.PASSWORD;
	private static final Long GROUPID = DefaultConfig.GROUPID;
	@SuppressWarnings("unused")
	private static final Long ROOTFOLDER = DefaultConfig.ROOTFOLDER;
	private static final Long COMPANYID = DefaultConfig.COMPANYID;

	/**
	 * @uml.property  name="locatorUser"
	 * @uml.associationEnd  
	 */
	private UserServiceSoapServiceLocator locatorUser;
	/**
	 * @uml.property  name="soapUser"
	 * @uml.associationEnd  
	 */
	private UserServiceSoap soapUser;

	/**
	 * @uml.property  name="repoId"
	 */
	@SuppressWarnings("unused")
	private long repoId;

	@SuppressWarnings("unused")
	private UserImporter() throws ServiceException {

		this.locatorUser = new UserServiceSoapServiceLocator();
		this.soapUser = this.locatorUser.getPortal_UserService();
		((Portal_UserServiceSoapBindingStub) this.soapUser).setUsername(USERNAME);
		((Portal_UserServiceSoapBindingStub) this.soapUser).setPassword(PASSWORD);
		if (DefaultConfig.OVERRIDE_SOAP_TIMEOUT == true) {
			((Portal_UserServiceSoapBindingStub) this.soapUser)
			.setTimeout(DefaultConfig.SOAP_TIMEOUT * 1000);			
		}
	}

	private UserImporter(long repositoryId) throws ServiceException {
		this();
		this.repoId = repositoryId;
	}

	
	/**
	 * 
	 * @param userId
	 * @param password
	 * @return
	 */
	public boolean setUserPassword(long userId, String password){
		
		try {
			this.setUserPassword(this.soapUser.getUserById(userId),password);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}
	
	
	/**
	 * 
	 * @param userSoap
	 * @param password
	 */
	public void setUserPassword(UserSoap userSoap, String password){
		userSoap.setPassword(password);
	}
	
	
	/**
	 * 
	 * @param screenName
	 * @param emailAddress
	 * @param firstName
	 * @return
	 */
	public long addUserWithAutoPassword(String screenName, String emailAddress,
			String firstName) {
		return this.addUser(true, null, null, false, screenName, emailAddress,
				0, null, null, firstName, null, null, 0, 0, true, 1, 1, 1970,
				null, null, null, null, null, false, new ServiceContext());
	}
	
	/**
	 * 
	 * @param password
	 * @param screenName
	 * @param emailAddress
	 * @param firstName
	 * @return
	 */
	public long addUserWithPassword(String password, String screenName, String emailAddress,
			String firstName) {
		
		return this.addUser(false, password, password, false, screenName, emailAddress,
				0, new String(), null, firstName, null, null, 0, 0, true, 1, 1, 1970,
				null, null, null, null, null, false, new ServiceContext());
		
	}

	/**
	 * 
	 * @param autoPassword
	 * @param password1
	 * @param password2
	 * @param autoScreenName
	 * @param screenName
	 * @param emailAddress
	 * @param facebookId
	 * @param openId Must Not Be Null
	 * @param locale
	 * @param firstName
	 * @param middleName
	 * @param lastName
	 * @param prefixId
	 * @param suffixId
	 * @param male
	 * @param birthdayMonth
	 * @param birthdayDay
	 * @param birthdayYear
	 * @param jobTitle
	 * @param groupIds
	 * @param organizationIds
	 * @param roleIds
	 * @param userGroupIds
	 * @param sendEmail
	 * @param serviceContext
	 * @return
	 */
	public long addUser(boolean autoPassword, String password1,
			String password2, boolean autoScreenName, String screenName,
			String emailAddress, long facebookId, String openId, String locale,
			String firstName, String middleName, String lastName, int prefixId,
			int suffixId, boolean male, int birthdayMonth, int birthdayDay,
			int birthdayYear, String jobTitle, long[] groupIds,
			long[] organizationIds, long[] roleIds, long[] userGroupIds,
			boolean sendEmail, ServiceContext serviceContext) {

		UserSoap userSoap = null;

		try {

			ServiceContext srvCntx = new ServiceContext();
			srvCntx.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);
			
			userSoap = this.soapUser.addUser(COMPANYID, autoPassword,
					password1, password2, autoScreenName, screenName,
					emailAddress, facebookId, openId, locale, firstName,
					middleName, lastName, prefixId, suffixId, male,
					birthdayMonth, birthdayDay, birthdayYear, jobTitle,
					groupIds, organizationIds, roleIds, userGroupIds, sendEmail,
					srvCntx);

		} catch (RemoteException e) {
			e.printStackTrace();
			return DefaultConfig.RETERROR;
		}

		return userSoap.getUserId();

	}
}
