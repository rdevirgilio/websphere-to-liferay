package it.liferay.classimport;

import com.liferay.portlet.asset.model.AssetCategorySoap;
import it.liferay.config.DefaultConfig;

public class ImporterFacade {
	public static long ERRORCODE = DefaultConfig.RETERROR;
	
	public static long addStructure (Long parentStructureId, Long structureId, String title, String description){
		return StructureImporter.getInstance().addStructure(parentStructureId, structureId, title, description);
	}
	
	public static long addStructureInRoot(Long structureId, String title, String description){
		return addStructure(null,structureId, title, description);
	}
	
	public static long addDirectoryInRoot(String directoryName) {
		return addDirectoryInRoot(directoryName, "");
	}
	
	public static  long addCategoryRoot (java.lang.String title, java.lang.String description) {
		return addCategory(0, title, description);
	}
	
	public long addArticle(String title, String body){
		return JournalImporter.getInstance().addArticle(title, body);
	}
	
	public long addTemplate(String title, String structureId){
		return TemplateImporter.getInstance().addTemplate(title, structureId);
	}
	
	public static  long addCategory (long parentCategoryId, java.lang.String title, java.lang.String description) {
		return CategoryImporter.getInstance().addCategory(parentCategoryId, title, description);
	}
	
	public static long addDirectoryInRoot(String directoryName, String description) {
		return DirectoryImporter.getInstance().addFolderInRoot(directoryName, description);
	}
	
	public static long addDirectory(String directoryName, long parentDirectory) {
		return addDirectory(directoryName, "", parentDirectory);
	}
	
	public static long addDirectory(String directoryName, String description, long parentDirectory) {
		return DirectoryImporter.getInstance().addFolder(directoryName, description, parentDirectory);
	}	
	
	public static long addFileFromPath(String pathSource, long folderId, String name,
			String mimeType) {
		return addFileFromPath(pathSource, folderId, name, mimeType, "");
	}
	
	public static long addFileFromPath(String pathSource, long folderId, String name,
			String mimeType, String title) {
		return addFileFromPath(pathSource, folderId, name, mimeType, title, "");
	}	
	
	public static long addFileFromPath(String pathSource, long folderId, String name,
			String mimeType, String title, String description) {
		return addFileFromPath(pathSource, folderId, name, mimeType, title, description, "");
	}
	
	public static long addFileFromPath(String pathSource, long folderId, String name,
			String mimeType, String title, String description, String changelog) {
		return FileImporter.getInstance().addFileEntryFromPath(folderId, pathSource, name, mimeType, title, description, changelog);
	}	
	
	public static long addFileFromBase64(String base64Content, long folderId, String name,
			String mimeType) {
		return addFileFromBase64(base64Content, folderId, name, mimeType, "");
	}
	
	public static long addFileFromBase64(String base64Content, long folderId, String name,
			String mimeType, String title) {
		return addFileFromBase64(base64Content, folderId, name, mimeType, title, "");
	}	
	
	public static long addFileFromBase64(String base64Content, long folderId, String name,
			String mimeType, String title, String description) {
		return addFileFromBase64(base64Content, folderId, name, mimeType, title, description, "");
	}
	
	public static long addFileFromBase64(String base64Content, long folderId, String name,
			String mimeType, String title, String description, String changelog) {
		return FileImporter.getInstance().addFileEntryFromBase64String(folderId, base64Content, name, mimeType, title, description, changelog);
	}		
}
