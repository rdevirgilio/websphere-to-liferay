package it.liferay.classimport;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import it.liferay.config.Credentials;
import it.liferay.config.DefaultConfig;
import it.liferay.util.DescriptionHelper;

import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.asset.model.AssetCategorySoap;
import com.liferay.portlet.journal.model.JournalStructureSoap;
import com.liferay.portlet.journal.service.http.JournalStructureServiceSoap;
import com.liferay.portlet.journal.service.http.JournalStructureServiceSoapServiceLocator;
import com.liferay.portlet.journal.service.http.Portlet_Journal_JournalArticleServiceSoapBindingStub;
import com.liferay.portlet.journal.service.http.Portlet_Journal_JournalStructureServiceSoapBindingStub;

public class StructureImporter {

	private static StructureImporter singletonStructure = null;
	
	private static final String USERNAME = Credentials.USERNAME;
	private static final String PASSWORD = Credentials.PASSWORD;	
	private static final Long GROUPID = DefaultConfig.GROUPID;
	private static final String xmlSchema = "<?xml version=\"1.0\"?> <root> <dynamic-element name=\"empty\" type=\"text\" index-type=\"\" repeatable=\"false\"/></root>";
	private String structureId;
	private String parentStructureId;
	private JournalStructureServiceSoap soapStructure; 	
	private JournalStructureServiceSoapServiceLocator locatorStructure;
	
	public StructureImporter () throws ServiceException{
		this.locatorStructure = new JournalStructureServiceSoapServiceLocator();
		this.soapStructure = this.locatorStructure.getPortlet_Journal_JournalStructureService();
		((Portlet_Journal_JournalStructureServiceSoapBindingStub)this.soapStructure).setUsername(USERNAME);
		((Portlet_Journal_JournalStructureServiceSoapBindingStub)this.soapStructure).setPassword(PASSWORD);
		if (DefaultConfig.OVERRIDE_SOAP_TIMEOUT == true) {
			((Portlet_Journal_JournalStructureServiceSoapBindingStub) this.soapStructure)
			.setTimeout(DefaultConfig.SOAP_TIMEOUT * 1000);			
		}     
	}

	public  StructureImporter (String structureId) throws ServiceException{
		this();
		this.structureId = structureId;
	}
	public static StructureImporter getInstance() {
		return getInstance(GROUPID);
	}
	
	
	public static StructureImporter getInstance(Long structureId) {
		if ( singletonStructure == null ) {
			try {
				singletonStructure = new StructureImporter (String.valueOf(structureId));
			} catch (ServiceException e) {
				e.printStackTrace();
				singletonStructure = null;
			}
		}
		return singletonStructure;
	}
	
	
	//Controllare se createdStructure.getId() � il metodo giusto per avere l'id
	public long addStructure (Long parentStructureId, Long structureId, String title, String description) {
		long retValue = structureId;
		JournalStructureSoap createdStructure= null;
		ServiceContext srvCntx = null;
		this.structureId = String.valueOf(structureId);
		this.parentStructureId = String.valueOf(parentStructureId);
		String[] titleMapValues = DescriptionHelper.setAndGetTitlesFromLanguageIds(title);
		String[] descriptionMapValues = DescriptionHelper.setAndGetDescriptionsFromLanguageIds(description); 
		try {
			srvCntx = new ServiceContext();
			srvCntx.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);
			createdStructure = this.soapStructure.addStructure(GROUPID, this.structureId, false, this.parentStructureId, DefaultConfig.titleMapLanguageIds, titleMapValues, DefaultConfig.descriptionMapLanguageIds, descriptionMapValues, xmlSchema, srvCntx);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return retValue;
	}
	
}
