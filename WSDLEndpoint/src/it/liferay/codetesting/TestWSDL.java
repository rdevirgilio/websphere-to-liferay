package it.liferay.codetesting;

import it.liferay.classimport.CategoryImporter;
import it.liferay.classimport.JournalImporter;
import it.liferay.classimport.DirectoryImporter;
import it.liferay.classimport.FileImporter;
import it.liferay.classimport.StructureImporter;
import it.liferay.classimport.UserImporter;

import java.net.URL;
import java.util.List;

import com.liferay.portal.kernel.repository.model.FileEntrySoap;
import com.liferay.portal.model.UserSoap;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.http.*;
import com.liferay.portlet.documentlibrary.service.http.DLAppServiceSoap;
import com.liferay.portlet.documentlibrary.service.http.DLAppServiceSoapServiceLocator;
import com.liferay.portlet.documentlibrary.service.http.DLFolderServiceSoap;
import com.liferay.portlet.documentlibrary.service.http.DLFolderServiceSoapServiceLocator;
import com.liferay.portlet.documentlibrary.service.http.Portlet_DL_DLAppServiceSoapBindingStub;
import com.liferay.portlet.documentlibrary.service.http.Portlet_DL_DLFolderServiceSoapBindingStub;

public class TestWSDL {
	private static String USERNAME = "servlet@liferay.com";
	private static String PASSWORD = "servlet";
	private static Long GROUPID = new Long(10180);
	private static Long ROOTFOLDER = new Long(0);

    public static void main(String[] args) {
    	testStructure();
   }
    
      

    private static void testStructure(){
    	try{
    		Long resData = StructureImporter.getInstance().addStructure(null, null, "structureProva1", "desc una prova");
    		System.out.print(resData);
    	} catch (Exception e){
    		e.printStackTrace();
    	}
    	
    	
    }
    
    private static void testCategory() {
        try {
        	long resData = CategoryImporter.getInstance().addCategory(0, "categoriaProva4", "unaProvaNuova");
        	//CategoryImporter.getInstance().addCategory(resData, "FiglioProva4", "descfiglioProva4");
        		 System.out.println(resData);
       } catch (Exception e) {
          e.printStackTrace();
       } 
    }    
    
    
    private static void testAddBlog() {
        try {
        	long resData = JournalImporter.getInstance().addArticle("AltraProvaLocaleMap", "<h1>TitleMap</h1>");
        		 System.out.println(resData);
       } catch (Exception e) {
          e.printStackTrace();
       } 
    }      
    
    private static void testAddUser() {
        try {
        	long resData = UserImporter.getInstance().addUserWithPassword("pass", "utenteprova", "prova@prova.it", "stefano");
            System.out.println(resData);
       } catch (Exception e) {
          e.printStackTrace();
       } 
    }  
    
    private static void testAddFolder() {
        try {
        	long resData = DirectoryImporter.getInstance().addFolderInRoot("test", "Descrizione bellissima");
        	byte[] bData = new byte[4096];
        	for(int i = 0; i < bData.length; i++) {
        		bData[i] = 'a';
        	}
            FileImporter.getInstance().addFileEntry(resData, bData, "spacca.txt", "application/txt", "Spacca", "fa la rima con...", null);
            
       } catch (Exception e) {
          e.printStackTrace();
       } 
    }    
   
   
   /*
    *
    // No need use the Portlet_DL_DLAppServiceSoapBindingStub trick
	private static URL _getURL(String remoteUser, String password, String serviceName) throws Exception { 
		// Unathenticated url 
		String url = "http://pamir.dia.uniroma3.it:8080/liferay/api/axis/" + serviceName; 
		// Authenticated url 
		if (true) { 
			url = "http://" + remoteUser + ":" + password + "@pamir.dia.uniroma3.it:8080/liferay/api/secure/axis/" + serviceName; 
		} 
		
		return new URL(url); 
	}
	*/
		
}
