package it.liferay.config;

public class ServerProperties {
	
	public static final String Portlet_DL_DLAppService_address = "http://pamir.dia.uniroma3.it:8080/liferay/api/secure/axis/Portlet_DL_DLAppService";
	public static final String Portlet_DL_DLFileShortcutService_address = "http://pamir.dia.uniroma3.it:8080/liferay/api/secure/axis/Portlet_DL_DLFileShortcutService";
	public static final String Portlet_DL_DLFolderService_address = "http://pamir.dia.uniroma3.it:8080/liferay/api/secure/axis/Portlet_DL_DLFolderService";
	public static final String Portlet_Journal_JournalArticleService_address = "http://pamir.dia.uniroma3.it:8080/liferay/api/secure/axis/Portlet_Journal_JournalArticleService";
	public static final String Portal_UserService_address = "http://pamir.dia.uniroma3.it:8080/liferay/api/secure/axis/Portal_UserService";
	public static final String Portlet_Wiki_WikiPageService_address = "http://pamir.dia.uniroma3.it:8080/liferay/api/secure/axis/Portlet_Wiki_WikiPageService";
	public static final String Portlet_Asset_AssetCategoryPropertyService_address = "http://pamir.dia.uniroma3.it:8080/liferay/api/secure/axis/Portlet_Asset_AssetCategoryService";
}
