package it.liferay.config;

public class DefaultConfig {
	private DefaultConfig() {} 
	//
	public static final Long GROUPID = new Long(10180);
	public static final Long REPOSITORYID = new Long(10180);
	public static final Long ROOTFOLDER = new Long(0);
	public static final Long COMPANYID = new Long(10154);
	public static final Long BLOGID = COMPANYID;
	//
	public static final String[] titleMapLanguageIds = {"en_US","it_IT"};
	public static final String[] descriptionMapLanguageIds = {"en_US","it_IT"};
	public static final Long RETERROR = new Long(-1);
	//
	public static final boolean OVERRIDE_SOAP_TIMEOUT = true; //true for custom timeout
	public static final int SOAP_TIMEOUT = 80; // seconds
}
