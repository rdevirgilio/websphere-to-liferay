package it.liferay.util;

import it.liferay.config.DefaultConfig;

public class DescriptionHelper {

	public static String[] setAndGetTitlesFromLanguageIds( String title) {

		String[] titleMapValues = new String[getNumberOfLanguagesId()];
		int i;
		for (i = 0; i < titleMapValues.length; i++) {
			titleMapValues[i] = title;
		}

		return titleMapValues;
	}

	public static String[] setAndGetDescriptionsFromLanguageIds(String description) {

		String[] titleMapValues = new String[getNumberOfLanguagesId()];

		int i;
		for (i = 0; i < titleMapValues.length; i++) {
			titleMapValues[i] = description;
		}

		return titleMapValues;
	}

	public static int getNumberOfLanguagesId() {

		return DefaultConfig.titleMapLanguageIds.length;
	}

}
