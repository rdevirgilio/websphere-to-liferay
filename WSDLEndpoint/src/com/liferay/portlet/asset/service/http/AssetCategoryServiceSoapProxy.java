package com.liferay.portlet.asset.service.http;

public class AssetCategoryServiceSoapProxy implements com.liferay.portlet.asset.service.http.AssetCategoryServiceSoap {
  private String _endpoint = null;
  private com.liferay.portlet.asset.service.http.AssetCategoryServiceSoap assetCategoryServiceSoap = null;
  
  public AssetCategoryServiceSoapProxy() {
    _initAssetCategoryServiceSoapProxy();
  }
  
  public AssetCategoryServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initAssetCategoryServiceSoapProxy();
  }
  
  private void _initAssetCategoryServiceSoapProxy() {
    try {
      assetCategoryServiceSoap = (new com.liferay.portlet.asset.service.http.AssetCategoryServiceSoapServiceLocator()).getPortlet_Asset_AssetCategoryService();
      if (assetCategoryServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)assetCategoryServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)assetCategoryServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (assetCategoryServiceSoap != null)
      ((javax.xml.rpc.Stub)assetCategoryServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.liferay.portlet.asset.service.http.AssetCategoryServiceSoap getAssetCategoryServiceSoap() {
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap;
  }
  
  public com.liferay.portlet.asset.model.AssetCategorySoap addCategory(long parentCategoryId, java.lang.String[] titleMapLanguageIds, java.lang.String[] titleMapValues, java.lang.String[] descriptionMapLanguageIds, java.lang.String[] descriptionMapValues, long vocabularyId, java.lang.String[] categoryProperties, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.addCategory(parentCategoryId, titleMapLanguageIds, titleMapValues, descriptionMapLanguageIds, descriptionMapValues, vocabularyId, categoryProperties, serviceContext);
  }
  
  public void deleteCategories(long[] categoryIds) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    assetCategoryServiceSoap.deleteCategories(categoryIds);
  }
  
  public void deleteCategory(long categoryId) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    assetCategoryServiceSoap.deleteCategory(categoryId);
  }
  
  public com.liferay.portlet.asset.model.AssetCategorySoap[] getCategories(java.lang.String className, long classPK) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.getCategories(className, classPK);
  }
  
  public com.liferay.portlet.asset.model.AssetCategorySoap getCategory(long categoryId) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.getCategory(categoryId);
  }
  
  public com.liferay.portlet.asset.model.AssetCategorySoap[] getChildCategories(long parentCategoryId) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.getChildCategories(parentCategoryId);
  }
  
  public com.liferay.portlet.asset.model.AssetCategorySoap[] getChildCategories(long parentCategoryId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.getChildCategories(parentCategoryId, start, end, obc);
  }
  
  public java.lang.String getJSONSearch(long groupId, java.lang.String name, long[] vocabularyIds, int start, int end) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.getJSONSearch(groupId, name, vocabularyIds, start, end);
  }
  
  public java.lang.String getJSONSearch(long groupId, java.lang.String keywords, long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.getJSONSearch(groupId, keywords, vocabularyId, start, end, obc);
  }
  
  public java.lang.String getJSONVocabularyCategories(long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.getJSONVocabularyCategories(vocabularyId, start, end, obc);
  }
  
  public java.lang.String getJSONVocabularyCategories(long groupId, java.lang.String name, long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.getJSONVocabularyCategories(groupId, name, vocabularyId, start, end, obc);
  }
  
  public int getVocabularyCategoriesCount(long groupId, long vocabularyId) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.getVocabularyCategoriesCount(groupId, vocabularyId);
  }
  
  public int getVocabularyCategoriesCount(long groupId, java.lang.String name, long vocabularyId) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.getVocabularyCategoriesCount(groupId, name, vocabularyId);
  }
  
  public com.liferay.portlet.asset.model.AssetCategorySoap[] getVocabularyCategories(long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.getVocabularyCategories(vocabularyId, start, end, obc);
  }
  
  public com.liferay.portlet.asset.model.AssetCategorySoap[] getVocabularyCategories(long parentCategoryId, long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.getVocabularyCategories(parentCategoryId, vocabularyId, start, end, obc);
  }
  
  public com.liferay.portlet.asset.model.AssetCategorySoap[] getVocabularyCategories(long groupId, java.lang.String name, long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.getVocabularyCategories(groupId, name, vocabularyId, start, end, obc);
  }
  
  public com.liferay.portlet.asset.model.AssetCategorySoap[] getVocabularyRootCategories(long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.getVocabularyRootCategories(vocabularyId, start, end, obc);
  }
  
  public com.liferay.portlet.asset.model.AssetCategorySoap moveCategory(long categoryId, long parentCategoryId, long vocabularyId, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.moveCategory(categoryId, parentCategoryId, vocabularyId, serviceContext);
  }
  
  public java.lang.String search(long groupId, java.lang.String name, java.lang.String[] categoryProperties, int start, int end) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.search(groupId, name, categoryProperties, start, end);
  }
  
  public com.liferay.portlet.asset.model.AssetCategorySoap[] search(long groupId, java.lang.String keywords, long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.search(groupId, keywords, vocabularyId, start, end, obc);
  }
  
  public com.liferay.portlet.asset.model.AssetCategorySoap updateCategory(long categoryId, long parentCategoryId, java.lang.String[] titleMapLanguageIds, java.lang.String[] titleMapValues, java.lang.String[] descriptionMapLanguageIds, java.lang.String[] descriptionMapValues, long vocabularyId, java.lang.String[] categoryProperties, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (assetCategoryServiceSoap == null)
      _initAssetCategoryServiceSoapProxy();
    return assetCategoryServiceSoap.updateCategory(categoryId, parentCategoryId, titleMapLanguageIds, titleMapValues, descriptionMapLanguageIds, descriptionMapValues, vocabularyId, categoryProperties, serviceContext);
  }
  
  
}