/**
 * AssetCategoryServiceSoapService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.liferay.portlet.asset.service.http;

public interface AssetCategoryServiceSoapService extends javax.xml.rpc.Service {
    public java.lang.String getPortlet_Asset_AssetCategoryServiceAddress();

    public com.liferay.portlet.asset.service.http.AssetCategoryServiceSoap getPortlet_Asset_AssetCategoryService() throws javax.xml.rpc.ServiceException;

    public com.liferay.portlet.asset.service.http.AssetCategoryServiceSoap getPortlet_Asset_AssetCategoryService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
