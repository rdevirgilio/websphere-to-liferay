/**
 * AssetCategoryServiceSoapServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.liferay.portlet.asset.service.http;

import it.liferay.config.ServerProperties;

public class AssetCategoryServiceSoapServiceLocator extends org.apache.axis.client.Service implements com.liferay.portlet.asset.service.http.AssetCategoryServiceSoapService {

    public AssetCategoryServiceSoapServiceLocator() {
    }


    public AssetCategoryServiceSoapServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public AssetCategoryServiceSoapServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for Portlet_Asset_AssetCategoryService
    private java.lang.String Portlet_Asset_AssetCategoryService_address = ServerProperties.Portlet_Asset_AssetCategoryPropertyService_address;

    public java.lang.String getPortlet_Asset_AssetCategoryServiceAddress() {
        return Portlet_Asset_AssetCategoryService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String Portlet_Asset_AssetCategoryServiceWSDDServiceName = "Portlet_Asset_AssetCategoryService";

    public java.lang.String getPortlet_Asset_AssetCategoryServiceWSDDServiceName() {
        return Portlet_Asset_AssetCategoryServiceWSDDServiceName;
    }

    public void setPortlet_Asset_AssetCategoryServiceWSDDServiceName(java.lang.String name) {
        Portlet_Asset_AssetCategoryServiceWSDDServiceName = name;
    }

    public com.liferay.portlet.asset.service.http.AssetCategoryServiceSoap getPortlet_Asset_AssetCategoryService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(Portlet_Asset_AssetCategoryService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPortlet_Asset_AssetCategoryService(endpoint);
    }

    public com.liferay.portlet.asset.service.http.AssetCategoryServiceSoap getPortlet_Asset_AssetCategoryService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.liferay.portlet.asset.service.http.Portlet_Asset_AssetCategoryServiceSoapBindingStub _stub = new com.liferay.portlet.asset.service.http.Portlet_Asset_AssetCategoryServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getPortlet_Asset_AssetCategoryServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPortlet_Asset_AssetCategoryServiceEndpointAddress(java.lang.String address) {
        Portlet_Asset_AssetCategoryService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.liferay.portlet.asset.service.http.AssetCategoryServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.liferay.portlet.asset.service.http.Portlet_Asset_AssetCategoryServiceSoapBindingStub _stub = new com.liferay.portlet.asset.service.http.Portlet_Asset_AssetCategoryServiceSoapBindingStub(new java.net.URL(Portlet_Asset_AssetCategoryService_address), this);
                _stub.setPortName(getPortlet_Asset_AssetCategoryServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("Portlet_Asset_AssetCategoryService".equals(inputPortName)) {
            return getPortlet_Asset_AssetCategoryService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:http.service.asset.portlet.liferay.com", "AssetCategoryServiceSoapService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:http.service.asset.portlet.liferay.com", "Portlet_Asset_AssetCategoryService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("Portlet_Asset_AssetCategoryService".equals(portName)) {
            setPortlet_Asset_AssetCategoryServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
