/**
 * AssetCategoryServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.liferay.portlet.asset.service.http;

public interface AssetCategoryServiceSoap extends java.rmi.Remote {
    public com.liferay.portlet.asset.model.AssetCategorySoap addCategory(long parentCategoryId, java.lang.String[] titleMapLanguageIds, java.lang.String[] titleMapValues, java.lang.String[] descriptionMapLanguageIds, java.lang.String[] descriptionMapValues, long vocabularyId, java.lang.String[] categoryProperties, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException;
    public void deleteCategories(long[] categoryIds) throws java.rmi.RemoteException;
    public void deleteCategory(long categoryId) throws java.rmi.RemoteException;
    public com.liferay.portlet.asset.model.AssetCategorySoap[] getCategories(java.lang.String className, long classPK) throws java.rmi.RemoteException;
    public com.liferay.portlet.asset.model.AssetCategorySoap getCategory(long categoryId) throws java.rmi.RemoteException;
    public com.liferay.portlet.asset.model.AssetCategorySoap[] getChildCategories(long parentCategoryId) throws java.rmi.RemoteException;
    public com.liferay.portlet.asset.model.AssetCategorySoap[] getChildCategories(long parentCategoryId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException;
    public java.lang.String getJSONSearch(long groupId, java.lang.String name, long[] vocabularyIds, int start, int end) throws java.rmi.RemoteException;
    public java.lang.String getJSONSearch(long groupId, java.lang.String keywords, long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException;
    public java.lang.String getJSONVocabularyCategories(long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException;
    public java.lang.String getJSONVocabularyCategories(long groupId, java.lang.String name, long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException;
    public int getVocabularyCategoriesCount(long groupId, long vocabularyId) throws java.rmi.RemoteException;
    public int getVocabularyCategoriesCount(long groupId, java.lang.String name, long vocabularyId) throws java.rmi.RemoteException;
    public com.liferay.portlet.asset.model.AssetCategorySoap[] getVocabularyCategories(long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException;
    public com.liferay.portlet.asset.model.AssetCategorySoap[] getVocabularyCategories(long parentCategoryId, long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException;
    public com.liferay.portlet.asset.model.AssetCategorySoap[] getVocabularyCategories(long groupId, java.lang.String name, long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException;
    public com.liferay.portlet.asset.model.AssetCategorySoap[] getVocabularyRootCategories(long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException;
    public com.liferay.portlet.asset.model.AssetCategorySoap moveCategory(long categoryId, long parentCategoryId, long vocabularyId, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException;
    public java.lang.String search(long groupId, java.lang.String name, java.lang.String[] categoryProperties, int start, int end) throws java.rmi.RemoteException;
    public com.liferay.portlet.asset.model.AssetCategorySoap[] search(long groupId, java.lang.String keywords, long vocabularyId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException;
    public com.liferay.portlet.asset.model.AssetCategorySoap updateCategory(long categoryId, long parentCategoryId, java.lang.String[] titleMapLanguageIds, java.lang.String[] titleMapValues, java.lang.String[] descriptionMapLanguageIds, java.lang.String[] descriptionMapValues, long vocabularyId, java.lang.String[] categoryProperties, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException;
}
