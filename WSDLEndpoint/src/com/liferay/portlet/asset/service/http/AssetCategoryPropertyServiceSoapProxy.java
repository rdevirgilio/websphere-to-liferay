package com.liferay.portlet.asset.service.http;

public class AssetCategoryPropertyServiceSoapProxy implements com.liferay.portlet.asset.service.http.AssetCategoryPropertyServiceSoap {
  private String _endpoint = null;
  private com.liferay.portlet.asset.service.http.AssetCategoryPropertyServiceSoap assetCategoryPropertyServiceSoap = null;
  
  public AssetCategoryPropertyServiceSoapProxy() {
    _initAssetCategoryPropertyServiceSoapProxy();
  }
  
  public AssetCategoryPropertyServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initAssetCategoryPropertyServiceSoapProxy();
  }
  
  private void _initAssetCategoryPropertyServiceSoapProxy() {
    try {
      assetCategoryPropertyServiceSoap = (new com.liferay.portlet.asset.service.http.AssetCategoryPropertyServiceSoapServiceLocator()).getPortlet_Asset_AssetCategoryPropertyService();
      if (assetCategoryPropertyServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)assetCategoryPropertyServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)assetCategoryPropertyServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (assetCategoryPropertyServiceSoap != null)
      ((javax.xml.rpc.Stub)assetCategoryPropertyServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.liferay.portlet.asset.service.http.AssetCategoryPropertyServiceSoap getAssetCategoryPropertyServiceSoap() {
    if (assetCategoryPropertyServiceSoap == null)
      _initAssetCategoryPropertyServiceSoapProxy();
    return assetCategoryPropertyServiceSoap;
  }
  
  public com.liferay.portlet.asset.model.AssetCategoryPropertySoap addCategoryProperty(long entryId, java.lang.String key, java.lang.String value) throws java.rmi.RemoteException{
    if (assetCategoryPropertyServiceSoap == null)
      _initAssetCategoryPropertyServiceSoapProxy();
    return assetCategoryPropertyServiceSoap.addCategoryProperty(entryId, key, value);
  }
  
  public void deleteCategoryProperty(long categoryPropertyId) throws java.rmi.RemoteException{
    if (assetCategoryPropertyServiceSoap == null)
      _initAssetCategoryPropertyServiceSoapProxy();
    assetCategoryPropertyServiceSoap.deleteCategoryProperty(categoryPropertyId);
  }
  
  public com.liferay.portlet.asset.model.AssetCategoryPropertySoap[] getCategoryProperties(long entryId) throws java.rmi.RemoteException{
    if (assetCategoryPropertyServiceSoap == null)
      _initAssetCategoryPropertyServiceSoapProxy();
    return assetCategoryPropertyServiceSoap.getCategoryProperties(entryId);
  }
  
  public com.liferay.portlet.asset.model.AssetCategoryPropertySoap[] getCategoryPropertyValues(long companyId, java.lang.String key) throws java.rmi.RemoteException{
    if (assetCategoryPropertyServiceSoap == null)
      _initAssetCategoryPropertyServiceSoapProxy();
    return assetCategoryPropertyServiceSoap.getCategoryPropertyValues(companyId, key);
  }
  
  public com.liferay.portlet.asset.model.AssetCategoryPropertySoap updateCategoryProperty(long categoryPropertyId, java.lang.String key, java.lang.String value) throws java.rmi.RemoteException{
    if (assetCategoryPropertyServiceSoap == null)
      _initAssetCategoryPropertyServiceSoapProxy();
    return assetCategoryPropertyServiceSoap.updateCategoryProperty(categoryPropertyId, key, value);
  }
  
  
}