/**
 * AssetCategoryPropertyServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.liferay.portlet.asset.service.http;

public interface AssetCategoryPropertyServiceSoap extends java.rmi.Remote {
    public com.liferay.portlet.asset.model.AssetCategoryPropertySoap addCategoryProperty(long entryId, java.lang.String key, java.lang.String value) throws java.rmi.RemoteException;
    public void deleteCategoryProperty(long categoryPropertyId) throws java.rmi.RemoteException;
    public com.liferay.portlet.asset.model.AssetCategoryPropertySoap[] getCategoryProperties(long entryId) throws java.rmi.RemoteException;
    public com.liferay.portlet.asset.model.AssetCategoryPropertySoap[] getCategoryPropertyValues(long companyId, java.lang.String key) throws java.rmi.RemoteException;
    public com.liferay.portlet.asset.model.AssetCategoryPropertySoap updateCategoryProperty(long categoryPropertyId, java.lang.String key, java.lang.String value) throws java.rmi.RemoteException;
}
