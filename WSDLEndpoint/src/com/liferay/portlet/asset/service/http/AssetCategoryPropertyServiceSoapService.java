/**
 * AssetCategoryPropertyServiceSoapService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.liferay.portlet.asset.service.http;

public interface AssetCategoryPropertyServiceSoapService extends javax.xml.rpc.Service {
    public java.lang.String getPortlet_Asset_AssetCategoryPropertyServiceAddress();

    public com.liferay.portlet.asset.service.http.AssetCategoryPropertyServiceSoap getPortlet_Asset_AssetCategoryPropertyService() throws javax.xml.rpc.ServiceException;

    public com.liferay.portlet.asset.service.http.AssetCategoryPropertyServiceSoap getPortlet_Asset_AssetCategoryPropertyService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
