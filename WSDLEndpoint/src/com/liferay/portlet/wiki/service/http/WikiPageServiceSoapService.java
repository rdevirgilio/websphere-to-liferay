/**
 * WikiPageServiceSoapService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.liferay.portlet.wiki.service.http;

/**
 * @author  stefano
 */
public interface WikiPageServiceSoapService extends javax.xml.rpc.Service {
    /**
	 * @uml.property  name="portlet_Wiki_WikiPageServiceAddress"
	 */
    public java.lang.String getPortlet_Wiki_WikiPageServiceAddress();

    /**
	 * @uml.property  name="portlet_Wiki_WikiPageService"
	 * @uml.associationEnd  
	 */
    public com.liferay.portlet.wiki.service.http.WikiPageServiceSoap getPortlet_Wiki_WikiPageService() throws javax.xml.rpc.ServiceException;

    public com.liferay.portlet.wiki.service.http.WikiPageServiceSoap getPortlet_Wiki_WikiPageService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
