package com.liferay.portlet.wiki.service.http;

public class WikiPageServiceSoapProxy implements com.liferay.portlet.wiki.service.http.WikiPageServiceSoap {
  /**
 * @uml.property  name="_endpoint"
 */
private String _endpoint = null;
  /**
 * @uml.property  name="wikiPageServiceSoap"
 * @uml.associationEnd  multiplicity="(1 1)"
 */
private com.liferay.portlet.wiki.service.http.WikiPageServiceSoap wikiPageServiceSoap = null;
  
  public WikiPageServiceSoapProxy() {
    _initWikiPageServiceSoapProxy();
  }
  
  public WikiPageServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initWikiPageServiceSoapProxy();
  }
  
  private void _initWikiPageServiceSoapProxy() {
    try {
      wikiPageServiceSoap = (new com.liferay.portlet.wiki.service.http.WikiPageServiceSoapServiceLocator()).getPortlet_Wiki_WikiPageService();
      if (wikiPageServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wikiPageServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wikiPageServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wikiPageServiceSoap != null)
      ((javax.xml.rpc.Stub)wikiPageServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  /**
 * @return
 * @uml.property  name="wikiPageServiceSoap"
 */
public com.liferay.portlet.wiki.service.http.WikiPageServiceSoap getWikiPageServiceSoap() {
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    return wikiPageServiceSoap;
  }
  
  public void addPageAttachments(long nodeId, java.lang.String title, java.lang.Object[] inputStream) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    wikiPageServiceSoap.addPageAttachments(nodeId, title, inputStream);
  }
  
  public com.liferay.portlet.wiki.model.WikiPageSoap addPage(long nodeId, java.lang.String title, java.lang.String content, java.lang.String summary, boolean minorEdit, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    return wikiPageServiceSoap.addPage(nodeId, title, content, summary, minorEdit, serviceContext);
  }
  
  public com.liferay.portlet.wiki.model.WikiPageSoap addPage(long nodeId, java.lang.String title, java.lang.String content, java.lang.String summary, boolean minorEdit, java.lang.String format, java.lang.String parentTitle, java.lang.String redirectTitle, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    return wikiPageServiceSoap.addPage(nodeId, title, content, summary, minorEdit, format, parentTitle, redirectTitle, serviceContext);
  }
  
  public void changeParent(long nodeId, java.lang.String title, java.lang.String newParentTitle, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    wikiPageServiceSoap.changeParent(nodeId, title, newParentTitle, serviceContext);
  }
  
  public void deletePageAttachment(long nodeId, java.lang.String title, java.lang.String fileName) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    wikiPageServiceSoap.deletePageAttachment(nodeId, title, fileName);
  }
  
  public void deletePage(long nodeId, java.lang.String title) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    wikiPageServiceSoap.deletePage(nodeId, title);
  }
  
  public void deletePage(long nodeId, java.lang.String title, double version) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    wikiPageServiceSoap.deletePage(nodeId, title, version);
  }
  
  public void deleteTempPageAttachment(long nodeId, java.lang.String fileName, java.lang.String tempFolderName) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    wikiPageServiceSoap.deleteTempPageAttachment(nodeId, fileName, tempFolderName);
  }
  
  public com.liferay.portlet.wiki.model.WikiPageSoap getDraftPage(long nodeId, java.lang.String title) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    return wikiPageServiceSoap.getDraftPage(nodeId, title);
  }
  
  public java.lang.String getNodePagesRSS(long nodeId, int max, java.lang.String type, double version, java.lang.String displayStyle, java.lang.String feedURL, java.lang.String entryURL) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    return wikiPageServiceSoap.getNodePagesRSS(nodeId, max, type, version, displayStyle, feedURL, entryURL);
  }
  
  public com.liferay.portlet.wiki.model.WikiPageSoap[] getNodePages(long nodeId, int max) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    return wikiPageServiceSoap.getNodePages(nodeId, max);
  }
  
  public com.liferay.portlet.wiki.model.WikiPageSoap getPage(long nodeId, java.lang.String title) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    return wikiPageServiceSoap.getPage(nodeId, title);
  }
  
  public com.liferay.portlet.wiki.model.WikiPageSoap getPage(long nodeId, java.lang.String title, boolean head) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    return wikiPageServiceSoap.getPage(nodeId, title, head);
  }
  
  public com.liferay.portlet.wiki.model.WikiPageSoap getPage(long nodeId, java.lang.String title, double version) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    return wikiPageServiceSoap.getPage(nodeId, title, version);
  }
  
  public java.lang.String getPagesRSS(long companyId, long nodeId, java.lang.String title, int max, java.lang.String type, double version, java.lang.String displayStyle, java.lang.String feedURL, java.lang.String entryURL, java.lang.String locale) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    return wikiPageServiceSoap.getPagesRSS(companyId, nodeId, title, max, type, version, displayStyle, feedURL, entryURL, locale);
  }
  
  public java.lang.String[] getTempPageAttachmentNames(long nodeId, java.lang.String tempFolderName) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    return wikiPageServiceSoap.getTempPageAttachmentNames(nodeId, tempFolderName);
  }
  
  public void movePage(long nodeId, java.lang.String title, java.lang.String newTitle, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    wikiPageServiceSoap.movePage(nodeId, title, newTitle, serviceContext);
  }
  
  public com.liferay.portlet.wiki.model.WikiPageSoap revertPage(long nodeId, java.lang.String title, double version, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    return wikiPageServiceSoap.revertPage(nodeId, title, version, serviceContext);
  }
  
  public void subscribePage(long nodeId, java.lang.String title) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    wikiPageServiceSoap.subscribePage(nodeId, title);
  }
  
  public void unsubscribePage(long nodeId, java.lang.String title) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    wikiPageServiceSoap.unsubscribePage(nodeId, title);
  }
  
  public com.liferay.portlet.wiki.model.WikiPageSoap updatePage(long nodeId, java.lang.String title, double version, java.lang.String content, java.lang.String summary, boolean minorEdit, java.lang.String format, java.lang.String parentTitle, java.lang.String redirectTitle, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (wikiPageServiceSoap == null)
      _initWikiPageServiceSoapProxy();
    return wikiPageServiceSoap.updatePage(nodeId, title, version, content, summary, minorEdit, format, parentTitle, redirectTitle, serviceContext);
  }
  
  
}