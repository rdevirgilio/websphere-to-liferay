/**
 * WikiPageSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.liferay.portlet.wiki.model;

public class WikiPageSoap  implements java.io.Serializable {
    /**
	 * @uml.property  name="companyId"
	 */
    private long companyId;

    /**
	 * @uml.property  name="content"
	 */
    private java.lang.String content;

    /**
	 * @uml.property  name="createDate"
	 */
    private java.util.Calendar createDate;

    /**
	 * @uml.property  name="format"
	 */
    private java.lang.String format;

    /**
	 * @uml.property  name="groupId"
	 */
    private long groupId;

    /**
	 * @uml.property  name="head"
	 */
    private boolean head;

    /**
	 * @uml.property  name="minorEdit"
	 */
    private boolean minorEdit;

    /**
	 * @uml.property  name="modifiedDate"
	 */
    private java.util.Calendar modifiedDate;

    /**
	 * @uml.property  name="nodeId"
	 */
    private long nodeId;

    /**
	 * @uml.property  name="pageId"
	 */
    private long pageId;

    /**
	 * @uml.property  name="parentTitle"
	 */
    private java.lang.String parentTitle;

    /**
	 * @uml.property  name="primaryKey"
	 */
    private long primaryKey;

    /**
	 * @uml.property  name="redirectTitle"
	 */
    private java.lang.String redirectTitle;

    /**
	 * @uml.property  name="resourcePrimKey"
	 */
    private long resourcePrimKey;

    /**
	 * @uml.property  name="status"
	 */
    private int status;

    /**
	 * @uml.property  name="statusByUserId"
	 */
    private long statusByUserId;

    /**
	 * @uml.property  name="statusByUserName"
	 */
    private java.lang.String statusByUserName;

    /**
	 * @uml.property  name="statusDate"
	 */
    private java.util.Calendar statusDate;

    /**
	 * @uml.property  name="summary"
	 */
    private java.lang.String summary;

    /**
	 * @uml.property  name="title"
	 */
    private java.lang.String title;

    /**
	 * @uml.property  name="userId"
	 */
    private long userId;

    /**
	 * @uml.property  name="userName"
	 */
    private java.lang.String userName;

    /**
	 * @uml.property  name="uuid"
	 */
    private java.lang.String uuid;

    /**
	 * @uml.property  name="version"
	 */
    private double version;

    public WikiPageSoap() {
    }

    public WikiPageSoap(
           long companyId,
           java.lang.String content,
           java.util.Calendar createDate,
           java.lang.String format,
           long groupId,
           boolean head,
           boolean minorEdit,
           java.util.Calendar modifiedDate,
           long nodeId,
           long pageId,
           java.lang.String parentTitle,
           long primaryKey,
           java.lang.String redirectTitle,
           long resourcePrimKey,
           int status,
           long statusByUserId,
           java.lang.String statusByUserName,
           java.util.Calendar statusDate,
           java.lang.String summary,
           java.lang.String title,
           long userId,
           java.lang.String userName,
           java.lang.String uuid,
           double version) {
           this.companyId = companyId;
           this.content = content;
           this.createDate = createDate;
           this.format = format;
           this.groupId = groupId;
           this.head = head;
           this.minorEdit = minorEdit;
           this.modifiedDate = modifiedDate;
           this.nodeId = nodeId;
           this.pageId = pageId;
           this.parentTitle = parentTitle;
           this.primaryKey = primaryKey;
           this.redirectTitle = redirectTitle;
           this.resourcePrimKey = resourcePrimKey;
           this.status = status;
           this.statusByUserId = statusByUserId;
           this.statusByUserName = statusByUserName;
           this.statusDate = statusDate;
           this.summary = summary;
           this.title = title;
           this.userId = userId;
           this.userName = userName;
           this.uuid = uuid;
           this.version = version;
    }


    /**
	 * Gets the companyId value for this WikiPageSoap.
	 * @return  companyId
	 * @uml.property  name="companyId"
	 */
    public long getCompanyId() {
        return companyId;
    }


    /**
	 * Sets the companyId value for this WikiPageSoap.
	 * @param  companyId
	 * @uml.property  name="companyId"
	 */
    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }


    /**
	 * Gets the content value for this WikiPageSoap.
	 * @return  content
	 * @uml.property  name="content"
	 */
    public java.lang.String getContent() {
        return content;
    }


    /**
	 * Sets the content value for this WikiPageSoap.
	 * @param  content
	 * @uml.property  name="content"
	 */
    public void setContent(java.lang.String content) {
        this.content = content;
    }


    /**
	 * Gets the createDate value for this WikiPageSoap.
	 * @return  createDate
	 * @uml.property  name="createDate"
	 */
    public java.util.Calendar getCreateDate() {
        return createDate;
    }


    /**
	 * Sets the createDate value for this WikiPageSoap.
	 * @param  createDate
	 * @uml.property  name="createDate"
	 */
    public void setCreateDate(java.util.Calendar createDate) {
        this.createDate = createDate;
    }


    /**
	 * Gets the format value for this WikiPageSoap.
	 * @return  format
	 * @uml.property  name="format"
	 */
    public java.lang.String getFormat() {
        return format;
    }


    /**
	 * Sets the format value for this WikiPageSoap.
	 * @param  format
	 * @uml.property  name="format"
	 */
    public void setFormat(java.lang.String format) {
        this.format = format;
    }


    /**
	 * Gets the groupId value for this WikiPageSoap.
	 * @return  groupId
	 * @uml.property  name="groupId"
	 */
    public long getGroupId() {
        return groupId;
    }


    /**
	 * Sets the groupId value for this WikiPageSoap.
	 * @param  groupId
	 * @uml.property  name="groupId"
	 */
    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }


    /**
	 * Gets the head value for this WikiPageSoap.
	 * @return  head
	 * @uml.property  name="head"
	 */
    public boolean isHead() {
        return head;
    }


    /**
	 * Sets the head value for this WikiPageSoap.
	 * @param  head
	 * @uml.property  name="head"
	 */
    public void setHead(boolean head) {
        this.head = head;
    }


    /**
	 * Gets the minorEdit value for this WikiPageSoap.
	 * @return  minorEdit
	 * @uml.property  name="minorEdit"
	 */
    public boolean isMinorEdit() {
        return minorEdit;
    }


    /**
	 * Sets the minorEdit value for this WikiPageSoap.
	 * @param  minorEdit
	 * @uml.property  name="minorEdit"
	 */
    public void setMinorEdit(boolean minorEdit) {
        this.minorEdit = minorEdit;
    }


    /**
	 * Gets the modifiedDate value for this WikiPageSoap.
	 * @return  modifiedDate
	 * @uml.property  name="modifiedDate"
	 */
    public java.util.Calendar getModifiedDate() {
        return modifiedDate;
    }


    /**
	 * Sets the modifiedDate value for this WikiPageSoap.
	 * @param  modifiedDate
	 * @uml.property  name="modifiedDate"
	 */
    public void setModifiedDate(java.util.Calendar modifiedDate) {
        this.modifiedDate = modifiedDate;
    }


    /**
	 * Gets the nodeId value for this WikiPageSoap.
	 * @return  nodeId
	 * @uml.property  name="nodeId"
	 */
    public long getNodeId() {
        return nodeId;
    }


    /**
	 * Sets the nodeId value for this WikiPageSoap.
	 * @param  nodeId
	 * @uml.property  name="nodeId"
	 */
    public void setNodeId(long nodeId) {
        this.nodeId = nodeId;
    }


    /**
	 * Gets the pageId value for this WikiPageSoap.
	 * @return  pageId
	 * @uml.property  name="pageId"
	 */
    public long getPageId() {
        return pageId;
    }


    /**
	 * Sets the pageId value for this WikiPageSoap.
	 * @param  pageId
	 * @uml.property  name="pageId"
	 */
    public void setPageId(long pageId) {
        this.pageId = pageId;
    }


    /**
	 * Gets the parentTitle value for this WikiPageSoap.
	 * @return  parentTitle
	 * @uml.property  name="parentTitle"
	 */
    public java.lang.String getParentTitle() {
        return parentTitle;
    }


    /**
	 * Sets the parentTitle value for this WikiPageSoap.
	 * @param  parentTitle
	 * @uml.property  name="parentTitle"
	 */
    public void setParentTitle(java.lang.String parentTitle) {
        this.parentTitle = parentTitle;
    }


    /**
	 * Gets the primaryKey value for this WikiPageSoap.
	 * @return  primaryKey
	 * @uml.property  name="primaryKey"
	 */
    public long getPrimaryKey() {
        return primaryKey;
    }


    /**
	 * Sets the primaryKey value for this WikiPageSoap.
	 * @param  primaryKey
	 * @uml.property  name="primaryKey"
	 */
    public void setPrimaryKey(long primaryKey) {
        this.primaryKey = primaryKey;
    }


    /**
	 * Gets the redirectTitle value for this WikiPageSoap.
	 * @return  redirectTitle
	 * @uml.property  name="redirectTitle"
	 */
    public java.lang.String getRedirectTitle() {
        return redirectTitle;
    }


    /**
	 * Sets the redirectTitle value for this WikiPageSoap.
	 * @param  redirectTitle
	 * @uml.property  name="redirectTitle"
	 */
    public void setRedirectTitle(java.lang.String redirectTitle) {
        this.redirectTitle = redirectTitle;
    }


    /**
	 * Gets the resourcePrimKey value for this WikiPageSoap.
	 * @return  resourcePrimKey
	 * @uml.property  name="resourcePrimKey"
	 */
    public long getResourcePrimKey() {
        return resourcePrimKey;
    }


    /**
	 * Sets the resourcePrimKey value for this WikiPageSoap.
	 * @param  resourcePrimKey
	 * @uml.property  name="resourcePrimKey"
	 */
    public void setResourcePrimKey(long resourcePrimKey) {
        this.resourcePrimKey = resourcePrimKey;
    }


    /**
	 * Gets the status value for this WikiPageSoap.
	 * @return  status
	 * @uml.property  name="status"
	 */
    public int getStatus() {
        return status;
    }


    /**
	 * Sets the status value for this WikiPageSoap.
	 * @param  status
	 * @uml.property  name="status"
	 */
    public void setStatus(int status) {
        this.status = status;
    }


    /**
	 * Gets the statusByUserId value for this WikiPageSoap.
	 * @return  statusByUserId
	 * @uml.property  name="statusByUserId"
	 */
    public long getStatusByUserId() {
        return statusByUserId;
    }


    /**
	 * Sets the statusByUserId value for this WikiPageSoap.
	 * @param  statusByUserId
	 * @uml.property  name="statusByUserId"
	 */
    public void setStatusByUserId(long statusByUserId) {
        this.statusByUserId = statusByUserId;
    }


    /**
	 * Gets the statusByUserName value for this WikiPageSoap.
	 * @return  statusByUserName
	 * @uml.property  name="statusByUserName"
	 */
    public java.lang.String getStatusByUserName() {
        return statusByUserName;
    }


    /**
	 * Sets the statusByUserName value for this WikiPageSoap.
	 * @param  statusByUserName
	 * @uml.property  name="statusByUserName"
	 */
    public void setStatusByUserName(java.lang.String statusByUserName) {
        this.statusByUserName = statusByUserName;
    }


    /**
	 * Gets the statusDate value for this WikiPageSoap.
	 * @return  statusDate
	 * @uml.property  name="statusDate"
	 */
    public java.util.Calendar getStatusDate() {
        return statusDate;
    }


    /**
	 * Sets the statusDate value for this WikiPageSoap.
	 * @param  statusDate
	 * @uml.property  name="statusDate"
	 */
    public void setStatusDate(java.util.Calendar statusDate) {
        this.statusDate = statusDate;
    }


    /**
	 * Gets the summary value for this WikiPageSoap.
	 * @return  summary
	 * @uml.property  name="summary"
	 */
    public java.lang.String getSummary() {
        return summary;
    }


    /**
	 * Sets the summary value for this WikiPageSoap.
	 * @param  summary
	 * @uml.property  name="summary"
	 */
    public void setSummary(java.lang.String summary) {
        this.summary = summary;
    }


    /**
	 * Gets the title value for this WikiPageSoap.
	 * @return  title
	 * @uml.property  name="title"
	 */
    public java.lang.String getTitle() {
        return title;
    }


    /**
	 * Sets the title value for this WikiPageSoap.
	 * @param  title
	 * @uml.property  name="title"
	 */
    public void setTitle(java.lang.String title) {
        this.title = title;
    }


    /**
	 * Gets the userId value for this WikiPageSoap.
	 * @return  userId
	 * @uml.property  name="userId"
	 */
    public long getUserId() {
        return userId;
    }


    /**
	 * Sets the userId value for this WikiPageSoap.
	 * @param  userId
	 * @uml.property  name="userId"
	 */
    public void setUserId(long userId) {
        this.userId = userId;
    }


    /**
	 * Gets the userName value for this WikiPageSoap.
	 * @return  userName
	 * @uml.property  name="userName"
	 */
    public java.lang.String getUserName() {
        return userName;
    }


    /**
	 * Sets the userName value for this WikiPageSoap.
	 * @param  userName
	 * @uml.property  name="userName"
	 */
    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }


    /**
	 * Gets the uuid value for this WikiPageSoap.
	 * @return  uuid
	 * @uml.property  name="uuid"
	 */
    public java.lang.String getUuid() {
        return uuid;
    }


    /**
	 * Sets the uuid value for this WikiPageSoap.
	 * @param  uuid
	 * @uml.property  name="uuid"
	 */
    public void setUuid(java.lang.String uuid) {
        this.uuid = uuid;
    }


    /**
	 * Gets the version value for this WikiPageSoap.
	 * @return  version
	 * @uml.property  name="version"
	 */
    public double getVersion() {
        return version;
    }


    /**
	 * Sets the version value for this WikiPageSoap.
	 * @param  version
	 * @uml.property  name="version"
	 */
    public void setVersion(double version) {
        this.version = version;
    }

    /**
	 * @uml.property  name="__equalsCalc"
	 */
    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WikiPageSoap)) return false;
        WikiPageSoap other = (WikiPageSoap) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.companyId == other.getCompanyId() &&
            ((this.content==null && other.getContent()==null) || 
             (this.content!=null &&
              this.content.equals(other.getContent()))) &&
            ((this.createDate==null && other.getCreateDate()==null) || 
             (this.createDate!=null &&
              this.createDate.equals(other.getCreateDate()))) &&
            ((this.format==null && other.getFormat()==null) || 
             (this.format!=null &&
              this.format.equals(other.getFormat()))) &&
            this.groupId == other.getGroupId() &&
            this.head == other.isHead() &&
            this.minorEdit == other.isMinorEdit() &&
            ((this.modifiedDate==null && other.getModifiedDate()==null) || 
             (this.modifiedDate!=null &&
              this.modifiedDate.equals(other.getModifiedDate()))) &&
            this.nodeId == other.getNodeId() &&
            this.pageId == other.getPageId() &&
            ((this.parentTitle==null && other.getParentTitle()==null) || 
             (this.parentTitle!=null &&
              this.parentTitle.equals(other.getParentTitle()))) &&
            this.primaryKey == other.getPrimaryKey() &&
            ((this.redirectTitle==null && other.getRedirectTitle()==null) || 
             (this.redirectTitle!=null &&
              this.redirectTitle.equals(other.getRedirectTitle()))) &&
            this.resourcePrimKey == other.getResourcePrimKey() &&
            this.status == other.getStatus() &&
            this.statusByUserId == other.getStatusByUserId() &&
            ((this.statusByUserName==null && other.getStatusByUserName()==null) || 
             (this.statusByUserName!=null &&
              this.statusByUserName.equals(other.getStatusByUserName()))) &&
            ((this.statusDate==null && other.getStatusDate()==null) || 
             (this.statusDate!=null &&
              this.statusDate.equals(other.getStatusDate()))) &&
            ((this.summary==null && other.getSummary()==null) || 
             (this.summary!=null &&
              this.summary.equals(other.getSummary()))) &&
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            this.userId == other.getUserId() &&
            ((this.userName==null && other.getUserName()==null) || 
             (this.userName!=null &&
              this.userName.equals(other.getUserName()))) &&
            ((this.uuid==null && other.getUuid()==null) || 
             (this.uuid!=null &&
              this.uuid.equals(other.getUuid()))) &&
            this.version == other.getVersion();
        __equalsCalc = null;
        return _equals;
    }

    /**
	 * @uml.property  name="__hashCodeCalc"
	 */
    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getCompanyId()).hashCode();
        if (getContent() != null) {
            _hashCode += getContent().hashCode();
        }
        if (getCreateDate() != null) {
            _hashCode += getCreateDate().hashCode();
        }
        if (getFormat() != null) {
            _hashCode += getFormat().hashCode();
        }
        _hashCode += new Long(getGroupId()).hashCode();
        _hashCode += (isHead() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isMinorEdit() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getModifiedDate() != null) {
            _hashCode += getModifiedDate().hashCode();
        }
        _hashCode += new Long(getNodeId()).hashCode();
        _hashCode += new Long(getPageId()).hashCode();
        if (getParentTitle() != null) {
            _hashCode += getParentTitle().hashCode();
        }
        _hashCode += new Long(getPrimaryKey()).hashCode();
        if (getRedirectTitle() != null) {
            _hashCode += getRedirectTitle().hashCode();
        }
        _hashCode += new Long(getResourcePrimKey()).hashCode();
        _hashCode += getStatus();
        _hashCode += new Long(getStatusByUserId()).hashCode();
        if (getStatusByUserName() != null) {
            _hashCode += getStatusByUserName().hashCode();
        }
        if (getStatusDate() != null) {
            _hashCode += getStatusDate().hashCode();
        }
        if (getSummary() != null) {
            _hashCode += getSummary().hashCode();
        }
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        _hashCode += new Long(getUserId()).hashCode();
        if (getUserName() != null) {
            _hashCode += getUserName().hashCode();
        }
        if (getUuid() != null) {
            _hashCode += getUuid().hashCode();
        }
        _hashCode += new Double(getVersion()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WikiPageSoap.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://model.wiki.portlet.liferay.com", "WikiPageSoap"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "companyId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("content");
        elemField.setXmlName(new javax.xml.namespace.QName("", "content"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "createDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("format");
        elemField.setXmlName(new javax.xml.namespace.QName("", "format"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "groupId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("head");
        elemField.setXmlName(new javax.xml.namespace.QName("", "head"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("minorEdit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "minorEdit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifiedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modifiedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nodeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nodeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pageId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pageId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentTitle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "parentTitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("primaryKey");
        elemField.setXmlName(new javax.xml.namespace.QName("", "primaryKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("redirectTitle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "redirectTitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resourcePrimKey");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resourcePrimKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusByUserId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statusByUserId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusByUserName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statusByUserName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statusDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("summary");
        elemField.setXmlName(new javax.xml.namespace.QName("", "summary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uuid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "uuid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("", "version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
