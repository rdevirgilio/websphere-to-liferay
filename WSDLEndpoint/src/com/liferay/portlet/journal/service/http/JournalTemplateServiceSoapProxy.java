package com.liferay.portlet.journal.service.http;

public class JournalTemplateServiceSoapProxy implements com.liferay.portlet.journal.service.http.JournalTemplateServiceSoap {
  private String _endpoint = null;
  private com.liferay.portlet.journal.service.http.JournalTemplateServiceSoap journalTemplateServiceSoap = null;
  
  public JournalTemplateServiceSoapProxy() {
    _initJournalTemplateServiceSoapProxy();
  }
  
  public JournalTemplateServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initJournalTemplateServiceSoapProxy();
  }
  
  private void _initJournalTemplateServiceSoapProxy() {
    try {
      journalTemplateServiceSoap = (new com.liferay.portlet.journal.service.http.JournalTemplateServiceSoapServiceLocator()).getPortlet_Journal_JournalTemplateService();
      if (journalTemplateServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)journalTemplateServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)journalTemplateServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (journalTemplateServiceSoap != null)
      ((javax.xml.rpc.Stub)journalTemplateServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.liferay.portlet.journal.service.http.JournalTemplateServiceSoap getJournalTemplateServiceSoap() {
    if (journalTemplateServiceSoap == null)
      _initJournalTemplateServiceSoapProxy();
    return journalTemplateServiceSoap;
  }
  
  public com.liferay.portlet.journal.model.JournalTemplateSoap addTemplate(long groupId, java.lang.String templateId, boolean autoTemplateId, java.lang.String structureId, java.lang.String[] nameMapLanguageIds, java.lang.String[] nameMapValues, java.lang.String[] descriptionMapLanguageIds, java.lang.String[] descriptionMapValues, java.lang.String xsl, boolean formatXsl, java.lang.String langType, boolean cacheable, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (journalTemplateServiceSoap == null)
      _initJournalTemplateServiceSoapProxy();
    return journalTemplateServiceSoap.addTemplate(groupId, templateId, autoTemplateId, structureId, nameMapLanguageIds, nameMapValues, descriptionMapLanguageIds, descriptionMapValues, xsl, formatXsl, langType, cacheable, serviceContext);
  }
  
  public com.liferay.portlet.journal.model.JournalTemplateSoap copyTemplate(long groupId, java.lang.String oldTemplateId, java.lang.String newTemplateId, boolean autoTemplateId) throws java.rmi.RemoteException{
    if (journalTemplateServiceSoap == null)
      _initJournalTemplateServiceSoapProxy();
    return journalTemplateServiceSoap.copyTemplate(groupId, oldTemplateId, newTemplateId, autoTemplateId);
  }
  
  public void deleteTemplate(long groupId, java.lang.String templateId) throws java.rmi.RemoteException{
    if (journalTemplateServiceSoap == null)
      _initJournalTemplateServiceSoapProxy();
    journalTemplateServiceSoap.deleteTemplate(groupId, templateId);
  }
  
  public com.liferay.portlet.journal.model.JournalTemplateSoap[] getStructureTemplates(long groupId, java.lang.String structureId) throws java.rmi.RemoteException{
    if (journalTemplateServiceSoap == null)
      _initJournalTemplateServiceSoapProxy();
    return journalTemplateServiceSoap.getStructureTemplates(groupId, structureId);
  }
  
  public com.liferay.portlet.journal.model.JournalTemplateSoap getTemplate(long groupId, java.lang.String templateId) throws java.rmi.RemoteException{
    if (journalTemplateServiceSoap == null)
      _initJournalTemplateServiceSoapProxy();
    return journalTemplateServiceSoap.getTemplate(groupId, templateId);
  }
  
  public int searchCount(long companyId, long[] groupIds, java.lang.String keywords, java.lang.String structureId, java.lang.String structureIdComparator) throws java.rmi.RemoteException{
    if (journalTemplateServiceSoap == null)
      _initJournalTemplateServiceSoapProxy();
    return journalTemplateServiceSoap.searchCount(companyId, groupIds, keywords, structureId, structureIdComparator);
  }
  
  public int searchCount(long companyId, long[] groupIds, java.lang.String templateId, java.lang.String structureId, java.lang.String structureIdComparator, java.lang.String name, java.lang.String description, boolean andOperator) throws java.rmi.RemoteException{
    if (journalTemplateServiceSoap == null)
      _initJournalTemplateServiceSoapProxy();
    return journalTemplateServiceSoap.searchCount(companyId, groupIds, templateId, structureId, structureIdComparator, name, description, andOperator);
  }
  
  public com.liferay.portlet.journal.model.JournalTemplateSoap[] search(long companyId, long[] groupIds, java.lang.String keywords, java.lang.String structureId, java.lang.String structureIdComparator, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (journalTemplateServiceSoap == null)
      _initJournalTemplateServiceSoapProxy();
    return journalTemplateServiceSoap.search(companyId, groupIds, keywords, structureId, structureIdComparator, start, end, obc);
  }
  
  public com.liferay.portlet.journal.model.JournalTemplateSoap[] search(long companyId, long[] groupIds, java.lang.String templateId, java.lang.String structureId, java.lang.String structureIdComparator, java.lang.String name, java.lang.String description, boolean andOperator, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (journalTemplateServiceSoap == null)
      _initJournalTemplateServiceSoapProxy();
    return journalTemplateServiceSoap.search(companyId, groupIds, templateId, structureId, structureIdComparator, name, description, andOperator, start, end, obc);
  }
  
  public com.liferay.portlet.journal.model.JournalTemplateSoap updateTemplate(long groupId, java.lang.String templateId, java.lang.String structureId, java.lang.String[] nameMapLanguageIds, java.lang.String[] nameMapValues, java.lang.String[] descriptionMapLanguageIds, java.lang.String[] descriptionMapValues, java.lang.String xsl, boolean formatXsl, java.lang.String langType, boolean cacheable, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (journalTemplateServiceSoap == null)
      _initJournalTemplateServiceSoapProxy();
    return journalTemplateServiceSoap.updateTemplate(groupId, templateId, structureId, nameMapLanguageIds, nameMapValues, descriptionMapLanguageIds, descriptionMapValues, xsl, formatXsl, langType, cacheable, serviceContext);
  }
  
  
}