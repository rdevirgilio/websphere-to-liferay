package com.liferay.portlet.journal.service.http;

public class JournalStructureServiceSoapProxy implements com.liferay.portlet.journal.service.http.JournalStructureServiceSoap {
  private String _endpoint = null;
  private com.liferay.portlet.journal.service.http.JournalStructureServiceSoap journalStructureServiceSoap = null;
  
  public JournalStructureServiceSoapProxy() {
    _initJournalStructureServiceSoapProxy();
  }
  
  public JournalStructureServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initJournalStructureServiceSoapProxy();
  }
  
  private void _initJournalStructureServiceSoapProxy() {
    try {
      journalStructureServiceSoap = (new com.liferay.portlet.journal.service.http.JournalStructureServiceSoapServiceLocator()).getPortlet_Journal_JournalStructureService();
      if (journalStructureServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)journalStructureServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)journalStructureServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (journalStructureServiceSoap != null)
      ((javax.xml.rpc.Stub)journalStructureServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.liferay.portlet.journal.service.http.JournalStructureServiceSoap getJournalStructureServiceSoap() {
    if (journalStructureServiceSoap == null)
      _initJournalStructureServiceSoapProxy();
    return journalStructureServiceSoap;
  }
  
  public com.liferay.portlet.journal.model.JournalStructureSoap addStructure(long groupId, java.lang.String structureId, boolean autoStructureId, java.lang.String parentStructureId, java.lang.String[] nameMapLanguageIds, java.lang.String[] nameMapValues, java.lang.String[] descriptionMapLanguageIds, java.lang.String[] descriptionMapValues, java.lang.String xsd, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (journalStructureServiceSoap == null)
      _initJournalStructureServiceSoapProxy();
    return journalStructureServiceSoap.addStructure(groupId, structureId, autoStructureId, parentStructureId, nameMapLanguageIds, nameMapValues, descriptionMapLanguageIds, descriptionMapValues, xsd, serviceContext);
  }
  
  public com.liferay.portlet.journal.model.JournalStructureSoap copyStructure(long groupId, java.lang.String oldStructureId, java.lang.String newStructureId, boolean autoStructureId) throws java.rmi.RemoteException{
    if (journalStructureServiceSoap == null)
      _initJournalStructureServiceSoapProxy();
    return journalStructureServiceSoap.copyStructure(groupId, oldStructureId, newStructureId, autoStructureId);
  }
  
  public void deleteStructure(long groupId, java.lang.String structureId) throws java.rmi.RemoteException{
    if (journalStructureServiceSoap == null)
      _initJournalStructureServiceSoapProxy();
    journalStructureServiceSoap.deleteStructure(groupId, structureId);
  }
  
  public com.liferay.portlet.journal.model.JournalStructureSoap getStructure(long groupId, java.lang.String structureId) throws java.rmi.RemoteException{
    if (journalStructureServiceSoap == null)
      _initJournalStructureServiceSoapProxy();
    return journalStructureServiceSoap.getStructure(groupId, structureId);
  }
  
  public com.liferay.portlet.journal.model.JournalStructureSoap[] getStructures(long groupId) throws java.rmi.RemoteException{
    if (journalStructureServiceSoap == null)
      _initJournalStructureServiceSoapProxy();
    return journalStructureServiceSoap.getStructures(groupId);
  }
  
  public int searchCount(long companyId, long[] groupIds, java.lang.String keywords) throws java.rmi.RemoteException{
    if (journalStructureServiceSoap == null)
      _initJournalStructureServiceSoapProxy();
    return journalStructureServiceSoap.searchCount(companyId, groupIds, keywords);
  }
  
  public int searchCount(long companyId, long[] groupIds, java.lang.String structureId, java.lang.String name, java.lang.String description, boolean andOperator) throws java.rmi.RemoteException{
    if (journalStructureServiceSoap == null)
      _initJournalStructureServiceSoapProxy();
    return journalStructureServiceSoap.searchCount(companyId, groupIds, structureId, name, description, andOperator);
  }
  
  public com.liferay.portlet.journal.model.JournalStructureSoap[] search(long companyId, long[] groupIds, java.lang.String keywords, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (journalStructureServiceSoap == null)
      _initJournalStructureServiceSoapProxy();
    return journalStructureServiceSoap.search(companyId, groupIds, keywords, start, end, obc);
  }
  
  public com.liferay.portlet.journal.model.JournalStructureSoap[] search(long companyId, long[] groupIds, java.lang.String structureId, java.lang.String name, java.lang.String description, boolean andOperator, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (journalStructureServiceSoap == null)
      _initJournalStructureServiceSoapProxy();
    return journalStructureServiceSoap.search(companyId, groupIds, structureId, name, description, andOperator, start, end, obc);
  }
  
  public com.liferay.portlet.journal.model.JournalStructureSoap updateStructure(long groupId, java.lang.String structureId, java.lang.String parentStructureId, java.lang.String[] nameMapLanguageIds, java.lang.String[] nameMapValues, java.lang.String[] descriptionMapLanguageIds, java.lang.String[] descriptionMapValues, java.lang.String xsd, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (journalStructureServiceSoap == null)
      _initJournalStructureServiceSoapProxy();
    return journalStructureServiceSoap.updateStructure(groupId, structureId, parentStructureId, nameMapLanguageIds, nameMapValues, descriptionMapLanguageIds, descriptionMapValues, xsd, serviceContext);
  }
  
  
}