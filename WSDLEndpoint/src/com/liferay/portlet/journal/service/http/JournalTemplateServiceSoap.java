/**
 * JournalTemplateServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.liferay.portlet.journal.service.http;

public interface JournalTemplateServiceSoap extends java.rmi.Remote {
    public com.liferay.portlet.journal.model.JournalTemplateSoap addTemplate(long groupId, java.lang.String templateId, boolean autoTemplateId, java.lang.String structureId, java.lang.String[] nameMapLanguageIds, java.lang.String[] nameMapValues, java.lang.String[] descriptionMapLanguageIds, java.lang.String[] descriptionMapValues, java.lang.String xsl, boolean formatXsl, java.lang.String langType, boolean cacheable, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException;
    public com.liferay.portlet.journal.model.JournalTemplateSoap copyTemplate(long groupId, java.lang.String oldTemplateId, java.lang.String newTemplateId, boolean autoTemplateId) throws java.rmi.RemoteException;
    public void deleteTemplate(long groupId, java.lang.String templateId) throws java.rmi.RemoteException;
    public com.liferay.portlet.journal.model.JournalTemplateSoap[] getStructureTemplates(long groupId, java.lang.String structureId) throws java.rmi.RemoteException;
    public com.liferay.portlet.journal.model.JournalTemplateSoap getTemplate(long groupId, java.lang.String templateId) throws java.rmi.RemoteException;
    public int searchCount(long companyId, long[] groupIds, java.lang.String keywords, java.lang.String structureId, java.lang.String structureIdComparator) throws java.rmi.RemoteException;
    public int searchCount(long companyId, long[] groupIds, java.lang.String templateId, java.lang.String structureId, java.lang.String structureIdComparator, java.lang.String name, java.lang.String description, boolean andOperator) throws java.rmi.RemoteException;
    public com.liferay.portlet.journal.model.JournalTemplateSoap[] search(long companyId, long[] groupIds, java.lang.String keywords, java.lang.String structureId, java.lang.String structureIdComparator, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException;
    public com.liferay.portlet.journal.model.JournalTemplateSoap[] search(long companyId, long[] groupIds, java.lang.String templateId, java.lang.String structureId, java.lang.String structureIdComparator, java.lang.String name, java.lang.String description, boolean andOperator, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException;
    public com.liferay.portlet.journal.model.JournalTemplateSoap updateTemplate(long groupId, java.lang.String templateId, java.lang.String structureId, java.lang.String[] nameMapLanguageIds, java.lang.String[] nameMapValues, java.lang.String[] descriptionMapLanguageIds, java.lang.String[] descriptionMapValues, java.lang.String xsl, boolean formatXsl, java.lang.String langType, boolean cacheable, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException;
}
