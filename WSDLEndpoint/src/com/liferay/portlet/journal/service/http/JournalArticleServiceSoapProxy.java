package com.liferay.portlet.journal.service.http;

public class JournalArticleServiceSoapProxy implements com.liferay.portlet.journal.service.http.JournalArticleServiceSoap {
  private String _endpoint = null;
  private com.liferay.portlet.journal.service.http.JournalArticleServiceSoap journalArticleServiceSoap = null;
  
  public JournalArticleServiceSoapProxy() {
    _initJournalArticleServiceSoapProxy();
  }
  
  public JournalArticleServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initJournalArticleServiceSoapProxy();
  }
  
  private void _initJournalArticleServiceSoapProxy() {
    try {
      journalArticleServiceSoap = (new com.liferay.portlet.journal.service.http.JournalArticleServiceSoapServiceLocator()).getPortlet_Journal_JournalArticleService();
      if (journalArticleServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)journalArticleServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)journalArticleServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (journalArticleServiceSoap != null)
      ((javax.xml.rpc.Stub)journalArticleServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.liferay.portlet.journal.service.http.JournalArticleServiceSoap getJournalArticleServiceSoap() {
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap;
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap addArticle(long groupId, long classNameId, long classPK, java.lang.String articleId, boolean autoArticleId, java.lang.String[] titleMapLanguageIds, java.lang.String[] titleMapValues, java.lang.String[] descriptionMapLanguageIds, java.lang.String[] descriptionMapValues, java.lang.String content, java.lang.String type, java.lang.String structureId, java.lang.String templateId, java.lang.String layoutUuid, int displayDateMonth, int displayDateDay, int displayDateYear, int displayDateHour, int displayDateMinute, int expirationDateMonth, int expirationDateDay, int expirationDateYear, int expirationDateHour, int expirationDateMinute, boolean neverExpire, int reviewDateMonth, int reviewDateDay, int reviewDateYear, int reviewDateHour, int reviewDateMinute, boolean neverReview, boolean indexable, java.lang.String articleURL, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.addArticle(groupId, classNameId, classPK, articleId, autoArticleId, titleMapLanguageIds, titleMapValues, descriptionMapLanguageIds, descriptionMapValues, content, type, structureId, templateId, layoutUuid, displayDateMonth, displayDateDay, displayDateYear, displayDateHour, displayDateMinute, expirationDateMonth, expirationDateDay, expirationDateYear, expirationDateHour, expirationDateMinute, neverExpire, reviewDateMonth, reviewDateDay, reviewDateYear, reviewDateHour, reviewDateMinute, neverReview, indexable, articleURL, serviceContext);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap copyArticle(long groupId, java.lang.String oldArticleId, java.lang.String newArticleId, boolean autoArticleId, double version) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.copyArticle(groupId, oldArticleId, newArticleId, autoArticleId, version);
  }
  
  public void deleteArticle(long groupId, java.lang.String articleId, double version, java.lang.String articleURL, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    journalArticleServiceSoap.deleteArticle(groupId, articleId, version, articleURL, serviceContext);
  }
  
  public void deleteArticle(long groupId, java.lang.String articleId, java.lang.String articleURL, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    journalArticleServiceSoap.deleteArticle(groupId, articleId, articleURL, serviceContext);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap expireArticle(long groupId, java.lang.String articleId, double version, java.lang.String articleURL, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.expireArticle(groupId, articleId, version, articleURL, serviceContext);
  }
  
  public void expireArticle(long groupId, java.lang.String articleId, java.lang.String articleURL, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    journalArticleServiceSoap.expireArticle(groupId, articleId, articleURL, serviceContext);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap getArticleByUrlTitle(long groupId, java.lang.String urlTitle) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.getArticleByUrlTitle(groupId, urlTitle);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap getArticle(long id) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.getArticle(id);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap getArticle(long groupId, java.lang.String articleId) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.getArticle(groupId, articleId);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap getArticle(long groupId, java.lang.String articleId, double version) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.getArticle(groupId, articleId, version);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap getArticle(long groupId, java.lang.String className, long classPK) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.getArticle(groupId, className, classPK);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap[] getArticlesByArticleId(long groupId, java.lang.String articleId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.getArticlesByArticleId(groupId, articleId, start, end, obc);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap[] getArticlesByLayoutUuid(long groupId, java.lang.String layoutUuid) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.getArticlesByLayoutUuid(groupId, layoutUuid);
  }
  
  public int getArticlesCountByArticleId(long groupId, java.lang.String articleId) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.getArticlesCountByArticleId(groupId, articleId);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap getDisplayArticleByUrlTitle(long groupId, java.lang.String urlTitle) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.getDisplayArticleByUrlTitle(groupId, urlTitle);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap getLatestArticle(long resourcePrimKey) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.getLatestArticle(resourcePrimKey);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap getLatestArticle(long groupId, java.lang.String articleId, int status) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.getLatestArticle(groupId, articleId, status);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap getLatestArticle(long groupId, java.lang.String className, long classPK) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.getLatestArticle(groupId, className, classPK);
  }
  
  public void removeArticleLocale(long companyId, java.lang.String languageId) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    journalArticleServiceSoap.removeArticleLocale(companyId, languageId);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap removeArticleLocale(long groupId, java.lang.String articleId, double version, java.lang.String languageId) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.removeArticleLocale(groupId, articleId, version, languageId);
  }
  
  public int searchCount(long companyId, long groupId, long classNameId, java.lang.String keywords, double version, java.lang.String type, java.lang.String structureId, java.lang.String templateId, java.util.Calendar displayDateGT, java.util.Calendar displayDateLT, int status, java.util.Calendar reviewDate) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.searchCount(companyId, groupId, classNameId, keywords, version, type, structureId, templateId, displayDateGT, displayDateLT, status, reviewDate);
  }
  
  public int searchCount(long companyId, long groupId, long classNameId, java.lang.String articleId, double version, java.lang.String title, java.lang.String description, java.lang.String content, java.lang.String type, java.lang.String[] structureIds, java.lang.String[] templateIds, java.util.Calendar displayDateGT, java.util.Calendar displayDateLT, int status, java.util.Calendar reviewDate, boolean andOperator) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.searchCount(companyId, groupId, classNameId, articleId, version, title, description, content, type, structureIds, templateIds, displayDateGT, displayDateLT, status, reviewDate, andOperator);
  }
  
  public int searchCount(long companyId, long groupId, long classNameId, java.lang.String articleId, double version, java.lang.String title, java.lang.String description, java.lang.String content, java.lang.String type, java.lang.String structureId, java.lang.String templateId, java.util.Calendar displayDateGT, java.util.Calendar displayDateLT, int status, java.util.Calendar reviewDate, boolean andOperator) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.searchCount(companyId, groupId, classNameId, articleId, version, title, description, content, type, structureId, templateId, displayDateGT, displayDateLT, status, reviewDate, andOperator);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap[] search(long companyId, long groupId, long classNameId, java.lang.String keywords, double version, java.lang.String type, java.lang.String structureId, java.lang.String templateId, java.util.Calendar displayDateGT, java.util.Calendar displayDateLT, int status, java.util.Calendar reviewDate, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.search(companyId, groupId, classNameId, keywords, version, type, structureId, templateId, displayDateGT, displayDateLT, status, reviewDate, start, end, obc);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap[] search(long companyId, long groupId, long classNameId, java.lang.String articleId, double version, java.lang.String title, java.lang.String description, java.lang.String content, java.lang.String type, java.lang.String[] structureIds, java.lang.String[] templateIds, java.util.Calendar displayDateGT, java.util.Calendar displayDateLT, int status, java.util.Calendar reviewDate, boolean andOperator, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.search(companyId, groupId, classNameId, articleId, version, title, description, content, type, structureIds, templateIds, displayDateGT, displayDateLT, status, reviewDate, andOperator, start, end, obc);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap[] search(long companyId, long groupId, long classNameId, java.lang.String articleId, double version, java.lang.String title, java.lang.String description, java.lang.String content, java.lang.String type, java.lang.String structureId, java.lang.String templateId, java.util.Calendar displayDateGT, java.util.Calendar displayDateLT, int status, java.util.Calendar reviewDate, boolean andOperator, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.search(companyId, groupId, classNameId, articleId, version, title, description, content, type, structureId, templateId, displayDateGT, displayDateLT, status, reviewDate, andOperator, start, end, obc);
  }
  
  public void subscribe(long groupId) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    journalArticleServiceSoap.subscribe(groupId);
  }
  
  public void unsubscribe(long groupId) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    journalArticleServiceSoap.unsubscribe(groupId);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap updateArticle(long userId, long groupId, java.lang.String articleId, double version, java.lang.String[] titleMapLanguageIds, java.lang.String[] titleMapValues, java.lang.String[] descriptionMapLanguageIds, java.lang.String[] descriptionMapValues, java.lang.String content, java.lang.String layoutUuid, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.updateArticle(userId, groupId, articleId, version, titleMapLanguageIds, titleMapValues, descriptionMapLanguageIds, descriptionMapValues, content, layoutUuid, serviceContext);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap updateArticle(long groupId, java.lang.String articleId, double version, java.lang.String content, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.updateArticle(groupId, articleId, version, content, serviceContext);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap updateContent(long groupId, java.lang.String articleId, double version, java.lang.String content) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.updateContent(groupId, articleId, version, content);
  }
  
  public com.liferay.portlet.journal.model.JournalArticleSoap updateStatus(long groupId, java.lang.String articleId, double version, int status, java.lang.String articleURL, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (journalArticleServiceSoap == null)
      _initJournalArticleServiceSoapProxy();
    return journalArticleServiceSoap.updateStatus(groupId, articleId, version, status, articleURL, serviceContext);
  }
  
  
}