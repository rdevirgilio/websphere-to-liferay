/**
 * DLFolderSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.liferay.portlet.documentlibrary.model;

public class DLFolderSoap  implements java.io.Serializable {
    /**
	 * @uml.property  name="companyId"
	 */
    private long companyId;

    /**
	 * @uml.property  name="createDate"
	 */
    private java.util.Calendar createDate;

    /**
	 * @uml.property  name="defaultFileEntryTypeId"
	 */
    private long defaultFileEntryTypeId;

    /**
	 * @uml.property  name="description"
	 */
    private java.lang.String description;

    /**
	 * @uml.property  name="folderId"
	 */
    private long folderId;

    /**
	 * @uml.property  name="groupId"
	 */
    private long groupId;

    /**
	 * @uml.property  name="lastPostDate"
	 */
    private java.util.Calendar lastPostDate;

    /**
	 * @uml.property  name="modifiedDate"
	 */
    private java.util.Calendar modifiedDate;

    /**
	 * @uml.property  name="mountPoint"
	 */
    private boolean mountPoint;

    /**
	 * @uml.property  name="name"
	 */
    private java.lang.String name;

    /**
	 * @uml.property  name="overrideFileEntryTypes"
	 */
    private boolean overrideFileEntryTypes;

    /**
	 * @uml.property  name="parentFolderId"
	 */
    private long parentFolderId;

    /**
	 * @uml.property  name="primaryKey"
	 */
    private long primaryKey;

    /**
	 * @uml.property  name="repositoryId"
	 */
    private long repositoryId;

    /**
	 * @uml.property  name="userId"
	 */
    private long userId;

    /**
	 * @uml.property  name="userName"
	 */
    private java.lang.String userName;

    /**
	 * @uml.property  name="uuid"
	 */
    private java.lang.String uuid;

    public DLFolderSoap() {
    }

    public DLFolderSoap(
           long companyId,
           java.util.Calendar createDate,
           long defaultFileEntryTypeId,
           java.lang.String description,
           long folderId,
           long groupId,
           java.util.Calendar lastPostDate,
           java.util.Calendar modifiedDate,
           boolean mountPoint,
           java.lang.String name,
           boolean overrideFileEntryTypes,
           long parentFolderId,
           long primaryKey,
           long repositoryId,
           long userId,
           java.lang.String userName,
           java.lang.String uuid) {
           this.companyId = companyId;
           this.createDate = createDate;
           this.defaultFileEntryTypeId = defaultFileEntryTypeId;
           this.description = description;
           this.folderId = folderId;
           this.groupId = groupId;
           this.lastPostDate = lastPostDate;
           this.modifiedDate = modifiedDate;
           this.mountPoint = mountPoint;
           this.name = name;
           this.overrideFileEntryTypes = overrideFileEntryTypes;
           this.parentFolderId = parentFolderId;
           this.primaryKey = primaryKey;
           this.repositoryId = repositoryId;
           this.userId = userId;
           this.userName = userName;
           this.uuid = uuid;
    }


    /**
	 * Gets the companyId value for this DLFolderSoap.
	 * @return  companyId
	 * @uml.property  name="companyId"
	 */
    public long getCompanyId() {
        return companyId;
    }


    /**
	 * Sets the companyId value for this DLFolderSoap.
	 * @param  companyId
	 * @uml.property  name="companyId"
	 */
    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }


    /**
	 * Gets the createDate value for this DLFolderSoap.
	 * @return  createDate
	 * @uml.property  name="createDate"
	 */
    public java.util.Calendar getCreateDate() {
        return createDate;
    }


    /**
	 * Sets the createDate value for this DLFolderSoap.
	 * @param  createDate
	 * @uml.property  name="createDate"
	 */
    public void setCreateDate(java.util.Calendar createDate) {
        this.createDate = createDate;
    }


    /**
	 * Gets the defaultFileEntryTypeId value for this DLFolderSoap.
	 * @return  defaultFileEntryTypeId
	 * @uml.property  name="defaultFileEntryTypeId"
	 */
    public long getDefaultFileEntryTypeId() {
        return defaultFileEntryTypeId;
    }


    /**
	 * Sets the defaultFileEntryTypeId value for this DLFolderSoap.
	 * @param  defaultFileEntryTypeId
	 * @uml.property  name="defaultFileEntryTypeId"
	 */
    public void setDefaultFileEntryTypeId(long defaultFileEntryTypeId) {
        this.defaultFileEntryTypeId = defaultFileEntryTypeId;
    }


    /**
	 * Gets the description value for this DLFolderSoap.
	 * @return  description
	 * @uml.property  name="description"
	 */
    public java.lang.String getDescription() {
        return description;
    }


    /**
	 * Sets the description value for this DLFolderSoap.
	 * @param  description
	 * @uml.property  name="description"
	 */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
	 * Gets the folderId value for this DLFolderSoap.
	 * @return  folderId
	 * @uml.property  name="folderId"
	 */
    public long getFolderId() {
        return folderId;
    }


    /**
	 * Sets the folderId value for this DLFolderSoap.
	 * @param  folderId
	 * @uml.property  name="folderId"
	 */
    public void setFolderId(long folderId) {
        this.folderId = folderId;
    }


    /**
	 * Gets the groupId value for this DLFolderSoap.
	 * @return  groupId
	 * @uml.property  name="groupId"
	 */
    public long getGroupId() {
        return groupId;
    }


    /**
	 * Sets the groupId value for this DLFolderSoap.
	 * @param  groupId
	 * @uml.property  name="groupId"
	 */
    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }


    /**
	 * Gets the lastPostDate value for this DLFolderSoap.
	 * @return  lastPostDate
	 * @uml.property  name="lastPostDate"
	 */
    public java.util.Calendar getLastPostDate() {
        return lastPostDate;
    }


    /**
	 * Sets the lastPostDate value for this DLFolderSoap.
	 * @param  lastPostDate
	 * @uml.property  name="lastPostDate"
	 */
    public void setLastPostDate(java.util.Calendar lastPostDate) {
        this.lastPostDate = lastPostDate;
    }


    /**
	 * Gets the modifiedDate value for this DLFolderSoap.
	 * @return  modifiedDate
	 * @uml.property  name="modifiedDate"
	 */
    public java.util.Calendar getModifiedDate() {
        return modifiedDate;
    }


    /**
	 * Sets the modifiedDate value for this DLFolderSoap.
	 * @param  modifiedDate
	 * @uml.property  name="modifiedDate"
	 */
    public void setModifiedDate(java.util.Calendar modifiedDate) {
        this.modifiedDate = modifiedDate;
    }


    /**
	 * Gets the mountPoint value for this DLFolderSoap.
	 * @return  mountPoint
	 * @uml.property  name="mountPoint"
	 */
    public boolean isMountPoint() {
        return mountPoint;
    }


    /**
	 * Sets the mountPoint value for this DLFolderSoap.
	 * @param  mountPoint
	 * @uml.property  name="mountPoint"
	 */
    public void setMountPoint(boolean mountPoint) {
        this.mountPoint = mountPoint;
    }


    /**
	 * Gets the name value for this DLFolderSoap.
	 * @return  name
	 * @uml.property  name="name"
	 */
    public java.lang.String getName() {
        return name;
    }


    /**
	 * Sets the name value for this DLFolderSoap.
	 * @param  name
	 * @uml.property  name="name"
	 */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
	 * Gets the overrideFileEntryTypes value for this DLFolderSoap.
	 * @return  overrideFileEntryTypes
	 * @uml.property  name="overrideFileEntryTypes"
	 */
    public boolean isOverrideFileEntryTypes() {
        return overrideFileEntryTypes;
    }


    /**
	 * Sets the overrideFileEntryTypes value for this DLFolderSoap.
	 * @param  overrideFileEntryTypes
	 * @uml.property  name="overrideFileEntryTypes"
	 */
    public void setOverrideFileEntryTypes(boolean overrideFileEntryTypes) {
        this.overrideFileEntryTypes = overrideFileEntryTypes;
    }


    /**
	 * Gets the parentFolderId value for this DLFolderSoap.
	 * @return  parentFolderId
	 * @uml.property  name="parentFolderId"
	 */
    public long getParentFolderId() {
        return parentFolderId;
    }


    /**
	 * Sets the parentFolderId value for this DLFolderSoap.
	 * @param  parentFolderId
	 * @uml.property  name="parentFolderId"
	 */
    public void setParentFolderId(long parentFolderId) {
        this.parentFolderId = parentFolderId;
    }


    /**
	 * Gets the primaryKey value for this DLFolderSoap.
	 * @return  primaryKey
	 * @uml.property  name="primaryKey"
	 */
    public long getPrimaryKey() {
        return primaryKey;
    }


    /**
	 * Sets the primaryKey value for this DLFolderSoap.
	 * @param  primaryKey
	 * @uml.property  name="primaryKey"
	 */
    public void setPrimaryKey(long primaryKey) {
        this.primaryKey = primaryKey;
    }


    /**
	 * Gets the repositoryId value for this DLFolderSoap.
	 * @return  repositoryId
	 * @uml.property  name="repositoryId"
	 */
    public long getRepositoryId() {
        return repositoryId;
    }


    /**
	 * Sets the repositoryId value for this DLFolderSoap.
	 * @param  repositoryId
	 * @uml.property  name="repositoryId"
	 */
    public void setRepositoryId(long repositoryId) {
        this.repositoryId = repositoryId;
    }


    /**
	 * Gets the userId value for this DLFolderSoap.
	 * @return  userId
	 * @uml.property  name="userId"
	 */
    public long getUserId() {
        return userId;
    }


    /**
	 * Sets the userId value for this DLFolderSoap.
	 * @param  userId
	 * @uml.property  name="userId"
	 */
    public void setUserId(long userId) {
        this.userId = userId;
    }


    /**
	 * Gets the userName value for this DLFolderSoap.
	 * @return  userName
	 * @uml.property  name="userName"
	 */
    public java.lang.String getUserName() {
        return userName;
    }


    /**
	 * Sets the userName value for this DLFolderSoap.
	 * @param  userName
	 * @uml.property  name="userName"
	 */
    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }


    /**
	 * Gets the uuid value for this DLFolderSoap.
	 * @return  uuid
	 * @uml.property  name="uuid"
	 */
    public java.lang.String getUuid() {
        return uuid;
    }


    /**
	 * Sets the uuid value for this DLFolderSoap.
	 * @param  uuid
	 * @uml.property  name="uuid"
	 */
    public void setUuid(java.lang.String uuid) {
        this.uuid = uuid;
    }

    /**
	 * @uml.property  name="__equalsCalc"
	 */
    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DLFolderSoap)) return false;
        DLFolderSoap other = (DLFolderSoap) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.companyId == other.getCompanyId() &&
            ((this.createDate==null && other.getCreateDate()==null) || 
             (this.createDate!=null &&
              this.createDate.equals(other.getCreateDate()))) &&
            this.defaultFileEntryTypeId == other.getDefaultFileEntryTypeId() &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            this.folderId == other.getFolderId() &&
            this.groupId == other.getGroupId() &&
            ((this.lastPostDate==null && other.getLastPostDate()==null) || 
             (this.lastPostDate!=null &&
              this.lastPostDate.equals(other.getLastPostDate()))) &&
            ((this.modifiedDate==null && other.getModifiedDate()==null) || 
             (this.modifiedDate!=null &&
              this.modifiedDate.equals(other.getModifiedDate()))) &&
            this.mountPoint == other.isMountPoint() &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            this.overrideFileEntryTypes == other.isOverrideFileEntryTypes() &&
            this.parentFolderId == other.getParentFolderId() &&
            this.primaryKey == other.getPrimaryKey() &&
            this.repositoryId == other.getRepositoryId() &&
            this.userId == other.getUserId() &&
            ((this.userName==null && other.getUserName()==null) || 
             (this.userName!=null &&
              this.userName.equals(other.getUserName()))) &&
            ((this.uuid==null && other.getUuid()==null) || 
             (this.uuid!=null &&
              this.uuid.equals(other.getUuid())));
        __equalsCalc = null;
        return _equals;
    }

    /**
	 * @uml.property  name="__hashCodeCalc"
	 */
    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getCompanyId()).hashCode();
        if (getCreateDate() != null) {
            _hashCode += getCreateDate().hashCode();
        }
        _hashCode += new Long(getDefaultFileEntryTypeId()).hashCode();
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        _hashCode += new Long(getFolderId()).hashCode();
        _hashCode += new Long(getGroupId()).hashCode();
        if (getLastPostDate() != null) {
            _hashCode += getLastPostDate().hashCode();
        }
        if (getModifiedDate() != null) {
            _hashCode += getModifiedDate().hashCode();
        }
        _hashCode += (isMountPoint() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        _hashCode += (isOverrideFileEntryTypes() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += new Long(getParentFolderId()).hashCode();
        _hashCode += new Long(getPrimaryKey()).hashCode();
        _hashCode += new Long(getRepositoryId()).hashCode();
        _hashCode += new Long(getUserId()).hashCode();
        if (getUserName() != null) {
            _hashCode += getUserName().hashCode();
        }
        if (getUuid() != null) {
            _hashCode += getUuid().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DLFolderSoap.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://model.documentlibrary.portlet.liferay.com", "DLFolderSoap"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "companyId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "createDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("defaultFileEntryTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "defaultFileEntryTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("folderId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "folderId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "groupId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastPostDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lastPostDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifiedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modifiedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mountPoint");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mountPoint"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overrideFileEntryTypes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "overrideFileEntryTypes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentFolderId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "parentFolderId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("primaryKey");
        elemField.setXmlName(new javax.xml.namespace.QName("", "primaryKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("repositoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "repositoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uuid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "uuid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
