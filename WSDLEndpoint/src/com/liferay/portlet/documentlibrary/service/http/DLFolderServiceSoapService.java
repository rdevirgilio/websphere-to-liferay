/**
 * DLFolderServiceSoapService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.liferay.portlet.documentlibrary.service.http;

/**
 * @author  stefano
 */
public interface DLFolderServiceSoapService extends javax.xml.rpc.Service {
    /**
	 * @uml.property  name="portlet_DL_DLFolderServiceAddress"
	 */
    public java.lang.String getPortlet_DL_DLFolderServiceAddress();

    /**
	 * @uml.property  name="portlet_DL_DLFolderService"
	 * @uml.associationEnd  
	 */
    public com.liferay.portlet.documentlibrary.service.http.DLFolderServiceSoap getPortlet_DL_DLFolderService() throws javax.xml.rpc.ServiceException;

    public com.liferay.portlet.documentlibrary.service.http.DLFolderServiceSoap getPortlet_DL_DLFolderService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
