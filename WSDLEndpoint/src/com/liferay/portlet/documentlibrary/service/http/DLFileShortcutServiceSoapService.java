/**
 * DLFileShortcutServiceSoapService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.liferay.portlet.documentlibrary.service.http;

/**
 * @author  stefano
 */
public interface DLFileShortcutServiceSoapService extends javax.xml.rpc.Service {
    /**
	 * @uml.property  name="portlet_DL_DLFileShortcutServiceAddress"
	 */
    public java.lang.String getPortlet_DL_DLFileShortcutServiceAddress();

    /**
	 * @uml.property  name="portlet_DL_DLFileShortcutService"
	 * @uml.associationEnd  
	 */
    public com.liferay.portlet.documentlibrary.service.http.DLFileShortcutServiceSoap getPortlet_DL_DLFileShortcutService() throws javax.xml.rpc.ServiceException;

    public com.liferay.portlet.documentlibrary.service.http.DLFileShortcutServiceSoap getPortlet_DL_DLFileShortcutService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
