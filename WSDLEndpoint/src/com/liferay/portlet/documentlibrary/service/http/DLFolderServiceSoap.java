/**
 * DLFolderServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.liferay.portlet.documentlibrary.service.http;

public interface DLFolderServiceSoap extends java.rmi.Remote {
    public com.liferay.portlet.documentlibrary.model.DLFolderSoap addFolder(long groupId, long repositoryId, boolean mountPoint, long parentFolderId, java.lang.String name, java.lang.String description, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException;
    public void deleteFolder(long folderId) throws java.rmi.RemoteException;
    public void deleteFolder(long groupId, long parentFolderId, java.lang.String name) throws java.rmi.RemoteException;
    public int getFileEntriesAndFileShortcutsCount(long groupId, long folderId, int status) throws java.rmi.RemoteException;
    public int getFileEntriesAndFileShortcutsCount(long groupId, long folderId, int status, java.lang.String[] mimeTypes) throws java.rmi.RemoteException;
    public long[] getFolderIds(long groupId, long folderId) throws java.rmi.RemoteException;
    public com.liferay.portlet.documentlibrary.model.DLFolderSoap getFolder(long folderId) throws java.rmi.RemoteException;
    public com.liferay.portlet.documentlibrary.model.DLFolderSoap getFolder(long groupId, long parentFolderId, java.lang.String name) throws java.rmi.RemoteException;
    public int getFoldersAndFileEntriesAndFileShortcutsCount(long groupId, long folderId, int status, java.lang.String[] mimeTypes, boolean includeMountFolders) throws java.rmi.RemoteException;
    public int getFoldersAndFileEntriesAndFileShortcutsCount(long groupId, long folderId, int status, boolean includeMountFolders) throws java.rmi.RemoteException;
    public int getFoldersAndFileEntriesAndFileShortcuts(long groupId, long folderId, int status, java.lang.String[] mimeTypes, boolean includeMountFolders) throws java.rmi.RemoteException;
    public int getFoldersCount(long groupId, long parentFolderId) throws java.rmi.RemoteException;
    public int getFoldersCount(long groupId, long parentFolderId, boolean includeMountfolders) throws java.rmi.RemoteException;
    public com.liferay.portlet.documentlibrary.model.DLFolderSoap[] getFolders(long groupId, long parentFolderId, boolean includeMountfolders, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException;
    public com.liferay.portlet.documentlibrary.model.DLFolderSoap[] getFolders(long groupId, long parentFolderId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException;
    public int getMountFoldersCount(long groupId, long parentFolderId) throws java.rmi.RemoteException;
    public com.liferay.portlet.documentlibrary.model.DLFolderSoap[] getMountFolders(long groupId, long parentFolderId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException;
    public void getSubfolderIds(long[] folderIds, long groupId, long folderId) throws java.rmi.RemoteException;
    public long[] getSubfolderIds(long groupId, long folderId, boolean recurse) throws java.rmi.RemoteException;
    public boolean hasFolderLock(long folderId) throws java.rmi.RemoteException;
    public boolean hasInheritableLock(long folderId) throws java.rmi.RemoteException;
    public boolean isFolderLocked(long folderId) throws java.rmi.RemoteException;
    public com.liferay.portlet.documentlibrary.model.DLFolderSoap moveFolder(long folderId, long parentFolderId, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException;
    public void unlockFolder(long groupId, long folderId, java.lang.String lockUuid) throws java.rmi.RemoteException;
    public void unlockFolder(long groupId, long parentFolderId, java.lang.String name, java.lang.String lockUuid) throws java.rmi.RemoteException;
    public com.liferay.portlet.documentlibrary.model.DLFolderSoap updateFolder(long folderId, java.lang.String name, java.lang.String description, long defaultFileEntryTypeId, long[] fileEntryTypeIds, boolean overrideFileEntryTypes, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException;
    public boolean verifyInheritableLock(long folderId, java.lang.String lockUuid) throws java.rmi.RemoteException;
}
