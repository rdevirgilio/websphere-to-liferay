package com.liferay.portlet.documentlibrary.service.http;

public class DLFolderServiceSoapProxy implements com.liferay.portlet.documentlibrary.service.http.DLFolderServiceSoap {
  /**
 * @uml.property  name="_endpoint"
 */
private String _endpoint = null;
  /**
 * @uml.property  name="dLFolderServiceSoap"
 * @uml.associationEnd  multiplicity="(1 1)"
 */
private com.liferay.portlet.documentlibrary.service.http.DLFolderServiceSoap dLFolderServiceSoap = null;
  
  public DLFolderServiceSoapProxy() {
    _initDLFolderServiceSoapProxy();
  }
  
  public DLFolderServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initDLFolderServiceSoapProxy();
  }
  
  private void _initDLFolderServiceSoapProxy() {
    try {
      dLFolderServiceSoap = (new com.liferay.portlet.documentlibrary.service.http.DLFolderServiceSoapServiceLocator()).getPortlet_DL_DLFolderService();
      if (dLFolderServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)dLFolderServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)dLFolderServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (dLFolderServiceSoap != null)
      ((javax.xml.rpc.Stub)dLFolderServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.liferay.portlet.documentlibrary.service.http.DLFolderServiceSoap getDLFolderServiceSoap() {
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap;
  }
  
  public com.liferay.portlet.documentlibrary.model.DLFolderSoap addFolder(long groupId, long repositoryId, boolean mountPoint, long parentFolderId, java.lang.String name, java.lang.String description, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.addFolder(groupId, repositoryId, mountPoint, parentFolderId, name, description, serviceContext);
  }
  
  public void deleteFolder(long folderId) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    dLFolderServiceSoap.deleteFolder(folderId);
  }
  
  public void deleteFolder(long groupId, long parentFolderId, java.lang.String name) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    dLFolderServiceSoap.deleteFolder(groupId, parentFolderId, name);
  }
  
  public int getFileEntriesAndFileShortcutsCount(long groupId, long folderId, int status) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.getFileEntriesAndFileShortcutsCount(groupId, folderId, status);
  }
  
  public int getFileEntriesAndFileShortcutsCount(long groupId, long folderId, int status, java.lang.String[] mimeTypes) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.getFileEntriesAndFileShortcutsCount(groupId, folderId, status, mimeTypes);
  }
  
  public long[] getFolderIds(long groupId, long folderId) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.getFolderIds(groupId, folderId);
  }
  
  public com.liferay.portlet.documentlibrary.model.DLFolderSoap getFolder(long folderId) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.getFolder(folderId);
  }
  
  public com.liferay.portlet.documentlibrary.model.DLFolderSoap getFolder(long groupId, long parentFolderId, java.lang.String name) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.getFolder(groupId, parentFolderId, name);
  }
  
  public int getFoldersAndFileEntriesAndFileShortcutsCount(long groupId, long folderId, int status, java.lang.String[] mimeTypes, boolean includeMountFolders) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.getFoldersAndFileEntriesAndFileShortcutsCount(groupId, folderId, status, mimeTypes, includeMountFolders);
  }
  
  public int getFoldersAndFileEntriesAndFileShortcutsCount(long groupId, long folderId, int status, boolean includeMountFolders) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.getFoldersAndFileEntriesAndFileShortcutsCount(groupId, folderId, status, includeMountFolders);
  }
  
  public int getFoldersAndFileEntriesAndFileShortcuts(long groupId, long folderId, int status, java.lang.String[] mimeTypes, boolean includeMountFolders) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.getFoldersAndFileEntriesAndFileShortcuts(groupId, folderId, status, mimeTypes, includeMountFolders);
  }
  
  public int getFoldersCount(long groupId, long parentFolderId) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.getFoldersCount(groupId, parentFolderId);
  }
  
  public int getFoldersCount(long groupId, long parentFolderId, boolean includeMountfolders) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.getFoldersCount(groupId, parentFolderId, includeMountfolders);
  }
  
  public com.liferay.portlet.documentlibrary.model.DLFolderSoap[] getFolders(long groupId, long parentFolderId, boolean includeMountfolders, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.getFolders(groupId, parentFolderId, includeMountfolders, start, end, obc);
  }
  
  public com.liferay.portlet.documentlibrary.model.DLFolderSoap[] getFolders(long groupId, long parentFolderId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.getFolders(groupId, parentFolderId, start, end, obc);
  }
  
  public int getMountFoldersCount(long groupId, long parentFolderId) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.getMountFoldersCount(groupId, parentFolderId);
  }
  
  public com.liferay.portlet.documentlibrary.model.DLFolderSoap[] getMountFolders(long groupId, long parentFolderId, int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.getMountFolders(groupId, parentFolderId, start, end, obc);
  }
  
  public void getSubfolderIds(long[] folderIds, long groupId, long folderId) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    dLFolderServiceSoap.getSubfolderIds(folderIds, groupId, folderId);
  }
  
  public long[] getSubfolderIds(long groupId, long folderId, boolean recurse) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.getSubfolderIds(groupId, folderId, recurse);
  }
  
  public boolean hasFolderLock(long folderId) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.hasFolderLock(folderId);
  }
  
  public boolean hasInheritableLock(long folderId) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.hasInheritableLock(folderId);
  }
  
  public boolean isFolderLocked(long folderId) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.isFolderLocked(folderId);
  }
  
  public com.liferay.portlet.documentlibrary.model.DLFolderSoap moveFolder(long folderId, long parentFolderId, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.moveFolder(folderId, parentFolderId, serviceContext);
  }
  
  public void unlockFolder(long groupId, long folderId, java.lang.String lockUuid) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    dLFolderServiceSoap.unlockFolder(groupId, folderId, lockUuid);
  }
  
  public void unlockFolder(long groupId, long parentFolderId, java.lang.String name, java.lang.String lockUuid) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    dLFolderServiceSoap.unlockFolder(groupId, parentFolderId, name, lockUuid);
  }
  
  public com.liferay.portlet.documentlibrary.model.DLFolderSoap updateFolder(long folderId, java.lang.String name, java.lang.String description, long defaultFileEntryTypeId, long[] fileEntryTypeIds, boolean overrideFileEntryTypes, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.updateFolder(folderId, name, description, defaultFileEntryTypeId, fileEntryTypeIds, overrideFileEntryTypes, serviceContext);
  }
  
  public boolean verifyInheritableLock(long folderId, java.lang.String lockUuid) throws java.rmi.RemoteException{
    if (dLFolderServiceSoap == null)
      _initDLFolderServiceSoapProxy();
    return dLFolderServiceSoap.verifyInheritableLock(folderId, lockUuid);
  }
  
  
}