/**
 * DLFileShortcutServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.liferay.portlet.documentlibrary.service.http;

public interface DLFileShortcutServiceSoap extends java.rmi.Remote {
    public com.liferay.portlet.documentlibrary.model.DLFileShortcutSoap addFileShortcut(long groupId, long folderId, long toFileEntryId, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException;
    public void deleteFileShortcut(long fileShortcutId) throws java.rmi.RemoteException;
    public com.liferay.portlet.documentlibrary.model.DLFileShortcutSoap getFileShortcut(long fileShortcutId) throws java.rmi.RemoteException;
    public com.liferay.portlet.documentlibrary.model.DLFileShortcutSoap updateFileShortcut(long fileShortcutId, long folderId, long toFileEntryId, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException;
}
