package com.liferay.portlet.documentlibrary.service.http;

public class DLFileShortcutServiceSoapProxy implements com.liferay.portlet.documentlibrary.service.http.DLFileShortcutServiceSoap {
  /**
 * @uml.property  name="_endpoint"
 */
private String _endpoint = null;
  /**
 * @uml.property  name="dLFileShortcutServiceSoap"
 * @uml.associationEnd  multiplicity="(1 1)"
 */
private com.liferay.portlet.documentlibrary.service.http.DLFileShortcutServiceSoap dLFileShortcutServiceSoap = null;
  
  public DLFileShortcutServiceSoapProxy() {
    _initDLFileShortcutServiceSoapProxy();
  }
  
  public DLFileShortcutServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initDLFileShortcutServiceSoapProxy();
  }
  
  private void _initDLFileShortcutServiceSoapProxy() {
    try {
      dLFileShortcutServiceSoap = (new com.liferay.portlet.documentlibrary.service.http.DLFileShortcutServiceSoapServiceLocator()).getPortlet_DL_DLFileShortcutService();
      if (dLFileShortcutServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)dLFileShortcutServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)dLFileShortcutServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (dLFileShortcutServiceSoap != null)
      ((javax.xml.rpc.Stub)dLFileShortcutServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.liferay.portlet.documentlibrary.service.http.DLFileShortcutServiceSoap getDLFileShortcutServiceSoap() {
    if (dLFileShortcutServiceSoap == null)
      _initDLFileShortcutServiceSoapProxy();
    return dLFileShortcutServiceSoap;
  }
  
  public com.liferay.portlet.documentlibrary.model.DLFileShortcutSoap addFileShortcut(long groupId, long folderId, long toFileEntryId, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (dLFileShortcutServiceSoap == null)
      _initDLFileShortcutServiceSoapProxy();
    return dLFileShortcutServiceSoap.addFileShortcut(groupId, folderId, toFileEntryId, serviceContext);
  }
  
  public void deleteFileShortcut(long fileShortcutId) throws java.rmi.RemoteException{
    if (dLFileShortcutServiceSoap == null)
      _initDLFileShortcutServiceSoapProxy();
    dLFileShortcutServiceSoap.deleteFileShortcut(fileShortcutId);
  }
  
  public com.liferay.portlet.documentlibrary.model.DLFileShortcutSoap getFileShortcut(long fileShortcutId) throws java.rmi.RemoteException{
    if (dLFileShortcutServiceSoap == null)
      _initDLFileShortcutServiceSoapProxy();
    return dLFileShortcutServiceSoap.getFileShortcut(fileShortcutId);
  }
  
  public com.liferay.portlet.documentlibrary.model.DLFileShortcutSoap updateFileShortcut(long fileShortcutId, long folderId, long toFileEntryId, com.liferay.portal.service.ServiceContext serviceContext) throws java.rmi.RemoteException{
    if (dLFileShortcutServiceSoap == null)
      _initDLFileShortcutServiceSoapProxy();
    return dLFileShortcutServiceSoap.updateFileShortcut(fileShortcutId, folderId, toFileEntryId, serviceContext);
  }
  
  
}