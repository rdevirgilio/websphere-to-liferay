package parser;

import java.io.File;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import model.Node;

/**
 * Class that uses the SAX parser to parse correctly 
 * and NodeHandler files .node
 *
 */
public class XMLParser {

	private static XMLParser parser;	

	public static XMLParser getParser(){
		if(parser==null){
			parser = new XMLParser();
		}
		return parser;
	}	

	/**
	 * Method that parse the .node file
	 * 
	 * @param File .node to parse.
	 * @return Node file that contains all the data parsed.
	 */
	
	public Node parseNode(File file) {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser;
		NodesHandler nodesHandler = NodesHandler.getNodesHandler();
		try {
			saxParser = factory.newSAXParser();
			saxParser.parse(file, nodesHandler);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nodesHandler.getNodeResult();
	}

}
