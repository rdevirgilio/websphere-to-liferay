package parser;

import java.util.LinkedList;
import java.util.List;

import model.TagPropertyValue;
import model.Node;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import util.Base64Converter;

/**
 * Handler class used by SAX to parse correctly the file .node
 * 
 * Parse the contents of the file and creates an object Node 
 * (currentNode) that contains all the properties parsed.
 * 
 */
public class NodesHandler extends DefaultHandler {

	private static NodesHandler nodesHandler;
	private static Node nodeResult;
	//private static Base64Converter base64Converter;

	/**
	 * @uml.property  name="property"
	 */
	private boolean property = false;	
	/**
	 * @uml.property  name="nodeType"
	 */
	private boolean nodeType = false;
	/**
	 * @uml.property  name="svName"
	 */
	private String svName = null;
	/**
	 * @uml.property  name="svValueMimeType"
	 */
	private String svValueMimeType = null;

	/**
	 * @uml.property  name="resourceName"
	 */
	private boolean resourceName = false;
	/**
	 * @uml.property  name="wcmName"
	 */
	private boolean wcmName = false;
	/**
	 * @uml.property  name="label"
	 */
	private boolean label = false;
	/**
	 * @uml.property  name="title"
	 */
	private boolean title = false; 
	/**
	 * @uml.property  name="description"
	 */
	private boolean description = false;
	/**
	 * @uml.property  name="classification"
	 */
	private boolean classification = false; 
	/**
	 * @uml.property  name="isDoc"
	 */
	private boolean isDoc = false; 
	
	/**
	 * Lists used for filtering CData of interest
	 */
	private static List<String> svNames;
	private static List<String> mimeTypes;
	/**
	 * @uml.property  name="mimeType"
	 */
	private boolean mimeType = false;
	/**
	 * @uml.property  name="cDATAstarted"
	 */
	private boolean CDATAstarted = false;
	
	private boolean authoringStyleMap=false;
	
	private boolean presentationStyleMap=false;

	
	public static NodesHandler getNodesHandler(){		
		if(nodesHandler==null){
			
			nodeResult = new Node();
			//base64Converter = new Base64Converter();
			nodesHandler = new NodesHandler();
			/**
			 * List initialization
			 */
			svNames = new LinkedList<String>(){
				private static final long serialVersionUID = 7478819330857309073L;
			{				
				add("ibmcontentwcm:html");
				add("ibmcontentwcm:elementData");
				add("icm:data");
				add("ibmcontentwcm:authoringStyleMap");
				/*l'add nodeType e authors sono stati inseriti da DD*/
				add("jcr:nodeType");
				add("icm:authors");
				//add("ibmcontentwcm:effectivePermissions");
				//add("ibmcontentwcm:historyLogEntries");
			}};
			
			mimeTypes = new LinkedList<String>(){
				private static final long serialVersionUID = 8153731667965761104L;
			{
				add("text/plain");
				add("application/octet-stream");
				add("application/pdf");
				add("application/javascript");
				add("application/json");
				add("text/javascript");
				add("application/zip");
				add("audio/mpeg");
				add("image/gif");
				add("image/jpeg");
				add("image/png");
				add("text/css");
				add("text/xml");
			}};
			
		}
		
		nodeResult = new Node();
		return nodesHandler;

	}

	public void startElement(String uri, String localName,String qName, 
			Attributes attributes) throws SAXException {

		if (qName.equalsIgnoreCase("icm:node")) {

			if(attributes.getValue("icm:path")!=null)	{
				getNodeResult().setPath(attributes.getValue("icm:path"));						
			}

			if(attributes.getValue("icm:workspace")!=null)	{
				getNodeResult().setWorkspace(attributes.getValue("icm:workspace"));						
			}

			if(attributes.getValue("icm:uuid")!=null)	{
				getNodeResult().setUUID(attributes.getValue("icm:uuid"));						
			}					

		}

		if (qName.equalsIgnoreCase("sv:node")) {

			if(attributes.getValue("sv:name")!=null)	{
				getNodeResult().setName(attributes.getValue("sv:name"));						
			}	
		}

		if (qName.equalsIgnoreCase("sv:property")) {
			
			setProperty(true);
			
			if(attributes.getValue("sv:name")!=null && attributes.getValue("sv:name").equals("jcr:nodeType"))	{
				nodeType = true;
			}	

			if(attributes.getValue("sv:name")!=null && svNames.contains(attributes.getValue("sv:name")))	{
				mimeType = true;	
				svName = attributes.getValue("sv:name");
			}	
			
			if(attributes.getValue("sv:name")!=null && attributes.getValue("sv:name").equals("ibmcontentwcm:resourceName"))	{
				resourceName = true;					
			}
			
			if(attributes.getValue("sv:name")!=null && attributes.getValue("sv:name").equals("ibmcontentwcm:name"))	{
				wcmName = true;					
			}

			if(attributes.getValue("sv:name")!=null && attributes.getValue("sv:name").equals("icm:label"))	{
				label = true;					
			}	

			if(attributes.getValue("sv:name")!=null && attributes.getValue("sv:name").equals("icm:title"))	{
				title = true;					
			}	

			if(attributes.getValue("sv:name")!=null && attributes.getValue("sv:name").equals("icm:description"))	{
				description = true;					
			}	

			if(attributes.getValue("sv:name")!=null && attributes.getValue("sv:name").equals("ibmcontentwcm:classification"))	{
				classification = true;					
			}	

			if(attributes.getValue("sv:name")!=null && attributes.getValue("sv:name").equals("icm:isdoc"))	{
				isDoc = true;					
			}
			
			if(attributes.getValue("sv:name")!=null && attributes.getValue("sv:name").equals("ibmcontentwcm:authoringStyleMap"))	{
				authoringStyleMap = true;					
			}
			
			if(attributes.getValue("sv:name")!=null && attributes.getValue("sv:name").equals("ibmcontentwcm:presentationStyleMap"))	{
				presentationStyleMap = true;					
			}
			
			
			
		}

		if (qName.equalsIgnoreCase("sv:value")) {								
			if(mimeType && mimeTypes.contains(attributes.getValue("mimeType"))){
				CDATAstarted = true;
				svValueMimeType = attributes.getValue("mimeType");
			}	
		}
	}

	public void endElement(String uri, String localName,
			String qName) throws SAXException {

		if(qName.equals("sv:property")){
			setProperty(false);
			nodeType = false;
			mimeType = false;			
			resourceName = false;
			wcmName = false;	
			label = false;
			title = false;
			description = false;
			classification = false;
			CDATAstarted = false;
			isDoc = false;
			authoringStyleMap=false;
			presentationStyleMap=false;
		}

	}

	public void characters(char ch[], int start, int length) throws SAXException {

		if (nodeType) {
			getNodeResult().setNodeType(new String(ch, start, length));
			nodeType = false;
		}

		if (mimeType && CDATAstarted ) {			
			getNodeResult().getTagPropertyValue2CDATAMap().put(
					new TagPropertyValue(svName,svValueMimeType),new String(ch, start, length))	;
			mimeType = false;
			CDATAstarted = false;
		}
		
		if (resourceName) {
			getNodeResult().setResourceName((new String(ch, start, length)));
			resourceName = false;
		}
		
		if (wcmName) {
			getNodeResult().setWcmName((new String(ch, start, length)));
			wcmName = false;
		}

		if (label) {
			getNodeResult().setLabel((new String(ch, start, length)));
			label = false;
		}

		if (title) {
			getNodeResult().setTitle((new String(ch, start, length)));
			title = false;
		}

		if (description) {
			getNodeResult().setDescription((new String(ch, start, length)));
			description = false;
		}

		if (classification) {
			getNodeResult().setClassification((new String(ch, start, length)));
			classification = false;
		}
		
		if (isDoc) {
			getNodeResult().setIsDoc((new String(ch, start, length)));
			isDoc = false;
		}
		
		if(authoringStyleMap){
			
			getNodeResult().getAuthoringStyleMap().add((new String(ch, start, length)));
			//authoringStyleMap=false;
		}
		
		if(presentationStyleMap){
			
			getNodeResult().getPresentationStyleMap().add((new String(ch, start, length)));
			//presentationStyleMap=false;
		}
		
		

	}

	public Node getNodeResult() {
		return nodeResult;
	}

	/**
	 * @return
	 * @uml.property  name="property"
	 */
	public boolean isProperty() {
		return property;
	}

	/**
	 * @param property
	 * @uml.property  name="property"
	 */
	public void setProperty(boolean property) {
		this.property = property;
	}

}
