package portlets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import store.SiteAreaStore;
import util.IDBroker;
import it.liferay.classimport.ImporterFacade;
import model.Node;

public class CategoriesDispatcher {
	
	private static CategoriesDispatcher categoriesDispatcher;

	public static CategoriesDispatcher getInstance(){
		if(categoriesDispatcher==null)
		{
			categoriesDispatcher = new CategoriesDispatcher();
		}
		
		return categoriesDispatcher;
	}

	public Long processSiteArea(Node currentNode) {
		String title = currentNode.getTitle().toLowerCase();
		String description = currentNode.getDescription();
		boolean root = false;
		Long returnedId = null;
		Long parentId = null;
		if (currentNode.getPath().matches(".*/Content$")){
			root = true;
		}
		
		if (root){
			
			returnedId = ImporterFacade.addCategoryRoot(title, description);
			SiteAreaStore.getSiteArea2returnedId().put(title, returnedId);
		}
		else{
			parentId = returnParentId(currentNode.getPath());
			System.out.println("path: " + currentNode.getPath());
			
			if(parentId==null) {
				System.out.println("padre non trovato");
				System.out.println(SiteAreaStore.getSiteArea2returnedId().toString());
			}
			else{
				returnedId = ImporterFacade.addCategory(parentId, title, description);
			SiteAreaStore.getSiteArea2returnedId().put(title, returnedId);
			}
		}
		return returnedId;
	}
	
	private static Long returnParentId(String path){
		String categoryFatherName = path.substring(path.lastIndexOf("/")+1).toLowerCase();
		System.out.println("categoryFatherName: "+categoryFatherName);
		//String categoryFatherName = splitPath[splitPath.length-1]; 
		return SiteAreaStore.getSiteArea2returnedId().get(categoryFatherName);
	}
	
	
	
}
