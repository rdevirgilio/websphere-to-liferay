package portlets;

import controller.NodeDispatcher;
import it.liferay.classimport.ImporterFacade;
import model.Node;
import model.TagPropertyValue;
import store.HierarchyStore;
import util.Base64Converter;
import util.NodeHelper;

public class DocumentsAndMediaDispatcher {
	
	private static Long returnedId;
	
	private static DocumentsAndMediaDispatcher DMDispatcher;
	
	public static DocumentsAndMediaDispatcher getInstance(){
		if(DMDispatcher==null)
		{
			DMDispatcher = new DocumentsAndMediaDispatcher();
		}
		
		return DMDispatcher;
	}
	
	
	public Long processDM(Node currentNode) {
		// TODO Auto-generated method stub
		return null;
	}
	

	public Long buildRoot(Node currentNode) throws Exception {

		String name;
		String name_temp = NodeHelper.getNodeName(currentNode);
		if (name_temp.length() >= 100)
			name = name_temp.substring(0, 50);
		else
			name = name_temp;
		currentNode.setName(name);
		currentNode.setLabel(name);
		currentNode.setTitle(name);

		Long returnedId = ImporterFacade.addDirectoryInRoot(currentNode
				.getName());
		if (returnedId == ImporterFacade.ERRORCODE) {
			throw new Exception("ERROR - Error importing node:folder ID: "
					+ currentNode.getUUID() + " NAME: " + currentNode.getName());
		}
		// MONITORING
		System.out.println();
		System.out.println("Root node detected.");
		System.out.println("Root name:" + currentNode.getName());
		System.out.println();
		// MONITORING

		return returnedId;
	}
	
		
	
	
	
	public Long dispatchNode(Node currentNode, String absPathNodeFile){
		
		

	
	return returnedId;
	
}
	
public Long buildFile(TagPropertyValue tagPropertyValue,Node currentNode){
	
	
	Long returnedId;
	String mimeType = tagPropertyValue.getMimeType();
	String name;
	String name_temp=NodeHelper.getResourceName(mimeType,currentNode);
	if(name_temp.length()>=100)
		name = name_temp.substring(0,50);
	else
		name = name_temp;
	currentNode.setName(name);
	currentNode.setLabel(name);
	currentNode.setTitle(name);
	/*
	 * Is there a file .value as real resource?  
	 */
	if( isFileFromPath(currentNode, tagPropertyValue) )
	{
		
		/*
		 * Yes, building the path to the actual file..
		 */
		
		String valuePath = buildValuePath(currentNode.getAbsFilePath(),tagPropertyValue);
	
		//MONITORING
		System.out.println();
		System.out.println("Resource from path detected");
		System.out.print("name: "+name);
		System.out.print(" mimeType: "+mimeType);
		System.out.print(" valuePath: "+valuePath);
		System.out.println();
		//MONITORING
		
		if (currentNode.getDescription() != null && currentNode.getTitle() != null)
		
			returnedId = ImporterFacade.addFileFromPath(valuePath,
					HierarchyStore.getUUID2newID().get(currentNode.getParentUUID()), name, mimeType,
					currentNode.getTitle(),
					currentNode.getDescription());
		
		else if (currentNode.getTitle() != null)
			
			returnedId = ImporterFacade.addFileFromPath(valuePath,
					HierarchyStore.getUUID2newID().get(currentNode.getParentUUID()), name, mimeType,
					currentNode.getTitle());
		
		else
			returnedId = ImporterFacade.addFileFromPath(valuePath,
					HierarchyStore.getUUID2newID().get(currentNode.getParentUUID()), name, mimeType);
	}
	
	else 
	{
		/*
		 * No, I'll consider the CDATA section coded in Base64
		 */
		String base64Content = currentNode.getTagPropertyValue2CDATAMap().get(tagPropertyValue);
		
		//MONITORING
		System.out.println("Resource from base64 detected.");
		System.out.println("name: "+name);
		System.out.println(" mimeType: "+mimeType);
		//System.out.println(" base64content: "+base64Content);
		System.out.println();
		//MONITORING
		
		if (currentNode.getDescription() != null && currentNode.getTitle() != null)
			returnedId = ImporterFacade.addFileFromBase64(base64Content,
					HierarchyStore.getUUID2newID().get(currentNode.getParentUUID()), name, mimeType,
					currentNode.getTitle(),
					currentNode.getDescription());
		else if (currentNode.getTitle() != null)
			returnedId = ImporterFacade.addFileFromBase64(base64Content,
					HierarchyStore.getUUID2newID().get(currentNode.getParentUUID()), name, mimeType,
					currentNode.getTitle());
		else
			returnedId = ImporterFacade.addFileFromBase64(base64Content,
					HierarchyStore.getUUID2newID().get(currentNode.getParentUUID()), name, mimeType);
	}

	

	return returnedId;

}


public Long buildDirectory(Node currentNode) throws Exception {
	Long returnedId = ImporterFacade.ERRORCODE;
	String name;
	String name_temp=NodeHelper.getNodeName(currentNode);
	if(name_temp.length()>=100)
		name = name_temp.substring(0,50);
	else
		name = name_temp;
	currentNode.setName(name);
	currentNode.setLabel(name);
	currentNode.setTitle(name);
	
	if(currentNode.getDescription()!=null&&currentNode.getDescription().length()>=100)
		currentNode.setDescription(currentNode.getDescription().substring(0, 50));
	
	if(name.length()>=100)
		name = name.substring(0,50);

	if (!HierarchyStore.getUUID2newID().containsKey(currentNode.getParentUUID())) {
		throw new Exception("ERROR - Error importing node:folder (no parent) : " + currentNode.getUUID() + "name:" + currentNode.getParentName() + " " +currentNode.getFileName() + " " +currentNode.getPath());
	}

	if (currentNode.getDescription() != null)
		returnedId = ImporterFacade.addDirectory(name,
				currentNode.getDescription(), HierarchyStore.getUUID2newID().get(currentNode.getParentUUID()) );
	else
		returnedId = ImporterFacade.addDirectory(name,
				HierarchyStore.getUUID2newID().get(currentNode.getParentUUID()) );

	if (returnedId == ImporterFacade.ERRORCODE)
	{
		throw new Exception("ERROR - Error importing node: "+currentNode.getName()+" "+ currentNode.getUUID()+" "+currentNode.getPath());
	}
	
	//MONITORING
	System.out.println("Folder detected.");
	System.out.print("name: "+name);
	System.out.print(" absFilePath: "+currentNode.getAbsFilePath());
	System.out.println();
	//MONITORING

	return returnedId;

}		



private boolean isFileFromPath(Node currentNode, TagPropertyValue d) {

	 String rawCDATA = currentNode.getTagPropertyValue2CDATAMap().get(d);
	 String CDATA = new String(Base64Converter.base64StringConvert(rawCDATA));
	 return  CDATA.contains("icm%3adata.value") || CDATA.contains("ibmcontentwcm%3ahtml.value");
	
}
	

public String buildValuePath(String absPath,TagPropertyValue tagPropertyValue) {
	String path = absPath.substring(0, absPath.lastIndexOf('.'));
	
	if(tagPropertyValue.getName().equals("icm:data"))
		return (path.concat(System.getProperty("file.separator")+"icm%3adata.value"));
	
	if(tagPropertyValue.getName().equals("ibmcontentwcm:html"))
		return (path.concat(System.getProperty("file.separator")+"ibmcontentwcm%3ahtml.value"));
	
	return null;
	
}




}
