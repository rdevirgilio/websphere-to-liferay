package portlets;

public class TemplatesDispatcher {
	
	private static TemplatesDispatcher templateDispatcher;

	public static TemplatesDispatcher getInstance() {
		if (templateDispatcher == null)
			templateDispatcher = new TemplatesDispatcher();
		return templateDispatcher;
	}

}
