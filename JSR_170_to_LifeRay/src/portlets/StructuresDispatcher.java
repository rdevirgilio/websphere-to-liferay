package portlets;

import it.liferay.classimport.ImporterFacade;
import store.SiteAreaStore;
import util.IDBroker;
import model.Node;

public class StructuresDispatcher {

	private static StructuresDispatcher structureDispatcher;
	
	public static StructuresDispatcher getInstance() {
		if(structureDispatcher == null)
			structureDispatcher = new StructuresDispatcher();
		return structureDispatcher;
	}

	public Long processPresentation(Node currentNode) {
		String title = currentNode.getTitle().toLowerCase();
		String description = currentNode.getDescription().toLowerCase();
		Long returnedId = null;
		
		boolean root = false;
		Long parentId = null;
		if (currentNode.getPath().matches(".*/Presentation Templates$")){
			root = true;
		}
		if (root){
			returnedId = (new IDBroker()).newID();
			ImporterFacade.addStructureInRoot(returnedId, title, description);
			SiteAreaStore.getPresentationTemplates2Id().put(title, returnedId);
		}
		else
		{
			
			parentId = returnParentId(currentNode.getPath());
			returnedId = (new IDBroker()).newID(); /*genero prima l'id*/
			ImporterFacade.addStructure(parentId, returnedId, title, description);
			SiteAreaStore.getPresentationTemplates2Id().put(title, returnedId);
		}
		return returnedId;
	}

	private static Long returnParentId(String path){
		String structureFatherName = path.substring(path.lastIndexOf("/")+1).toLowerCase();
		System.out.println("FatherName: "+structureFatherName);
		return SiteAreaStore.getPresentationTemplates2Id().get(structureFatherName);

	}
}
