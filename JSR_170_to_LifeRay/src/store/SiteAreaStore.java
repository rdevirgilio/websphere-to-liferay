package store;


import java.awt.geom.Area;
import java.util.ArrayList;
import java.util.HashMap;

public class SiteAreaStore extends HierarchyStore{
	
	static{
		getAreaStore();
	}
	
	private static SiteAreaStore store = null;
	private static ArrayList<String> authoringTemplates;
	private static ArrayList<String> presentationTemplates;
	private static HashMap<String,ArrayList<String>> siteAreaUUID2authoringTemplates;
	private static HashMap<String,ArrayList<String>> siteAreaUUID2presentationTemplates;
	private static HashMap<String,String> siteArea2UUID;
	private static HashMap<String, Long> siteArea2returnedId;
	private static HashMap<String, Long> presentationTemplates2Id;
	
	
	
	public static SiteAreaStore getAreaStore(){
		
		if(store==null)
		{
			store = new SiteAreaStore();
			siteArea2returnedId = new HashMap<String, Long>();
			presentationTemplates2Id = new HashMap<String, Long>();
		}
		return store;
		
	}
	
	public static HashMap<String, Long> getPresentationTemplates2Id() {
		return presentationTemplates2Id;
	}
	public static HashMap<String, Long> getSiteArea2returnedId() {
		return siteArea2returnedId;
	}
	
	public static ArrayList<String> getAuthoringTemplates() {
		return authoringTemplates;
	}
	public static void setAuthoringTemplates(ArrayList<String> authoringTemplates) {
		SiteAreaStore.authoringTemplates = authoringTemplates;
	}
	public static ArrayList<String> getPresentationTemplates() {
		return presentationTemplates;
	}
	public static void setPresentationTemplates(
			ArrayList<String> presentationTemplates) {
		SiteAreaStore.presentationTemplates = presentationTemplates;
	}
	public static HashMap<String, ArrayList<String>> getSiteArea2authoringTemplates() {
		return siteAreaUUID2authoringTemplates;
	}
	public static void setSiteArea2authoringTemplates(
			HashMap<String, ArrayList<String>> siteArea2authoringTemplates) {
		SiteAreaStore.siteAreaUUID2authoringTemplates = siteArea2authoringTemplates;
	}
	public static HashMap<String, ArrayList<String>> getSiteArea2presentationTemplates() {
		return siteAreaUUID2presentationTemplates;
	}
	public static void setSiteArea2presentationTemplates(
			HashMap<String, ArrayList<String>> siteArea2presentationTemplates) {
		SiteAreaStore.siteAreaUUID2presentationTemplates = siteArea2presentationTemplates;
	}
	public static HashMap<String, String> getSiteArea2UUID() {
		return siteArea2UUID;
	}
	public static void setSiteArea2UUID(HashMap<String, String> siteArea2UUID) {
		SiteAreaStore.siteArea2UUID = siteArea2UUID;
	}
	
	}
