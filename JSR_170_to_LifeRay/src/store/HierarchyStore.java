package store;

import java.util.HashMap;
import java.util.Map;

public class HierarchyStore {
	
	static {
		getHierarchyStore();
	}
	/**
	 * Map used to associate the UUID (relative to WebSpere) with the new ID
	 * obtained by putting that file/folder in LifeRay
	 */
	private static Map<String, Long> UUID2newID;
	
	/**
	 * Map used to associate the node file path with its UUID.
	 */
	private static Map<String, String> path2UUID;
	private static HierarchyStore store;
	public static HierarchyStore getHierarchyStore(){
		
		if(store==null)
		{
			store = new HierarchyStore();
			UUID2newID = new HashMap<String,Long>();
			path2UUID = new HashMap<String,String>();
			
			
		}
		return store;
		
	}
	public static Map<String, Long> getUUID2newID() {
		return UUID2newID;
	}
	public static void setUUID2newID(Map<String, Long> uUID2newID) {
		UUID2newID = uUID2newID;
	}
	public static Map<String, String> getPath2UUID() {
		return path2UUID;
	}
	public static void setPath2UUID(Map<String, String> path2uuid) {
		path2UUID = path2uuid;
	}
	public static HierarchyStore getStore() {
		return store;
	}
	public static void setStore(HierarchyStore store) {
		HierarchyStore.store = store;
	}
	


}
