package util;

import java.util.UUID;

/**
 * Class that handles the creation of new UUID
 *
 */
public class UUIDUtil {
	
	/**
	 * Method that generate a new UUID
	 * 
	 * @return UUID string
	 */
	public static String generate(){
		return UUID.randomUUID().toString();
	}
	
	public static Long ToLong(String UUID){
		return Long.getLong(UUID);
	}


}
