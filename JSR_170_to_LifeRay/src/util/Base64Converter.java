package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

/**
 * Class that handles the conversion and decoding strings in Base64 format
 * for the CDATA fields.
 */
public class Base64Converter {

	/**
	 * Decode a base64 string and puts it in a byte array.
	 * 
	 * @param base64EncodedString
	 * @return a byte array
	 */
	public static byte[] base64StringConvert(String base64EncodedString) {
		return (new Base64()).decode(base64EncodedString.getBytes());
	}

	/**
	 * Read a file from the file at the specified path and puts it in a byte array.
	 * 
	 * @param path where the file is located in the file
	 * @return
	 */
	public byte[] readFileAndConvert(String path) {

		byte array[] = null;
		
		try {
			array = IOUtils.toByteArray(new FileInputStream(new File(path)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;

	}
}