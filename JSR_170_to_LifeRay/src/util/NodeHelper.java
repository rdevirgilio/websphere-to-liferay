package util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;

import model.Node;
import model.TagPropertyValue;

public class NodeHelper {

	private static HashMap<String, String> ResourceMimeTypes = new HashMap<String, String>() {

		/**
		 * 
		 */
		private static final long serialVersionUID = -4779045379050774553L;
		{

			put("application/pdf", ".pdf");
			put("text/css", ".css");
			put("image/jpeg", ".jp");
			put("image/gif", ".gif");
			put("image/png", ".png");
			put("text/html",".ht");
			put("application/octet-stream",".");
		}
	};

	

	public static String getNodeName(Node currentNode) {

		String defaultName = decodeURL(currentNode.getName());
		String title = currentNode.getTitle();
		String label = currentNode.getLabel();

		if(isValidWord(defaultName))
			return defaultName;
		
		if (label != null)
		{
//			//MONITORING
//			System.out.println("NodeHelper.getName label: "+label);
//			//MONITORING
			if(isValidWord(label))
				return label;
			
			return replaceBadChars(label);
			
		}

		
		if (title != null)
		{
//			//MONITORING
//			System.out.println("NodeHelper.getName title: "+title);
//			//MONITORING
//			
			if(isValidWord(title))
				return title;
				
			return replaceBadChars(title);
			
		}

//		//MONITORING
//		System.out.println("NodeHelper.getName defaultName: "+defaultName);
//		//MONITORING
//		
		return replaceBadChars(defaultName);

	}


	public static String getResourceName(String mimeType, Node currentNode)
	{
		String resourceName = currentNode.getResourceName();
		String ibmContentWcmName = currentNode.getWcmName();
		String title = currentNode.getTitle();
		String classification = currentNode.getClassification();
//		if(nodeType.equalsIgnoreCase("ibmcontentwcm:resourceElement"))
//		{
		if(resourceName!=null)
		{
			if(resourceName.contains("."))
			{

//				//MONITORING
//				System.out.println("NodeHelper.getResource ResourceName: "+currentNode.getResourceName());
//				//MONITORING
//		
				if(isValidWord(resourceName))
					return resourceName;
				
				resourceName = replaceBadChars(resourceName);
				return resourceName;
				
			}
		}
		
		if(ibmContentWcmName!=null)
		{
			if(isValidWord(ibmContentWcmName))
			{
				currentNode.setTitle(ibmContentWcmName);
				currentNode.setLabel(ibmContentWcmName);
				return ibmContentWcmName;
			}
			ibmContentWcmName = replaceBadChars(ibmContentWcmName);
			currentNode.setTitle(ibmContentWcmName);
			currentNode.setLabel(ibmContentWcmName);
			return ibmContentWcmName;
			
		}
		
//		//MONITORING
//		System.out.println("NodeHelper.getResource DefaultName "+defaultName);
//		//MONITORING
		
		if(title!=null)
		{
			if(isValidWord(title))
				return title;
			return replaceBadChars(title);
		}
		

		String id = Integer.toString(IDgenerator.generateId());
		if(isValidWord(classification))
				return classification.concat("_").concat(id);
	
		return ( replaceBadChars(currentNode.getClassification()) ).concat("_").concat(id);
	

	}
	
	
	/**
	 * Methods for managing data types and tags for resources (icm:data,wcm:hmtml..)
	 */
	
	public static TagPropertyValue getPropertyValue(String propertyName, Node currentNode){
		TagPropertyValue property = null;
		
		for (TagPropertyValue t : currentNode.getTagPropertyValue2CDATAMap().keySet())
		{
			if(t.getName()!=null&&t.getName().equals(propertyName))
			{	
				if(t.getName().equals("ibmcontentwcm:html"))	
					correctHtmlExtension(currentNode,t);
				
				//MONITORING
				System.out.println(propertyName+" section: "+t.getName()+" "+t.getMimeType() );
				return t;
			}
		}
		return property;

		
	}
		

	private static void correctHtmlExtension(Node currentNode,TagPropertyValue t) {
		System.out.println();
		System.out.println("Correcting html mimeType...");
		System.out.println();
		String rawCDATA = currentNode.getTagPropertyValue2CDATAMap().remove(t);
		String mimeType = "text/html";
		t.setMimeType(mimeType);
		currentNode.getTagPropertyValue2CDATAMap().put(t, rawCDATA);
	}


	/**
	 * Method used to decode an encoded URL String
	 * 
	 * @param encodedURD
	 * @return
	 */
	public static String decodeURL(String encodedURD) {

		try {
			return URLDecoder.decode(encodedURD, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return encodedURD;
		}

	}

	public static boolean isValidWord(String word) {

		if (CharPool.isNull(word)) {
			return false;
		} else {
			char[] wordCharArray = word.toCharArray();

			for (char c : wordCharArray) {
				for (char invalidChar : CharPool.INVALID_CHARACTERS) {
					if (c == invalidChar)
						return false;

				}
			}
		}

		return true;
	}

	public static String replaceBadChars(String name) {
	

		for (char c : CharPool.INVALID_CHARACTERS) {
			name = name.replace(c, '_');
		}

		return name;
	}

}
