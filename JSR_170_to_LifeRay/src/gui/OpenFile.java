package gui;

import java.io.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class OpenFile {
	
	/**
	 * Load a simple file browser to search the file with all sequences.
	 * The param "filter" it's used to set what type of extension will be showed
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	
	public static File getFileFromBrowser(String filter) throws Exception {

		JFileChooser chooser = new JFileChooser();
		
		chooser.setAcceptAllFileFilterUsed(false);
		
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			
//		chooser.addChoosableFileFilter(new FileNameExtensionFilter("Node Files", "node"));
			
		
		int returnVal = chooser.showOpenDialog(null);
		File f = null;
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			f = chooser.getSelectedFile();
		}
		
		return f;
	}
}