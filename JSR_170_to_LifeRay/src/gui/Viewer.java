package gui;

import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Viewer {
	
	/**
	 * Show the organisms parsed in a simple JTextArea
	 * @param the organisms map
	 * @param fileName (just for printing it out)
	 */
	public static void printForChecking(HashMap<String,String> organisms,String fileName){
		
		JFrame f = new JFrame("");
		f.setBounds(400,100, 50, 50);
		JPanel panel = new JPanel();
	    f.getContentPane().add(panel);
	    
	    JTextArea textArea = new JTextArea(25,50);
	    //textArea.setEditable(false);
	    textArea.append("ORGANISM FOUNDED IN FILE "+fileName+": "+organisms.size()+"\n\n");
	    
	    for(String name : organisms.keySet())
	    {
	    	textArea.append("Organism name: "+name);
	    	textArea.append("\n");
	    	textArea.append("Its DNA Sequence:");
	    	textArea.append("\n");
	    	textArea.append(organisms.get(name));
	    	textArea.append("\n\n");
	    }
	    //
	    //textArea.setWrapStyleWord(true);

	    textArea.setLineWrap(true);
	    
	    panel.add(new JScrollPane(textArea) );	    
	    f.pack();    
	    f.setVisible(true);
	       
	    
	    }
	}
