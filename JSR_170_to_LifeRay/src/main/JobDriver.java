package main;

import java.io.File;

import gui.OpenFile;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;

import util.IDgenerator;
import util.NodeHelper;

import controller.NodeFileImporter;
import controller.NodeDispatcher;

/**
 * Main class that takes care of taking the necessary parameters
 * for the import of LifeRay
 */
public class JobDriver {

	public static final String NAME = "Job_Driver";
	
	/**
	 * Main method for job scheduler.
	 *
	 * @param command line parameter.
	 */
	public static void main(String[] args){
		File rootNode=null;
		
		try {
			rootNode = OpenFile.getFileFromBrowser("node");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		long start = System.currentTimeMillis();
	    
		
		if(rootNode!=null)
		{
			try {
				NodeFileImporter.getParser().processLibrary(rootNode.getAbsolutePath());
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("INFO - Importing done");
		}
		else
		{
			long end = System.currentTimeMillis();		
			System.out.println("INFO - Time taken for the job: "+ (end - start) + " msec");
		}
	}

}
