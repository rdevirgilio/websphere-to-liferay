package model;

/**
 * Class used for the management of the pairs <sv:name,mimeType>
 */
public class TagPropertyValue {

	/**
	 * @uml.property  name="name"
	 */
	private String name;
	/**
	 * @uml.property  name="mimeType"
	 */
	private String mimeType;
	/**
	 * @uml.property  name="crappyId"
	 */
	private long crappyId;
	
	private TagPropertyValue() {
		this.crappyId = System.currentTimeMillis();
	}

	public TagPropertyValue(String name, String mimeType) {
		this();
		this.name = name;
		this.mimeType = mimeType;
	}
	
	/**
	 * @return
	 * @uml.property  name="name"
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * @uml.property  name="name"
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return
	 * @uml.property  name="mimeType"
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * @param mimeType
	 * @uml.property  name="mimeType"
	 */
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagPropertyValue other = (TagPropertyValue) obj;
		if (crappyId != other.crappyId)
			return false;
		if (mimeType == null) {
			if (other.mimeType != null)
				return false;
		} else if (!mimeType.equals(other.mimeType))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (crappyId ^ (crappyId >>> 32));
		result = prime * result
				+ ((mimeType == null) ? 0 : mimeType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
}