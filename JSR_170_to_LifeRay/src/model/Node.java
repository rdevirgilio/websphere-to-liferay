package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import util.UUIDUtil;

/**
 * Class that models an object .node
 */
public class Node {
	
	/**
	 * Attributes in icm:node isRoot -> retrieved from icm:path path -> icm:path workspace -> icm:workspace (Always the same) UUID -> icm:uuid parentUUID -> retrieved from icm:path parentName -> retrieved from icm:path
	 * @uml.property  name="isRoot"
	 */
	private boolean isRoot;
	/**
	 * @uml.property  name="path"
	 */
	private String path;
	/**
	 * @uml.property  name="workspace"
	 */
	private String workspace;
	/**
	 * @uml.property  name="uUID"
	 */
	private String UUID;
	/**
	 * @uml.property  name="parentUUID"
	 * @uml.associationEnd  qualifier="key:java.lang.String java.lang.String"
	 */
	private String parentUUID;
	/**
	 * @uml.property  name="parentName"
	 */
	private String parentName;
	
	/**
	 * newID -> New UUID generator when stored in Liferay
	 * @uml.property  name="newID"
	 */
	private long newID;
	
	/**
	 * Attributes in sv:node name -> sv:name nodeType -> jcr:nodeType
	 * @uml.property  name="name"
	 */
	private String name;
	/**
	 * @uml.property  name="nodeType"
	 */
	private String nodeType;	
	

	/**
	 * Value taken from property tags resourceName -> ibmcontentwcm:resourceName label -> icm:label title -> icm:title isDoc -> icm:isdoc description -> icm:description classification -> ibmcontentwcm:classification
	 * @uml.property  name="resourceName"
	 */
	private String resourceName;
	
	/**
	 * @uml.property  name="wcmName"
	 */
	private String wcmName;

	/**
	 * @uml.property  name="label"
	 */
	private String label;
	/**
	 * @uml.property  name="title"
	 */
	private String title;
	/**
	 * @uml.property  name="isDoc"
	 */
	private String isDoc;
	
	/**
	 * @uml.property  name="description"
	 */
	private String description;
	/**
	 * @uml.property  name="classification"
	 */
	private String classification;
	
	/**
	 * @uml.property  name="fileName"
	 */
	private String fileName;
	/**
	 * @uml.property  name="absFilePath"
	 */
	private String AbsFilePath;
	
	
	private ArrayList<String> authoringStyleMap;
	
	private ArrayList<String> presentationStyleMap;
	
	/**
	 * Map that contains all the data from the file .node
	 * @uml.property  name="tagPropertyValue2CDATAMap"
	 * @uml.associationEnd  qualifier="t:model.TagPropertyValue java.lang.String"
	 */
	private Map<TagPropertyValue,String> tagPropertyValue2CDATAMap;
	
	public Node(){
		
		 this.tagPropertyValue2CDATAMap = new HashMap<TagPropertyValue,String>();
		 this.authoringStyleMap = new ArrayList<String>();
		 this.presentationStyleMap = new ArrayList<String>();

	}

	
	
	/* GETTER & SETTER */
	
	public ArrayList<String> getPresentationStyleMap() {
		return presentationStyleMap;
	}

	public void setPresentationStyleMap(ArrayList<String> presentationStyleMap) {
		this.presentationStyleMap = presentationStyleMap;
	}

	/**
	 * @return
	 * @uml.property  name="wcmName"
	 */
	public String getWcmName() {
		return wcmName;
	}

	public ArrayList<String> getAuthoringStyleMap() {
		return authoringStyleMap;
	}

	public void setAuthoringStyleMap(ArrayList<String> authoringStyleMap) {
		this.authoringStyleMap = authoringStyleMap;
	}

	/**
	 * @param wcmName
	 * @uml.property  name="wcmName"
	 */
	public void setWcmName(String wcmName) {
		this.wcmName = wcmName;
	}

	
	public void setTagPropertyValue2CDATAMap(
			Map<TagPropertyValue, String> tagPropertyValue2CDATAMap) {
		this.tagPropertyValue2CDATAMap = tagPropertyValue2CDATAMap;
	}

	/**
	 * @return
	 * @uml.property  name="absFilePath"
	 */
	public String getAbsFilePath() {
		return AbsFilePath;
	}

	/**
	 * @param absFilePath
	 * @uml.property  name="absFilePath"
	 */
	public void setAbsFilePath(String absFilePath) {
		AbsFilePath = absFilePath;
	}

	/**
	 * @param nodeType
	 * @uml.property  name="nodeType"
	 */
	public void setNodeType(String nodeType) {		
		
		this.nodeType = nodeType;
	}
	
	/**
	 * @return
	 * @uml.property  name="newID"
	 */
	public long getNewID() {
		return newID;
	}

	/**
	 * @param newID
	 * @uml.property  name="newID"
	 */
	public void setNewID(long newID) {
		this.newID = newID;
	}
	public Map<TagPropertyValue, String> getTagPropertyValue2CDATAMap() {
		return tagPropertyValue2CDATAMap;
	}
	
	/**
	 * @return
	 * @uml.property  name="isDoc"
	 */
	public String getIsDoc() {
		return isDoc;
	}

	/**
	 * @param isDoc
	 * @uml.property  name="isDoc"
	 */
	public void setIsDoc(String isDoc) {
		this.isDoc = isDoc;
	}

	/**
	 * @return
	 * @uml.property  name="title"
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 * @uml.property  name="title"
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return
	 * @uml.property  name="path"
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 * @uml.property  name="path"
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return
	 * @uml.property  name="workspace"
	 */
	public String getWorkspace() {
		return workspace;
	}

	/**
	 * @param workspace
	 * @uml.property  name="workspace"
	 */
	public void setWorkspace(String workspace) {
		this.workspace = workspace;
	}

	/**
	 * @return
	 * @uml.property  name="parentUUID"
	 */
	public String getParentUUID() {
		return parentUUID;
	}

	/**
	 * @param parentUUID
	 * @uml.property  name="parentUUID"
	 */
	public void setParentUUID(String parentUUID) {
		this.parentUUID = parentUUID;
	}

	/**
	 * @return
	 * @uml.property  name="parentName"
	 */
	public String getParentName() {
		return parentName;
	}

	/**
	 * @param parentName
	 * @uml.property  name="parentName"
	 */
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	/**
	 * @return
	 * @uml.property  name="name"
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * @uml.property  name="name"
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return
	 * @uml.property  name="resourceName"
	 */
	public String getResourceName() {
		return resourceName;
	}

	/**
	 * @param resourceName
	 * @uml.property  name="resourceName"
	 */
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	/**
	 * @return
	 * @uml.property  name="description"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 * @uml.property  name="description"
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return
	 * @uml.property  name="classification"
	 */
	public String getClassification() {
		return classification;
	}

	/**
	 * @param classification
	 * @uml.property  name="classification"
	 */
	public void setClassification(String classification) {
		this.classification = classification;
	}
	
	/**
	 * @return
	 * @uml.property  name="nodeType"
	 */
	public String getNodeType() {
		return nodeType;
	}
	
	public String generateUUID(){
		return this.UUID = UUIDUtil.generate();
	}

	/**
	 * @return
	 * @uml.property  name="label"
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 * @uml.property  name="label"
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	public boolean isRoot() {
		
		
		return (nodeType.equalsIgnoreCase("icm:documentLibrary"));
	
	}

	public void setRoot(boolean isRoot) {
		this.isRoot = isRoot;
	}
	
	/**
	 * @return
	 * @uml.property  name="uUID"
	 */
	public String getUUID() {
		return UUID;
	}
	/**
	 * @param uUID
	 * @uml.property  name="uUID"
	 */
	public void setUUID(String uUID) {
		UUID = uUID;
	}

	/**
	 * @return
	 * @uml.property  name="fileName"
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 * @uml.property  name="fileName"
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
