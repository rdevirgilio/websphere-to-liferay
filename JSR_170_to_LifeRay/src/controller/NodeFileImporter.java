package controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import parser.XMLParser;
import util.NodeHelper;

import model.Node;

/**
 * Class that explores the workspace of WebSphere to import 
 * and use the classes Dispatcher to go to save the contents
 */
public class NodeFileImporter {

	private static NodeFileImporter parser;
	private static XMLParser parserXML;
	private static NodeDispatcher objectDispatcher;
	private static ArrayList<String> avoidFolderList;
	private static ArrayList<String> inOrderToProcessFoldersList;
	
	/** 
	 * @uml.property name="currentNode"
	 * @uml.associationEnd 
	 */
	private Node currentNode;
	
	private static List<File> coda = new LinkedList<File>();
	
	public static NodeFileImporter getParser(){
		if(parser==null){
			parser = new NodeFileImporter();
			objectDispatcher = NodeDispatcher.getResourceManager();
			parserXML = XMLParser.getParser();
									
			inOrderToProcessFoldersList = new ArrayList<String>(){
				private static final long serialVersionUID = 5305805249675342792L;
			{
				
				add("%41uthoring %54emplates");
				add("%50resentation %54emplates");
				add("%43ontent");
				add("%43omponents");
							
			}};
			
			
			/**
			 * List of folders that are not considered 
			 * during the exploration of the workspace
			 */
			avoidFolderList = new ArrayList<String>(){
				private static final long serialVersionUID = 5305803249675342792L;
			{
				add("%57orkflow");
				add("%55%49%43ontrols");
				add("%54axonomies");
				add("%53ystem");
				/*Controllare se � giusto inserire i successivi due campi*/
				//add("%41uthoring %54emplates");
				//add("%50resentation %54emplates");
			}};
		}
		return parser;
	}
	
	/**
	 * Method that explores the contents of the workspace
	 * @param path
	 * @throws Exception
	 */
	
	public void processLibrary ( String path ) throws Exception {
		
		File root = new File( path );
		coda.addAll(Arrays.asList(root.listFiles()));
		
		for(File rootFile : coda){
			if(rootFile.getName().contains(".node"))
			{
		
				currentNode = parserXML.parseNode(new File(rootFile.getAbsolutePath()));
	        	currentNode.setFileName(NodeHelper.decodeURL(getNameFileNoExt(rootFile)));
	        	currentNode.setAbsFilePath(rootFile.getAbsolutePath());
	        	System.out.println("Dispatching...");
	        	System.out.println();
	        	objectDispatcher.parseNode(currentNode,rootFile.getAbsolutePath(),false);
	        	coda.remove(rootFile);
			}
			
		}
		
		File rootChildrenFolder = coda.remove(0);
		List<File> rootChildrenFilesAndFolders = Arrays.asList(rootChildrenFolder.listFiles());
		
		int i=0,j=0;
		//boolean webContentFilter = true; //if true, the method walk will not consider web content nodes.
		boolean webContentFilter = false;
		/*
		 * Pass to the walk method in order some folders. Need to pass first the authoring templates then the presentation. 
		 * Subsequently all the site areas and the all the content
		 */
		
		while(i<inOrderToProcessFoldersList.size())
		{
			boolean folderFound = false;
			System.out.println("Sto cercando - inOrderToProcessFoldersList (name): " + inOrderToProcessFoldersList.get(i));
			while(j<rootChildrenFilesAndFolders.size() && i<inOrderToProcessFoldersList.size())
			//while(j<rootChildrenFilesAndFolders.size() && !folderFound)
			{
				System.out.println("Parto da - rootChildrenFilesAndFolders (name): " + rootChildrenFilesAndFolders.get(j).getName());
				try{
				
				if(rootChildrenFilesAndFolders.get(j).getName().contains(inOrderToProcessFoldersList.get(i)))
				{
					walk(rootChildrenFilesAndFolders.get(j).getAbsolutePath(),webContentFilter);
					System.out.println("rootChildrenFilesAndFolders (size): " + rootChildrenFilesAndFolders.size() + " indice: "+j);
					System.out.println("inOrderToProcessFoldersList (size): " + inOrderToProcessFoldersList.size() + " indice: "+i);
					System.out.println("rootChildrenFilesAndFolders (name): " + rootChildrenFilesAndFolders.get(j).getName());
					System.out.println("inOrderToProcessFoldersList (name): " + inOrderToProcessFoldersList.get(i));
					System.out.println();
					folderFound = true;
					j=0;
					i++;
				}
				else
					j++;
			}catch (IndexOutOfBoundsException e)
			{
				System.out.println(e.toString());
				System.out.println("rootChildrenFilesAndFolders (size): " + rootChildrenFilesAndFolders.size() + " indice: "+j);
				System.out.println("inOrderToProcessFoldersList (size): " + inOrderToProcessFoldersList.size() + " indice: "+i);
			}
			}
			//i++;
		}
			
		
	}
	public void walk( String path, boolean processWebContent ) throws Exception {
		
		/**
		 * Perform a breadth-first search
		 */
		File root = new File( path );
		coda.add(root);

		File current = null;
        while(coda.size()!=0){
        	current = coda.remove(0);
        	
        	//If it is a folder
        	if ( current.isDirectory() && !avoidFolderList.contains(current.getName())) {
        		//System.out.println( "INFO - Dir: " + current.getAbsoluteFile());
        		coda.addAll(Arrays.asList(current.listFiles()));   

            }
        	//If it is a file
            else if (!avoidFolderList.contains(getNameFileNoExt(current))){
                //System.out.println( "INFO - File: " + current.getAbsoluteFile() );
                if(isNode(current)){
                	System.out.println("==================");
                	System.out.println();
                	System.out.println("Getting new file...");
                	System.out.println();   	
                	System.out.println("File: "+current.getAbsolutePath());
                	System.out.println("Parsing....");
                	System.out.println();
                	currentNode = parserXML.parseNode(new File(current.getAbsolutePath()));
                	currentNode.setFileName(NodeHelper.decodeURL(getNameFileNoExt(current)));
                	currentNode.setAbsFilePath(current.getAbsolutePath());
                	System.out.println("Dispatching...");
                	System.out.println();
                	if(!processWebContent)
                		objectDispatcher.parseNode(currentNode,current.getAbsolutePath(), processWebContent);
                	
//                	if(!processWebContent)
//                		resourceDispatcher.parseNode(currentNode,current.getAbsolutePath());
//                	else
                		
                }
            }
        }
    }

	/**
	 * Method that checks whether the analyzed file has extension .node
	 * @param File
	 * @return boolean that tells if the file is a node
	 */
	public static boolean isNode(File f) {
		if(f.isDirectory())
			return false;		
		String ext = f.getName().substring(f.getName().lastIndexOf('.'),f.getName().length());		
		if(ext.equals(".node"))
			return true;		
		return false;
	}
	
	/**
	 * Method that checks whether the analyzed file has extension .value
	 * @param File
	 * @return boolean - tells if the file is a value
	 */
	public static boolean isValue(File f) {
		if(f.isDirectory())
			return false;		
		String ext = f.getName().substring(f.getName().lastIndexOf('.'),f.getName().length());		
		if(ext.equals(".value"))
			return true;		
		return false;		
	}
	
	/**
	 * Method that returns the name of the file without extension
	 * @param File
	 * @return String - the file name without the extension
	 */
	public static String getNameFileNoExt(File f){
		if(!f.isDirectory())
			return f.getName().substring(0,f.getName().lastIndexOf('.'));
		return f.getName();
	}
}
