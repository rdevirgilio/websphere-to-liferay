package controller;

import it.liferay.classimport.ImporterFacade;

import java.util.HashMap;
import java.util.Map;

import com.sun.org.apache.xalan.internal.xsltc.dom.CurrentNodeListFilter;

import portlets.*;

import store.HierarchyStore;
import util.Base64Converter;
import util.NodeHelper;

import model.TagPropertyValue;
import model.Node;

/**
 * Static class that has the responsibility to analyze a resource with its
 * content and execute the opportune command to memorize it in liferay
 */
public class NodeDispatcher {
	
	private static Long returnedId;

	private static boolean dispatched=false;

	/**
	 * Instance of the class
	 */
	private static NodeDispatcher manager;

	/**
	 * TODO
	 */
	
	private static DocumentsAndMediaDispatcher DMDispatcher;
	/**
	 * TODO
	 */
	
	private static CategoriesDispatcher categoriesDispatcher;

	private static StructuresDispatcher structureDispatcher;
	
	private static TemplatesDispatcher templateDispatcher;
	
	/**
	 * Method to take the class's instance
	 * 
	 * @return the classes dispatcher's instance
	 */
	public static NodeDispatcher getResourceManager() {
		if (manager == null) {
			manager = new NodeDispatcher();
			DMDispatcher = DocumentsAndMediaDispatcher.getInstance();
			categoriesDispatcher = CategoriesDispatcher.getInstance();
			structureDispatcher = StructuresDispatcher.getInstance(); 
			templateDispatcher = TemplatesDispatcher.getInstance();
		}
		return manager;
	}

	/**
	 * Principal method responsible to analyze the node's content and decide if
	 * it can be memorized into Liferay.
	 * 
	 * @param currentNode
	 *            a reference of an instance of {@link Node}
	 * @param absPathNodeFile
	 *            the string of the absolute path of the node (folder or file)
	 *            on the filesystem
	 * @param fileName
	 *            its name
	 * @throws Exception
	 */
	public Long parseNode(Node currentNode, String absPathNodeFile, boolean processContent)
			throws Exception {
		
		Long returnedId = ImporterFacade.ERRORCODE;
		
		//MONITORING
		System.out.println("Pending node...");
		System.out.println("type: "+currentNode.getNodeType()+" name: "+currentNode.getName());
		System.out.println("absPathNodeFile"+currentNode.getAbsFilePath() + "--->  lastIndexOf '.' :  " + absPathNodeFile.lastIndexOf('.') );
		System.out.println("UUID--->  " + currentNode.getUUID());
		System.out.print(absPathNodeFile.substring(0, absPathNodeFile.lastIndexOf('.')));
		System.out.println();
		//MONITORING
		
//		if(currentNode.getNodeType()!=null && currentNode.getNodeType().contains("siteArea"))
//			returnedId = categoriesDispatcher.processSiteArea(currentNode);
//		else
//			if(currentNode.getNodeType()!=null && currentNode.getNodeType().contains("presentationTemplate")) 
//				returnedId = structureDispatcher.processPresentation(currentNode);
//			else
		
		HierarchyStore.getPath2UUID().put(absPathNodeFile.substring(0, absPathNodeFile.lastIndexOf('.')), currentNode.getUUID() );
		
		/*
		 *  Required for type nodes and convert them into Categories/Structure/Template
		 */
			String nodeType = currentNode.getNodeType();
			
		/*
		 *  SiteArea processing	
		 */
			if(nodeType!=null && nodeType.contains("siteArea"))
			{
				returnedId = categoriesDispatcher.processSiteArea(currentNode);
			}

			/*
			 * PresentationTemplate processing	
			 */
			if(nodeType!=null && nodeType.contains("presentationTemplates") )
				returnedId = structureDispatcher.processPresentation(currentNode);
		
		/*
		 * Building root node
		 */
			if (currentNode.isRoot()) 
			{
				
				returnedId = DMDispatcher.buildRoot(currentNode);
				
				//MONITORING
				System.out.println("Node \""+currentNode.getName()+"\" dispatched successfully" );
				System.out.println();
				//MONITORING
				
				return returnedId;
			}
		/*
		 * It's not root. parentUUID set. 
		 */
			String parentDirectory = absPathNodeFile.substring(0, absPathNodeFile.lastIndexOf(System.getProperty("file.separator"))); //abs path to parent directory..
			currentNode.setParentUUID(HierarchyStore.getPath2UUID().get(parentDirectory)); //..UUID parent set on currentNode
		
		//String name = currentNode.getName();
			
			
		if(processContent)
		{
			/*
			 * Time for get useful properties for parsing resources
			 */
			
						
			/*
			 * Required properties for the Documents and Media portlets 
			 */
				TagPropertyValue icmData = NodeHelper.getPropertyValue("icm:data", currentNode);
				TagPropertyValue wcmHtml = NodeHelper.getPropertyValue("ibmcontentwcm:html", currentNode);
			

			
			/*
			 * A "real" file as pdf, doc, css and so on
			 */
				if(icmData!=null)
					{

						returnedId = DMDispatcher.buildFile(icmData,currentNode);
						
						//MONITORING
						System.out.println("Node \""+currentNode.getName()+"\" dispatched successfully" );
						System.out.println();
						//MONITORING
						if(returnedId!=ImporterFacade.ERRORCODE)
						{
							currentNode.setNewID(returnedId);
							HierarchyStore.getUUID2newID().put(currentNode.getUUID(), returnedId);
						}
						
						return returnedId;
					}
				
				/*
				 * Html component
				 */
				if(wcmHtml!=null)
				{

					returnedId = DMDispatcher.buildFile(wcmHtml,currentNode);

					//MONITORING
					System.out.println("Node \""+currentNode.getName()+"\" dispatched successfully" );
					System.out.println();
					//MONITORING

					if(returnedId!=ImporterFacade.ERRORCODE)
					{
						currentNode.setNewID(returnedId);
						HierarchyStore.getUUID2newID().put(currentNode.getUUID(), returnedId);
					}

					return returnedId;
				}
				
				
				/*
				 * Unknown type. Let's make a folder.
				 */
				

				returnedId = DMDispatcher.buildDirectory(currentNode);

				//MONITORING
				System.out.println("Node \""+currentNode.getName()+"\" dispatched successfully" );
				System.out.println();
				//MONITORING

		}
		
		
		if (returnedId == null) System.out.println("nullo returnedID");
		
		
		if(returnedId!=ImporterFacade.ERRORCODE)
		{
			currentNode.setNewID(returnedId);
			HierarchyStore.getUUID2newID().put(currentNode.getUUID(), returnedId);
		}
		
		
		return returnedId;
	}


	/**
	 * 
	 * @param currentNode
	 * @return SOAP generated ID
	 * @throws Exception
	 */
	


	
	
	
	public static Long getReturnedId() {
		return returnedId;
	}

	public static void setReturnedId(Long returnedId) {
		NodeDispatcher.returnedId = returnedId;
	}

}
